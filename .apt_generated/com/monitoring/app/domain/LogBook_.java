package com.monitoring.app.domain;

import java.time.Instant;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.sehati.app.domain.LogBook;
import com.sehati.app.domain.User;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LogBook.class)
public abstract class LogBook_ {

	public static volatile SingularAttribute<LogBook, String> keperluan;
	public static volatile SingularAttribute<LogBook, Instant> selesai;
	public static volatile SingularAttribute<LogBook, String> catatan;
	public static volatile SingularAttribute<LogBook, String> kategori;
	public static volatile SingularAttribute<LogBook, Instant> mulai;
	public static volatile SingularAttribute<LogBook, Long> id;
	public static volatile SingularAttribute<LogBook, LocalDate> tanggal;
	public static volatile SingularAttribute<LogBook, User> user;

	public static final String KEPERLUAN = "keperluan";
	public static final String SELESAI = "selesai";
	public static final String CATATAN = "catatan";
	public static final String KATEGORI = "kategori";
	public static final String MULAI = "mulai";
	public static final String ID = "id";
	public static final String TANGGAL = "tanggal";
	public static final String USER = "user";

}

