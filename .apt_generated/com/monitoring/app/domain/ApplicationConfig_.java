package com.monitoring.app.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.sehati.app.domain.ApplicationConfig;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ApplicationConfig.class)
public abstract class ApplicationConfig_ {

	public static volatile SingularAttribute<ApplicationConfig, String> name;
	public static volatile SingularAttribute<ApplicationConfig, String> usedFor;
	public static volatile SingularAttribute<ApplicationConfig, Long> id;
	public static volatile SingularAttribute<ApplicationConfig, String> category;
	public static volatile SingularAttribute<ApplicationConfig, String> value;

	public static final String NAME = "name";
	public static final String USED_FOR = "usedFor";
	public static final String ID = "id";
	public static final String CATEGORY = "category";
	public static final String VALUE = "value";

}

