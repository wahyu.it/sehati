import { element, by, ElementFinder } from 'protractor';

export class SiswaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-siswa div table .btn-danger'));
  title = element.all(by.css('jhi-siswa div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SiswaUpdatePage {
  pageTitle = element(by.id('jhi-siswa-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nisInput = element(by.id('field_nis'));
  namaInput = element(by.id('field_nama'));
  alamatInput = element(by.id('field_alamat'));
  jenisKelaminSelect = element(by.id('field_jenisKelamin'));
  tempatLahirInput = element(by.id('field_tempatLahir'));
  tanggalLahirInput = element(by.id('field_tanggalLahir'));
  waliMuridInput = element(by.id('field_waliMurid'));
  noTelephoneInput = element(by.id('field_noTelephone'));
  tahunAjaranInput = element(by.id('field_tahunAjaran'));

  kelasSelect = element(by.id('field_kelas'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNisInput(nis: string): Promise<void> {
    await this.nisInput.sendKeys(nis);
  }

  async getNisInput(): Promise<string> {
    return await this.nisInput.getAttribute('value');
  }

  async setNamaInput(nama: string): Promise<void> {
    await this.namaInput.sendKeys(nama);
  }

  async getNamaInput(): Promise<string> {
    return await this.namaInput.getAttribute('value');
  }

  async setAlamatInput(alamat: string): Promise<void> {
    await this.alamatInput.sendKeys(alamat);
  }

  async getAlamatInput(): Promise<string> {
    return await this.alamatInput.getAttribute('value');
  }

  async setJenisKelaminSelect(jenisKelamin: string): Promise<void> {
    await this.jenisKelaminSelect.sendKeys(jenisKelamin);
  }

  async getJenisKelaminSelect(): Promise<string> {
    return await this.jenisKelaminSelect.element(by.css('option:checked')).getText();
  }

  async jenisKelaminSelectLastOption(): Promise<void> {
    await this.jenisKelaminSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setTempatLahirInput(tempatLahir: string): Promise<void> {
    await this.tempatLahirInput.sendKeys(tempatLahir);
  }

  async getTempatLahirInput(): Promise<string> {
    return await this.tempatLahirInput.getAttribute('value');
  }

  async setTanggalLahirInput(tanggalLahir: string): Promise<void> {
    await this.tanggalLahirInput.sendKeys(tanggalLahir);
  }

  async getTanggalLahirInput(): Promise<string> {
    return await this.tanggalLahirInput.getAttribute('value');
  }

  async setWaliMuridInput(waliMurid: string): Promise<void> {
    await this.waliMuridInput.sendKeys(waliMurid);
  }

  async getWaliMuridInput(): Promise<string> {
    return await this.waliMuridInput.getAttribute('value');
  }

  async setNoTelephoneInput(noTelephone: string): Promise<void> {
    await this.noTelephoneInput.sendKeys(noTelephone);
  }

  async getNoTelephoneInput(): Promise<string> {
    return await this.noTelephoneInput.getAttribute('value');
  }

  async setTahunAjaranInput(tahunAjaran: string): Promise<void> {
    await this.tahunAjaranInput.sendKeys(tahunAjaran);
  }

  async getTahunAjaranInput(): Promise<string> {
    return await this.tahunAjaranInput.getAttribute('value');
  }

  async kelasSelectLastOption(): Promise<void> {
    await this.kelasSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async kelasSelectOption(option: string): Promise<void> {
    await this.kelasSelect.sendKeys(option);
  }

  getKelasSelect(): ElementFinder {
    return this.kelasSelect;
  }

  async getKelasSelectedOption(): Promise<string> {
    return await this.kelasSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SiswaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-siswa-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-siswa'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
