import { element, by, ElementFinder } from 'protractor';

export class TagihanComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-tagihan div table .btn-danger'));
  title = element.all(by.css('jhi-tagihan div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TagihanUpdatePage {
  pageTitle = element(by.id('jhi-tagihan-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nomorInput = element(by.id('field_nomor'));
  tanggalInput = element(by.id('field_tanggal'));
  totalInput = element(by.id('field_total'));
  jatuhTempoInput = element(by.id('field_jatuhTempo'));
  tanggalBayarInput = element(by.id('field_tanggalBayar'));
  statusSelect = element(by.id('field_status'));

  besarBayarSelect = element(by.id('field_besarBayar'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNomorInput(nomor: string): Promise<void> {
    await this.nomorInput.sendKeys(nomor);
  }

  async getNomorInput(): Promise<string> {
    return await this.nomorInput.getAttribute('value');
  }

  async setTanggalInput(tanggal: string): Promise<void> {
    await this.tanggalInput.sendKeys(tanggal);
  }

  async getTanggalInput(): Promise<string> {
    return await this.tanggalInput.getAttribute('value');
  }

  async setTotalInput(total: string): Promise<void> {
    await this.totalInput.sendKeys(total);
  }

  async getTotalInput(): Promise<string> {
    return await this.totalInput.getAttribute('value');
  }

  async setJatuhTempoInput(jatuhTempo: string): Promise<void> {
    await this.jatuhTempoInput.sendKeys(jatuhTempo);
  }

  async getJatuhTempoInput(): Promise<string> {
    return await this.jatuhTempoInput.getAttribute('value');
  }

  async setTanggalBayarInput(tanggalBayar: string): Promise<void> {
    await this.tanggalBayarInput.sendKeys(tanggalBayar);
  }

  async getTanggalBayarInput(): Promise<string> {
    return await this.tanggalBayarInput.getAttribute('value');
  }

  async setStatusSelect(status: string): Promise<void> {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect(): Promise<string> {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(): Promise<void> {
    await this.statusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async besarBayarSelectLastOption(): Promise<void> {
    await this.besarBayarSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async besarBayarSelectOption(option: string): Promise<void> {
    await this.besarBayarSelect.sendKeys(option);
  }

  getBesarBayarSelect(): ElementFinder {
    return this.besarBayarSelect;
  }

  async getBesarBayarSelectedOption(): Promise<string> {
    return await this.besarBayarSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TagihanDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-tagihan-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-tagihan'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
