import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TagihanComponentsPage, TagihanDeleteDialog, TagihanUpdatePage } from './tagihan.page-object';

const expect = chai.expect;

describe('Tagihan e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let tagihanComponentsPage: TagihanComponentsPage;
  let tagihanUpdatePage: TagihanUpdatePage;
  let tagihanDeleteDialog: TagihanDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Tagihans', async () => {
    await navBarPage.goToEntity('tagihan');
    tagihanComponentsPage = new TagihanComponentsPage();
    await browser.wait(ec.visibilityOf(tagihanComponentsPage.title), 5000);
    expect(await tagihanComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.tagihan.home.title');
    await browser.wait(ec.or(ec.visibilityOf(tagihanComponentsPage.entities), ec.visibilityOf(tagihanComponentsPage.noResult)), 1000);
  });

  it('should load create Tagihan page', async () => {
    await tagihanComponentsPage.clickOnCreateButton();
    tagihanUpdatePage = new TagihanUpdatePage();
    expect(await tagihanUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.tagihan.home.createOrEditLabel');
    await tagihanUpdatePage.cancel();
  });

  it('should create and save Tagihans', async () => {
    const nbButtonsBeforeCreate = await tagihanComponentsPage.countDeleteButtons();

    await tagihanComponentsPage.clickOnCreateButton();

    await promise.all([
      tagihanUpdatePage.setNomorInput('nomor'),
      tagihanUpdatePage.setTanggalInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      tagihanUpdatePage.setTotalInput('5'),
      tagihanUpdatePage.setJatuhTempoInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      tagihanUpdatePage.setTanggalBayarInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      tagihanUpdatePage.statusSelectLastOption(),
      tagihanUpdatePage.besarBayarSelectLastOption()
    ]);

    expect(await tagihanUpdatePage.getNomorInput()).to.eq('nomor', 'Expected Nomor value to be equals to nomor');
    expect(await tagihanUpdatePage.getTanggalInput()).to.contain('2001-01-01T02:30', 'Expected tanggal value to be equals to 2000-12-31');
    expect(await tagihanUpdatePage.getTotalInput()).to.eq('5', 'Expected total value to be equals to 5');
    expect(await tagihanUpdatePage.getJatuhTempoInput()).to.contain(
      '2001-01-01T02:30',
      'Expected jatuhTempo value to be equals to 2000-12-31'
    );
    expect(await tagihanUpdatePage.getTanggalBayarInput()).to.contain(
      '2001-01-01T02:30',
      'Expected tanggalBayar value to be equals to 2000-12-31'
    );

    await tagihanUpdatePage.save();
    expect(await tagihanUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await tagihanComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Tagihan', async () => {
    const nbButtonsBeforeDelete = await tagihanComponentsPage.countDeleteButtons();
    await tagihanComponentsPage.clickOnLastDeleteButton();

    tagihanDeleteDialog = new TagihanDeleteDialog();
    expect(await tagihanDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.tagihan.delete.question');
    await tagihanDeleteDialog.clickOnConfirmButton();

    expect(await tagihanComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
