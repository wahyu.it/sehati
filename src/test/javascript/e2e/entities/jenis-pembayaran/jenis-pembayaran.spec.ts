import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { JenisPembayaranComponentsPage, JenisPembayaranDeleteDialog, JenisPembayaranUpdatePage } from './jenis-pembayaran.page-object';

const expect = chai.expect;

describe('JenisPembayaran e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let jenisPembayaranComponentsPage: JenisPembayaranComponentsPage;
  let jenisPembayaranUpdatePage: JenisPembayaranUpdatePage;
  let jenisPembayaranDeleteDialog: JenisPembayaranDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load JenisPembayarans', async () => {
    await navBarPage.goToEntity('jenis-pembayaran');
    jenisPembayaranComponentsPage = new JenisPembayaranComponentsPage();
    await browser.wait(ec.visibilityOf(jenisPembayaranComponentsPage.title), 5000);
    expect(await jenisPembayaranComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.jenisPembayaran.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(jenisPembayaranComponentsPage.entities), ec.visibilityOf(jenisPembayaranComponentsPage.noResult)),
      1000
    );
  });

  it('should load create JenisPembayaran page', async () => {
    await jenisPembayaranComponentsPage.clickOnCreateButton();
    jenisPembayaranUpdatePage = new JenisPembayaranUpdatePage();
    expect(await jenisPembayaranUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.jenisPembayaran.home.createOrEditLabel');
    await jenisPembayaranUpdatePage.cancel();
  });

  it('should create and save JenisPembayarans', async () => {
    const nbButtonsBeforeCreate = await jenisPembayaranComponentsPage.countDeleteButtons();

    await jenisPembayaranComponentsPage.clickOnCreateButton();

    await promise.all([jenisPembayaranUpdatePage.setJenisInput('jenis'), jenisPembayaranUpdatePage.setMetodeInput('metode')]);

    expect(await jenisPembayaranUpdatePage.getJenisInput()).to.eq('jenis', 'Expected Jenis value to be equals to jenis');
    expect(await jenisPembayaranUpdatePage.getMetodeInput()).to.eq('metode', 'Expected Metode value to be equals to metode');

    await jenisPembayaranUpdatePage.save();
    expect(await jenisPembayaranUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await jenisPembayaranComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last JenisPembayaran', async () => {
    const nbButtonsBeforeDelete = await jenisPembayaranComponentsPage.countDeleteButtons();
    await jenisPembayaranComponentsPage.clickOnLastDeleteButton();

    jenisPembayaranDeleteDialog = new JenisPembayaranDeleteDialog();
    expect(await jenisPembayaranDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.jenisPembayaran.delete.question');
    await jenisPembayaranDeleteDialog.clickOnConfirmButton();

    expect(await jenisPembayaranComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
