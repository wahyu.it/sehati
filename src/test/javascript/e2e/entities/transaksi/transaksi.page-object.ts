import { element, by, ElementFinder } from 'protractor';

export class TransaksiComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-transaksi div table .btn-danger'));
  title = element.all(by.css('jhi-transaksi div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TransaksiUpdatePage {
  pageTitle = element(by.id('jhi-transaksi-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nomorInput = element(by.id('field_nomor'));
  tanggalInput = element(by.id('field_tanggal'));
  metodeSelect = element(by.id('field_metode'));
  bankSelect = element(by.id('field_bank'));
  nomorRekeningInput = element(by.id('field_nomorRekening'));
  virtualAccountInput = element(by.id('field_virtualAccount'));
  statusSelect = element(by.id('field_status'));
  updateByInput = element(by.id('field_updateBy'));
  updateOnInput = element(by.id('field_updateOn'));

  tagihanSelect = element(by.id('field_tagihan'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNomorInput(nomor: string): Promise<void> {
    await this.nomorInput.sendKeys(nomor);
  }

  async getNomorInput(): Promise<string> {
    return await this.nomorInput.getAttribute('value');
  }

  async setTanggalInput(tanggal: string): Promise<void> {
    await this.tanggalInput.sendKeys(tanggal);
  }

  async getTanggalInput(): Promise<string> {
    return await this.tanggalInput.getAttribute('value');
  }

  async setMetodeSelect(metode: string): Promise<void> {
    await this.metodeSelect.sendKeys(metode);
  }

  async getMetodeSelect(): Promise<string> {
    return await this.metodeSelect.element(by.css('option:checked')).getText();
  }

  async metodeSelectLastOption(): Promise<void> {
    await this.metodeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setBankSelect(bank: string): Promise<void> {
    await this.bankSelect.sendKeys(bank);
  }

  async getBankSelect(): Promise<string> {
    return await this.bankSelect.element(by.css('option:checked')).getText();
  }

  async bankSelectLastOption(): Promise<void> {
    await this.bankSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setNomorRekeningInput(nomorRekening: string): Promise<void> {
    await this.nomorRekeningInput.sendKeys(nomorRekening);
  }

  async getNomorRekeningInput(): Promise<string> {
    return await this.nomorRekeningInput.getAttribute('value');
  }

  async setVirtualAccountInput(virtualAccount: string): Promise<void> {
    await this.virtualAccountInput.sendKeys(virtualAccount);
  }

  async getVirtualAccountInput(): Promise<string> {
    return await this.virtualAccountInput.getAttribute('value');
  }

  async setStatusSelect(status: string): Promise<void> {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect(): Promise<string> {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(): Promise<void> {
    await this.statusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setUpdateByInput(updateBy: string): Promise<void> {
    await this.updateByInput.sendKeys(updateBy);
  }

  async getUpdateByInput(): Promise<string> {
    return await this.updateByInput.getAttribute('value');
  }

  async setUpdateOnInput(updateOn: string): Promise<void> {
    await this.updateOnInput.sendKeys(updateOn);
  }

  async getUpdateOnInput(): Promise<string> {
    return await this.updateOnInput.getAttribute('value');
  }

  async tagihanSelectLastOption(): Promise<void> {
    await this.tagihanSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async tagihanSelectOption(option: string): Promise<void> {
    await this.tagihanSelect.sendKeys(option);
  }

  getTagihanSelect(): ElementFinder {
    return this.tagihanSelect;
  }

  async getTagihanSelectedOption(): Promise<string> {
    return await this.tagihanSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TransaksiDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-transaksi-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-transaksi'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
