package com.sehati.app.service.dto;

import org.apache.poi.ss.usermodel.CellStyle;

/**
 * Created by wahyu on 10/30/2020.
 */
public class CellObject {
    private int index;
    private Object value;
    private CellStyle style;

    public CellObject(int index, Object value, CellStyle style) {
        this.index = index;
        this.value = value;
        this.style = style;
    }

    public CellObject(int index, Object value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public CellStyle getStyle() {
        return style;
    }

    public void setStyle(CellStyle style) {
        this.style = style;
    }
}
