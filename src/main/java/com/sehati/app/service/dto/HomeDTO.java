package com.sehati.app.service.dto;

import java.time.LocalDate;

public class HomeDTO {

    private LocalDate tglPengajuan;
    
    private String namaPendamping;
    
    private String namaAdmin;
    
    private Long jmlTotalPerTahun;
    
    private Long jmlTotalPerBulan;
    
    private Long jmlComplete;
    
    private Long jmlRevisi;
    
    private Long jmlSubmit;
    
    private Long jmlProses;

	public HomeDTO(LocalDate tglPengajuan, String namaPendamping, String namaAdmin, Long jmlTotalPerTahun,
			Long jmlTotalPerBulan, Long jmlComplete, Long jmlRevisi, Long jmlSubmit, Long jmlProses) {
		this.tglPengajuan = tglPengajuan;
		this.namaPendamping = namaPendamping;
		this.namaAdmin = namaAdmin;
		this.jmlTotalPerTahun = jmlTotalPerTahun;
		this.jmlTotalPerBulan = jmlTotalPerBulan;
		this.jmlComplete = jmlComplete;
		this.jmlRevisi = jmlRevisi;
		this.jmlSubmit = jmlSubmit;
		this.jmlProses = jmlProses;
	}

	public LocalDate getTglPengajuan() {
		return tglPengajuan;
	}

	public void setTglPengajuan(LocalDate tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public String getNamaPendamping() {
		return namaPendamping;
	}

	public void setNamaPendamping(String namaPendamping) {
		this.namaPendamping = namaPendamping;
	}

	public String getNamaAdmin() {
		return namaAdmin;
	}

	public void setNamaAdmin(String namaAdmin) {
		this.namaAdmin = namaAdmin;
	}

	public Long getJmlTotalPerTahun() {
		return jmlTotalPerTahun;
	}

	public void setJmlTotalPerTahun(Long jmlTotalPerTahun) {
		this.jmlTotalPerTahun = jmlTotalPerTahun;
	}

	public Long getJmlTotalPerBulan() {
		return jmlTotalPerBulan;
	}

	public void setJmlTotalPerBulan(Long jmlTotalPerBulan) {
		this.jmlTotalPerBulan = jmlTotalPerBulan;
	}

	public Long getJmlComplete() {
		return jmlComplete;
	}

	public void setJmlComplete(Long jmlComplete) {
		this.jmlComplete = jmlComplete;
	}

	public Long getJmlRevisi() {
		return jmlRevisi;
	}

	public void setJmlRevisi(Long jmlRevisi) {
		this.jmlRevisi = jmlRevisi;
	}

	public Long getJmlSubmit() {
		return jmlSubmit;
	}

	public void setJmlSubmit(Long jmlSubmit) {
		this.jmlSubmit = jmlSubmit;
	}

	public Long getJmlProses() {
		return jmlProses;
	}

	public void setJmlProses(Long jmlProses) {
		this.jmlProses = jmlProses;
	}

	@Override
	public String toString() {
		return "HomeDTO [tglPengajuan=" + tglPengajuan + ", namaPendamping=" + namaPendamping + ", namaAdmin="
				+ namaAdmin + ", jmlTotalPerTahun=" + jmlTotalPerTahun + ", jmlTotalPerBulan=" + jmlTotalPerBulan
				+ ", jmlComplete=" + jmlComplete + ", jmlRevisi=" + jmlRevisi + ", jmlSubmit=" + jmlSubmit
				+ ", jmlProses=" + jmlProses + "]";
	}
}
