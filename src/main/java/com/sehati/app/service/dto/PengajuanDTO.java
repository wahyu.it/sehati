package com.sehati.app.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import com.sehati.app.domain.DaftarProduk;
import com.sehati.app.domain.Kabupaten;
import com.sehati.app.domain.Provinsi;

public class PengajuanDTO  implements Serializable {

    private Long id;
    private String noPengajuan;
    private LocalDate tglPengajuan;
    private String nama;
    private String nik;
    private String alamat;
    private String kodepos;
    private String email;
    private String noHp;
    private String namaUsaha;
    private String alamatUsaha;
    private String nib;
    private String merkDagang;
    private String fileFormPengajuan;
    private String fileKtp;
    private String fileFotoProduk;
    private String fotoPu;
    private Integer status;
    private String verifyBy;
    private ZonedDateTime verifyOn;
    private String noSertifikat;
    private LocalDate tglSertifikat;
    private LocalDate tglExp;
    private Provinsi provinsi;
    private Kabupaten kabupaten;
    private DaftarProduk daftarProduk;
    private Long provinsiid;
    private String provinsinama;
    private Long kabupatenid;
    private String kabupatennama;
    private Long daftarpordukid;
    private String daftarporduknama;
    
    public PengajuanDTO() {}
    
	public PengajuanDTO(Long id, String noPengajuan, LocalDate tglPengajuan, String nama, String nik, String alamat,
			String kodepos, String email, String noHp, String namaUsaha, String alamatUsaha, String nib,
			String merkDagang, String fileFormPengajuan, String fileKtp, String fileFotoProduk, String fotoPu,
			Integer status, String verifyBy, ZonedDateTime verifyOn, String noSertifikat, LocalDate tglSertifikat,
			LocalDate tglExp, Long provinsiid, String provinsinama, Long kabupatenid, String kabupatennama,
			Long daftarpordukid, String daftarporduknama) {
		this.id = id;
		this.noPengajuan = noPengajuan;
		this.tglPengajuan = tglPengajuan;
		this.nama = nama;
		this.nik = nik;
		this.alamat = alamat;
		this.kodepos = kodepos;
		this.email = email;
		this.noHp = noHp;
		this.namaUsaha = namaUsaha;
		this.alamatUsaha = alamatUsaha;
		this.nib = nib;
		this.merkDagang = merkDagang;
		this.fileFormPengajuan = fileFormPengajuan;
		this.fileKtp = fileKtp;
		this.fileFotoProduk = fileFotoProduk;
		this.fotoPu = fotoPu;
		this.status = status;
		this.verifyBy = verifyBy;
		this.verifyOn = verifyOn;
		this.noSertifikat = noSertifikat;
		this.tglSertifikat = tglSertifikat;
		this.tglExp = tglExp;
		this.provinsiid = provinsiid;
		this.provinsinama = provinsinama;
		this.kabupatenid = kabupatenid;
		this.kabupatennama = kabupatennama;
		this.daftarpordukid = daftarpordukid;
		this.daftarporduknama = daftarporduknama;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNoPengajuan() {
		return noPengajuan;
	}
	public void setNoPengajuan(String noPengajuan) {
		this.noPengajuan = noPengajuan;
	}
	public LocalDate getTglPengajuan() {
		return tglPengajuan;
	}
	public void setTglPengajuan(LocalDate tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getKodepos() {
		return kodepos;
	}
	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getNamaUsaha() {
		return namaUsaha;
	}
	public void setNamaUsaha(String namaUsaha) {
		this.namaUsaha = namaUsaha;
	}
	public String getAlamatUsaha() {
		return alamatUsaha;
	}
	public void setAlamatUsaha(String alamatUsaha) {
		this.alamatUsaha = alamatUsaha;
	}
	public String getNib() {
		return nib;
	}
	public void setNib(String nib) {
		this.nib = nib;
	}
	public String getMerkDagang() {
		return merkDagang;
	}
	public void setMerkDagang(String merkDagang) {
		this.merkDagang = merkDagang;
	}
	public String getFileFormPengajuan() {
		return fileFormPengajuan;
	}
	public void setFileFormPengajuan(String fileFormPengajuan) {
		this.fileFormPengajuan = fileFormPengajuan;
	}
	public String getFileKtp() {
		return fileKtp;
	}
	public void setFileKtp(String fileKtp) {
		this.fileKtp = fileKtp;
	}
	public String getFileFotoProduk() {
		return fileFotoProduk;
	}
	public void setFileFotoProduk(String fileFotoProduk) {
		this.fileFotoProduk = fileFotoProduk;
	}
	public String getFotoPu() {
		return fotoPu;
	}
	public void setFotoPu(String fotoPu) {
		this.fotoPu = fotoPu;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getVerifyBy() {
		return verifyBy;
	}
	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}
	public ZonedDateTime getVerifyOn() {
		return verifyOn;
	}
	public void setVerifyOn(ZonedDateTime verifyOn) {
		this.verifyOn = verifyOn;
	}
	public String getNoSertifikat() {
		return noSertifikat;
	}
	public void setNoSertifikat(String noSertifikat) {
		this.noSertifikat = noSertifikat;
	}
	public LocalDate getTglSertifikat() {
		return tglSertifikat;
	}
	public void setTglSertifikat(LocalDate tglSertifikat) {
		this.tglSertifikat = tglSertifikat;
	}
	public LocalDate getTglExp() {
		return tglExp;
	}
	public void setTglExp(LocalDate tglExp) {
		this.tglExp = tglExp;
	}
	public Provinsi getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}
	public Kabupaten getKabupaten() {
		return kabupaten;
	}
	public void setKabupaten(Kabupaten kabupaten) {
		this.kabupaten = kabupaten;
	}
	public DaftarProduk getDaftarProduk() {
		return daftarProduk;
	}
	public void setDaftarProduk(DaftarProduk daftarProduk) {
		this.daftarProduk = daftarProduk;
	}
	public Long getProvinsiid() {
		return provinsiid;
	}
	public void setProvinsiid(Long provinsiid) {
		this.provinsiid = provinsiid;
	}
	public String getProvinsinama() {
		return provinsinama;
	}
	public void setProvinsinama(String provinsinama) {
		this.provinsinama = provinsinama;
	}
	public Long getKabupatenid() {
		return kabupatenid;
	}
	public void setKabupatenid(Long kabupatenid) {
		this.kabupatenid = kabupatenid;
	}
	public String getKabupatennama() {
		return kabupatennama;
	}
	public void setKabupatennama(String kabupatennama) {
		this.kabupatennama = kabupatennama;
	}
	public Long getDaftarpordukid() {
		return daftarpordukid;
	}
	public void setDaftarpordukid(Long daftarpordukid) {
		this.daftarpordukid = daftarpordukid;
	}
	public String getDaftarporduknama() {
		return daftarporduknama;
	}
	public void setDaftarporduknama(String daftarporduknama) {
		this.daftarporduknama = daftarporduknama;
	}
	
	@Override
	public String toString() {
		return "PengajuanDTO [id=" + id + ", noPengajuan=" + noPengajuan + ", tglPengajuan=" + tglPengajuan + ", nama="
				+ nama + ", nik=" + nik + ", alamat=" + alamat + ", kodepos=" + kodepos + ", email=" + email + ", noHp="
				+ noHp + ", namaUsaha=" + namaUsaha + ", alamatUsaha=" + alamatUsaha + ", nib=" + nib + ", merkDagang="
				+ merkDagang + ", fileFormPengajuan=" + fileFormPengajuan + ", fileKtp=" + fileKtp + ", fileFotoProduk="
				+ fileFotoProduk + ", fotoPu=" + fotoPu + ", status=" + status + ", verifyBy=" + verifyBy
				+ ", verifyOn=" + verifyOn + ", noSertifikat=" + noSertifikat + ", tglSertifikat=" + tglSertifikat
				+ ", tglExp=" + tglExp + ", provinsi=" + provinsi + ", kabupaten=" + kabupaten + ", daftarProduk="
				+ daftarProduk + ", provinsiid=" + provinsiid + ", provinsinama=" + provinsinama + ", kabupatenid="
				+ kabupatenid + ", kabupatennama=" + kabupatennama + ", daftarpordukid=" + daftarpordukid
				+ ", daftarporduknama=" + daftarporduknama + "]";
	}
}
