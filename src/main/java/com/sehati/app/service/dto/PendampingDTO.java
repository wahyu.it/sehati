package com.sehati.app.service.dto;

import java.io.Serializable;

public class PendampingDTO  implements Serializable {
	
    private Long id;

    private String nama;

    private Integer saldo;

    private String username;
    
    private String password;

    private Integer freeSaldo;

    private Boolean status;
    
    public PendampingDTO() {};
    
	public PendampingDTO(Long id, String nama, Integer saldo, String username, String password, Integer freeSaldo,
			Boolean status) {
		this.id = id;
		this.nama = nama;
		this.saldo = saldo;
		this.username = username;
		this.password = password;
		this.freeSaldo = freeSaldo;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getFreeSaldo() {
		return freeSaldo;
	}

	public void setFreeSaldo(Integer freeSaldo) {
		this.freeSaldo = freeSaldo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PendampingDTO [id=" + id + ", nama=" + nama + ", saldo=" + saldo + ", username=" + username
				+ ", password=" + password + ", freeSaldo=" + freeSaldo + ", status=" + status + "]";
	}
}
