package com.sehati.app.service.dto;

import java.time.LocalDate;

public class SummaryPengajuanDTO {
	
    private String namaPendamping;
    private String namaOperator;
    private Long jmlPengajuan;
    private Long jmlPembayaran;
    private Long jmlComplete;
    private Long jmlProses;
    private Long jmlSubmit;
    private Long jmlReject;
    private LocalDate bulan;
    
	public SummaryPengajuanDTO(String namaPendamping, Long jmlPengajuan, Long jmlPembayaran, Long jmlComplete,
			Long jmlProses, Long jmlSubmit, Long jmlReject, LocalDate bulan) {
		this.namaPendamping = namaPendamping;
		this.jmlPengajuan = jmlPengajuan;
		this.jmlPembayaran = jmlPembayaran;
		this.jmlComplete = jmlComplete;
		this.jmlProses = jmlProses;
		this.jmlSubmit = jmlSubmit;
		this.jmlReject = jmlReject;
		this.bulan = bulan;
	}
	
	public SummaryPengajuanDTO(String namaOperator) {
		this.namaOperator = namaOperator;
	}

	public SummaryPengajuanDTO() {}

	public String getNamaPendamping() {
		return namaPendamping;
	}
	public void setNamaPendamping(String namaPendamping) {
		this.namaPendamping = namaPendamping;
	}
	public String getNamaOperator() {
		return namaOperator;
	}
	public void setNamaOperator(String namaOperator) {
		this.namaOperator = namaOperator;
	}
	public Long getJmlPengajuan() {
		return jmlPengajuan;
	}
	public void setJmlPengajuan(Long jmlPengajuan) {
		this.jmlPengajuan = jmlPengajuan;
	}
	public Long getJmlPembayaran() {
		return jmlPembayaran;
	}
	public void setJmlPembayaran(Long jmlPembayaran) {
		this.jmlPembayaran = jmlPembayaran;
	}
	public Long getJmlComplete() {
		return jmlComplete;
	}
	public void setJmlComplete(Long jmlComplete) {
		this.jmlComplete = jmlComplete;
	}
	public Long getJmlProses() {
		return jmlProses;
	}
	public void setJmlProses(Long jmlProses) {
		this.jmlProses = jmlProses;
	}
	public Long getJmlSubmit() {
		return jmlSubmit;
	}
	public void setJmlSubmit(Long jmlSubmit) {
		this.jmlSubmit = jmlSubmit;
	}
	public Long getJmlReject() {
		return jmlReject;
	}
	public void setJmlReject(Long jmlReject) {
		this.jmlReject = jmlReject;
	}
	
	public LocalDate getBulan() {
		return bulan;
	}

	public void setBulan(LocalDate bulan) {
		this.bulan = bulan;
	}

	@Override
	public String toString() {
		return "SummaryPengajuanDTO [namaPendamping=" + namaPendamping + ", namaOperator=" + namaOperator
				+ ", jmlPengajuan=" + jmlPengajuan + ", jmlPembayaran=" + jmlPembayaran + ", jmlComplete=" + jmlComplete
				+ ", jmlProses=" + jmlProses + ", jmlSubmit=" + jmlSubmit + ", jmlReject=" + jmlReject + ", bulan="
				+ bulan + "]";
	}
}
