package com.sehati.app.service.dto;

public class CustomerDetailsDTO {

    private String email;

    private String full_name;

    private String phone;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "CustomerDetailsDTO [email=" + email + ", full_name=" + full_name + ", phone=" + phone + "]";
	}
}
