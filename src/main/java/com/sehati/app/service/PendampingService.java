package com.sehati.app.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sehati.app.domain.EntityUser;
import com.sehati.app.domain.Lembaga;
import com.sehati.app.domain.Pendamping;
import com.sehati.app.domain.util.RecordStatus;
import com.sehati.app.repository.EntityUserRepository;
import com.sehati.app.security.SecurityUtils;


@Service
public class PendampingService {

    private final Logger log = LoggerFactory.getLogger(PendampingService.class);

    private final EntityUserRepository entityUserRepository;
    
    public PendampingService(EntityUserRepository entityUserRepository) {
		this.entityUserRepository = entityUserRepository;
	}
    
    public Pendamping findByPendampingLogin() {
    	Pendamping pendamping = null;

        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(
            RecordStatus.ACTIVE.value(),
            SecurityUtils.getCurrentUserLogin().get()
        );

        if(entityUser != null) {
        	pendamping = entityUser.getPendamping();
        }

        return pendamping;
    }
    
    public Lembaga findByLembagaLogin() {
    	Lembaga lembaga = null;

        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(
            RecordStatus.ACTIVE.value(),
            SecurityUtils.getCurrentUserLogin().get()
        );

        if(entityUser != null) {
        	lembaga = entityUser.getLembaga();
        }

        return lembaga;
    }
    

    
    public List<Lembaga> findByLembagaEntityUser() {
    	List<Lembaga> listLembaga = new ArrayList<Lembaga>();

        List<EntityUser> entityUsers = entityUserRepository.findByType(1);
        
        for (EntityUser entityUser : entityUsers) {
        	listLembaga.add(entityUser.getLembaga());
		}

        return listLembaga;
    }
}
