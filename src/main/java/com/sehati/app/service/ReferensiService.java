package com.sehati.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sehati.app.domain.Referensi;
import com.sehati.app.repository.ReferensiRepository;

@Service
public class ReferensiService {

    private final Logger log = LoggerFactory.getLogger(ReferensiService.class);
    
    private final static String applicationProp = "APPLICATION_PROPERTIES";
    
    private final ReferensiRepository referensiRepository;
    
    private Map<String, String> map = new HashMap<String, String>();
	
	public ReferensiService (ReferensiRepository referensiRepository) {
		this.referensiRepository = referensiRepository;
	}

    public String applicationProp(String name) {
    	return referensiRepository.findByCategoryAndName(applicationProp, name).get(0).getValue();
//    	return map.get(concat(applicationProp, name));
    }
    
    public String valueOf(String group, String name) {
    	return map.get(concat(group, name));
    }
    
    public int intValue(String group, String name) {
    	return Integer.parseInt(map.get(concat(group, name)));
    }
    
    public long longValue(String group, String name) {
    	return Long.parseLong(map.get(concat(group, name)));
    }
    
    public boolean booleanValue(String group, String name) {
    	return Boolean.valueOf(map.get(concat(group, name)));
    }
    
    public static String concat(String... strings) {
    	StringBuilder sb = new StringBuilder();
    	for (String string : strings) {
			sb.append(".").append(string);
		}
    	
    	return sb.deleteCharAt(0).toString();
    }

    public String applicationProp(String name, String usedFor) {
		return getValue(applicationProp, name, usedFor);
    }
    
    public String getValue(String category, String name, String usedFor) {
		List<Referensi> refs = referensiRepository.findByCategoryAndNameAndUsedFor(category, name, usedFor);
    	return refs != null && refs.size() > 0 ? refs.get(0).getValue()  : null;
    }
    
}
