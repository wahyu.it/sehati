package com.sehati.app.service;

import java.util.List;
import java.util.Optional;

import com.sehati.app.domain.EntityUser;

/**
 * Service Interface for managing {@link com.sehati.app.domain.EntityUser}.
 */
public interface EntityUserService {
    /**
     * Save a entityUser.
     *
     * @param entityUser the entity to save.
     * @return the persisted entity.
     */
    EntityUser save(EntityUser entityUser);

    /**
     * Updates a entityUser.
     *
     * @param entityUser the entity to update.
     * @return the persisted entity.
     */
    EntityUser update(EntityUser entityUser);

    /**
     * Partially updates a entityUser.
     *
     * @param entityUser the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EntityUser> partialUpdate(EntityUser entityUser);

    /**
     * Get all the entityUsers.
     *
     * @return the list of entities.
     */
    List<EntityUser> findAll();

    /**
     * Get the "id" entityUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EntityUser> findOne(Long id);

    /**
     * Delete the "id" entityUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
