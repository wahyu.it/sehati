package com.sehati.app.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sehati.app.config.ApplicationProperties;
import com.sehati.app.domain.Pengajuan;
import com.sehati.app.repository.PengajuanRepository;

@Service
public class PengajuanService {

    private final Logger log = LoggerFactory.getLogger(PendampingService.class);
    
    private final ApplicationProperties applicationProperties;
    
    private final PengajuanRepository pengajuanRepository;
    
    public PengajuanService(ApplicationProperties applicationProperties, PengajuanRepository pengajuanRepository) {
    	this.applicationProperties = applicationProperties;
    	this.pengajuanRepository = pengajuanRepository;
    };
    
    public String uploadfile (MultipartFile file, Pengajuan pengajuan, String tipe) throws IOException {
    	log.debug("upload service tipe {}, penajuan id {}, file {}", tipe, pengajuan.getId(), file);
    	
    	String baseDirectory = applicationProperties.getDomain().getBaseDirectory()
				+ applicationProperties.getDirectory().getBerkas();
    	String filename = file.getOriginalFilename();
    	String uploadFile = null;
    	int index = filename.lastIndexOf('.');
    	if (index > 0) {
    		String extension = filename.substring(index+1);
  			uploadFile = pengajuan.getId() + "-"+ tipe + "." + extension;
    	}
    	

		String dirPath = baseDirectory + LocalDate.now().toString();
		log.debug("dirPath: " + dirPath);
		File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdir();
		}
		
		String filePath = null;

		OutputStream out = null;
		InputStream fileContent = null;

		try {
			File outFile = new File(dir, uploadFile);
			if (outFile.exists()) {
				FileUtils.deleteQuietly(outFile);
			}

			out = new FileOutputStream(outFile);
			fileContent = file.getInputStream();

			int read = 0;
			final byte[] bytes = new byte[1024];
			while ((read = fileContent.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			
			filePath = LocalDate.now().toString() + "/" + uploadFile;
			
			if ("ktp".equals(tipe)) {
				pengajuan.setFileKtp(filePath);
			} else if ("formulir".equals(tipe)) {
				pengajuan.setFileFormPengajuan(filePath);
			} else if ("fileproduk".equals(tipe)) {
				pengajuan.setFileFotoProduk(filePath);
			} else {
				pengajuan.setFotoPu(filePath);
			} 

			pengajuanRepository.save(pengajuan);
			
			out.close();
			fileContent.close();
			return filePath;
		} catch (Exception e) {
			if (out != null) out.close();
			if (fileContent != null) fileContent.close();
		}
		
		return filePath;
    	
    }

}
