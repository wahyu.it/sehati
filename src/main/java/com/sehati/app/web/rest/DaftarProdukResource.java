package com.sehati.app.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.domain.DaftarProduk;
import com.sehati.app.repository.DaftarProdukRepository;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class DaftarProdukResource {
	
    private final Logger log = LoggerFactory.getLogger(DaftarProdukResource.class);
    
    private final DaftarProdukRepository daftarProdukRepository;

    public DaftarProdukResource(DaftarProdukRepository daftarProdukRepository) {
    	this.daftarProdukRepository = daftarProdukRepository;
    }
    
    @GetMapping("/daftar-produk/getall")
    public ResponseEntity<List<DaftarProduk>> getAllProduk(Pageable pageable) {
        log.debug("REST request to get a page of Produk");
        
        Page<DaftarProduk> page = daftarProdukRepository.findAll(pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
