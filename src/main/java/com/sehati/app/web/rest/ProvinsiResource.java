package com.sehati.app.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.domain.Provinsi;
import com.sehati.app.repository.ProvinsiRepository;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class ProvinsiResource {

    private final Logger log = LoggerFactory.getLogger(ProvinsiResource.class);
    
    private final ProvinsiRepository provinsiRepository;

    public ProvinsiResource(ProvinsiRepository provinsiRepository) {
    	this.provinsiRepository = provinsiRepository;
    }
    
    @GetMapping("/provinsi/getall")
    public ResponseEntity<List<Provinsi>> getAllProvinsi(Pageable pageable) {
        log.debug("REST request to get a page of Provinsi");
        
        Page<Provinsi> page = provinsiRepository.findAll(pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
