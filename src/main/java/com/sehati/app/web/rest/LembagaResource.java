package com.sehati.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.sehati.app.domain.Lembaga;

import com.sehati.app.domain.Pendamping;
import com.sehati.app.domain.User;
import com.sehati.app.repository.LembagaRepository;
import com.sehati.app.repository.PendampingRepository;
import com.sehati.app.security.AuthoritiesConstants;
import com.sehati.app.service.dto.UserDTO;
import com.sehati.app.web.rest.errors.BadRequestAlertException;
import com.sehati.app.web.rest.errors.EmailAlreadyUsedException;
import com.sehati.app.web.rest.errors.LoginAlreadyUsedException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api/lembaga")
public class LembagaResource {
	
    private final Logger log = LoggerFactory.getLogger(LembagaResource.class);
    
    private static final String ENTITY_NAME = "lembaga";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
    private final LembagaRepository lembagaRepository;
    
    public LembagaResource(LembagaRepository lembagaRepository) {
    	this.lembagaRepository = lembagaRepository;
    }
    
    @GetMapping("/getall")
    public ResponseEntity<List<Lembaga>> getAll(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            Pageable pageable) {
        log.debug("REST request to get a page of Pendamping");
        
        Page<Lembaga> page = null;
        
        if ("snama".equals(skey) && svalue != null ) {
            page = lembagaRepository.findAll(pageable);
        } else {
            page = lembagaRepository.findAll(pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @PostMapping("/create")
    public ResponseEntity<Lembaga> createLembaga(@Valid @RequestBody Lembaga lembaga) throws URISyntaxException {
        log.debug("REST request to save lembaga : {}", lembaga);

        if (lembaga.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
        }  else {
            Lembaga lemb = new Lembaga();
            lemb.setName(lembaga.getName());
            
            lembagaRepository.save(lemb);
            return ResponseEntity.created(new URI("/api/lembaga/" + lemb.getId()))
                .headers(HeaderUtil.createAlert(applicationName,  "userManagement.created", lemb.getName()))
                .body(lemb);
        }
    }

}
