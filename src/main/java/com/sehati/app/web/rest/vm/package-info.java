/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sehati.app.web.rest.vm;
