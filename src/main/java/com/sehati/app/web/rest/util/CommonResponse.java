package com.sehati.app.web.rest.util;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;

import com.sehati.app.util.DateUtils;
import com.sehati.app.util.ResponseStatus;
import com.sehati.app.util.StringUtils;

public class CommonResponse implements Serializable {
	private static final long serialVersionUID = 1L;

    private String service;
    private String parameter;
    private ZonedDateTime startTime;
    private ZonedDateTime endTime;
    private String status;
    private String message;
    private List<String> logs;
    private String errorStack;
    private long elapsedTime;
    
    public CommonResponse(String service, String parameter) {
    	this.init(service,parameter,null);
    }
    
    public CommonResponse(String service, String parameter, ZonedDateTime startTime) {
    	this.init(service,parameter,startTime);
    }
    
    //TODO TOBE REMOVED
    public CommonResponse(String service, String parameter, long startTimeStamp, String status, String message) {
    	this.service = service;
    	this.parameter = parameter;
    	this.startTime = DateUtils.nowDateTime();
    	this.status = status;
    	this.message = message;
    }
    
    public void init(String service) {
    	init(service, null, null);
    }
    
    public void init(String service, String parameter, ZonedDateTime startTime) {
    	this.service = service;
    	this.parameter = parameter;
    	this.startTime = startTime != null ? startTime : DateUtils.nowDateTime();
    }
    
    public CommonResponse complete(String status) {
    	this.complete(status, null, null, null);
    	return this;
    }
    
    public CommonResponse complete(ResponseStatus status) {
    	this.complete(status.value(), null, null, null);
    	return this;
    }
    
    public CommonResponse complete(String status, String message) {
    	this.complete(status, message, null, null);
    	return this;
    }
    
    public CommonResponse complete(ResponseStatus status, String message) {
    	this.complete(status.value(), message, null, null);
    	return this;
    }
    
    public CommonResponse complete(String status, String message, List<String> logs) {
    	this.complete(status, message, logs, null);
    	return this;
    }
    
    public CommonResponse complete(ResponseStatus status, String message, List<String> logs) {
    	this.complete(status.value(), message, logs, null);
    	return this;
    }
    
    public CommonResponse complete(String status, String message, String errorStack) {
    	this.complete(status, message, null, errorStack);
    	return this;
    }
    
    public CommonResponse complete(ResponseStatus status, String message, String errorStack) {
    	this.complete(status.value(), message, null, errorStack);
    	return this;
    }
    
    public void complete(String status, String message, List<String> logs, String errorStack) {
    	this.status = status;
    	this.message = message;
    	this.endTime = DateUtils.nowDateTime();
    	this.elapsedTime = DateUtils.diffInMillis(this.startTime!=null ? this.startTime : this.endTime, this.endTime);
    	this.logs = logs;
    	this.errorStack = errorStack;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public List<String> getLogs() {
		return logs;
	}

	public void setLogs(List<String> logs) {
		this.logs = logs;
	}

	public String getErrorStack() {
		return errorStack;
	}

	public void setErrorStack(String errorStack) {
		this.errorStack = errorStack;
	}
	
	public boolean isSuccess() {
		return ResponseStatus.COMPLETED.value().equals(status);
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommonResponse obj = (CommonResponse) o;

        if ( ! Objects.equals(service + "?" + parameter, obj.service + "?" + obj.parameter)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(service + "?" + parameter);
    }

    @Override
    public String toString() {
    	String start = startTime!=null ? DateUtils.formatZonedDateTime(startTime, DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2) : null;
    	String end = endTime!=null ? DateUtils.formatZonedDateTime(endTime, DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2) : null;
    	
    	String log = null;
    	if(logs!=null && !logs.isEmpty()) {
        	StringBuffer logSb = new StringBuffer();
    		for(String l : logs) {
    			logSb.append("\n   	\t").append(l);
    		}
    		
    		log = logSb.toString();
    	}
    	
    	StringBuffer sb = new StringBuffer();
    	sb.append("\nService     \t*").append(service).append("*");
    	sb.append("\nParameter\t").append(parameter);
    	sb.append("\nStart         \t").append(start);
    	sb.append("\nEnd           \t").append(end);
    	sb.append("\nDiff           \t").append(StringUtils.formatElapsedMillis(elapsedTime));
    	sb.append("\nStatus       \t*").append(status).append("*");
    	if(message != null)
    		sb.append("\nMessage   \t").append("```").append(message).append("```");
    	if(errorStack != null)
    		sb.append("\nError         \t").append("```").append(errorStack).append("```");
    	if(log != null)
    		sb.append("\nLogs          \t").append("```").append(log).append("```");
    	sb.append("\n");

    	String text = sb.toString();
    	
    	return text.replaceAll("&", "%26amp;").replaceAll("<", "%26lt;").replaceAll(">", "%26gt;");
    }
}
