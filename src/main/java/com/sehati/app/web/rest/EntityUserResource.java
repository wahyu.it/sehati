package com.sehati.app.web.rest;

import com.sehati.app.domain.EntityUser;
import com.sehati.app.domain.User;
import com.sehati.app.repository.EntityUserRepository;
import com.sehati.app.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.sehati.app.domain.EntityUser}.
 */
@RestController
@RequestMapping("/api/entity-users")
public class EntityUserResource {

    private final Logger log = LoggerFactory.getLogger(EntityUserResource.class);

    private static final String ENTITY_NAME = "entityUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EntityUserRepository entityUserRepository;

    public EntityUserResource(EntityUserRepository entityUserRepository) {
        this.entityUserRepository = entityUserRepository;
    }

    /**
     * {@code POST  /entity-users} : Create a new entityUser.
     *
     * @param entityUser the entityUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new entityUser, or with status {@code 400 (Bad Request)} if the entityUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<EntityUser> createEntityUser(@RequestBody EntityUser entityUser) throws URISyntaxException {
        log.debug("REST request to save EntityUser : {}", entityUser);
        if (entityUser.getId() != null) {
            throw new BadRequestAlertException("A new entityUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EntityUser result = entityUserRepository.save(entityUser);
        return ResponseEntity
            .created(new URI("/api/entity-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /entity-users/:id} : Updates an existing entityUser.
     *
     * @param id the id of the entityUser to save.
     * @param entityUser the entityUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated entityUser,
     * or with status {@code 400 (Bad Request)} if the entityUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the entityUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<EntityUser> updateEntityUser(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EntityUser entityUser
    ) throws URISyntaxException {
        log.debug("REST request to update EntityUser : {}, {}", id, entityUser);
        if (entityUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, entityUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!entityUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EntityUser result = entityUserRepository.save(entityUser);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, entityUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /entity-users/:id} : Partial updates given fields of an existing entityUser, field will ignore if it is null
     *
     * @param id the id of the entityUser to save.
     * @param entityUser the entityUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated entityUser,
     * or with status {@code 400 (Bad Request)} if the entityUser is not valid,
     * or with status {@code 404 (Not Found)} if the entityUser is not found,
     * or with status {@code 500 (Internal Server Error)} if the entityUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EntityUser> partialUpdateEntityUser(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EntityUser entityUser
    ) throws URISyntaxException {
        log.debug("REST request to partial update EntityUser partially : {}, {}", id, entityUser);
        if (entityUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, entityUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!entityUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EntityUser result = entityUserRepository.save(entityUser);

        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, entityUser.getId().toString()))
                .body(result);
    }

    /**
     * {@code GET  /entity-users} : get all the entityUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of entityUsers in body.
     */
    @GetMapping("")
    public List<EntityUser> getAllEntityUsers() {
        log.debug("REST request to get all EntityUsers");
        return entityUserRepository.findAll();
    }

    /**
     * {@code GET  /entity-users/:id} : get the "id" entityUser.
     *
     * @param id the id of the entityUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the entityUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<EntityUser> getEntityUser(@PathVariable("id") Long id) {
        log.debug("REST request to get EntityUser : {}", id);
        Optional<EntityUser> entityUser = entityUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(entityUser);
    }

    /**
     * {@code DELETE  /entity-users/:id} : delete the "id" entityUser.
     *
     * @param id the id of the entityUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEntityUser(@PathVariable("id") Long id) {
        log.debug("REST request to delete EntityUser : {}", id);
        entityUserRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/operator")
    public List<User> getAllOperator() {
        log.debug("REST request to get all EntityUsers");
        List<EntityUser> entityUser = entityUserRepository.findByRole("ROLE_OPERATOR");
        List<User> users = new ArrayList<User>();
        
        for (EntityUser entity : entityUser) {
        	users.add(entity.getUser());
		}
        
        return users;
    }
}
