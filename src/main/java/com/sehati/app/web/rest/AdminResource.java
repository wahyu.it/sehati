package com.sehati.app.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.domain.Admin;
import com.sehati.app.repository.AdminRepository;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class AdminResource {
	
    private final Logger log = LoggerFactory.getLogger(AdminResource.class);
    
    private final AdminRepository adminRepository;
    
    public AdminResource(AdminRepository adminRepository) {
    	this.adminRepository = adminRepository;
    }

    @GetMapping("/getall")
    public ResponseEntity<List<Admin>> getAllAdmin(Pageable pageable) {
        log.debug("REST request to get a page of Admin");
        Page<Admin> page = adminRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
