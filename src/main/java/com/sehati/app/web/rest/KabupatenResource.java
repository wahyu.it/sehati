package com.sehati.app.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.domain.Kabupaten;
import com.sehati.app.domain.Provinsi;
import com.sehati.app.repository.KabupatenRepository;
import com.sehati.app.repository.ProvinsiRepository;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class KabupatenResource {
	
    private final Logger log = LoggerFactory.getLogger(KabupatenResource.class);

    private final KabupatenRepository kabupatenRepository;

    private final ProvinsiRepository provinsiRepository;
    
    public KabupatenResource(KabupatenRepository kabupatenRepository, ProvinsiRepository provinsiRepository) {
    	this.kabupatenRepository = kabupatenRepository;
    	this.provinsiRepository = provinsiRepository;
    }
    
    @GetMapping("/kabupaten/getall")
    public ResponseEntity<List<Kabupaten>> getAllKabupaten(Pageable pageable) {
        log.debug("REST request to get a page of Kabupaten");
        
        Page<Kabupaten> page = kabupatenRepository.findAll(pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/kabupaten/provinsi/{id}")
    public ResponseEntity<List<Kabupaten>> getbyProvinsi(
    		@PathVariable("id") Long id,
    		Pageable pageable) {
        log.debug("REST request to get a page of getbyProvinsi");
        
        Provinsi provinsi = null;
        if (id != null) {
        	provinsi = provinsiRepository.findById(id).get();
        }
        
        Page<Kabupaten> page = kabupatenRepository.findByProvinsi(provinsi, pageable);
        
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
