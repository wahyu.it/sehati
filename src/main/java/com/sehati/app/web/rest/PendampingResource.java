package com.sehati.app.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.domain.Pendamping;
import com.sehati.app.repository.PendampingRepository;
import com.sehati.app.service.PendampingService;
import com.sehati.app.service.dto.PendampingDTO;
import com.sehati.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class PendampingResource {
	
    private final Logger log = LoggerFactory.getLogger(PendampingResource.class);
    
    private static final String ENTITY_NAME = "pendamping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
    private final PendampingRepository pendampingRepository;
    
    private final PendampingService pendampingService;

    public PendampingResource(PendampingService pendampingService, PendampingRepository pendampingRepository) {
    	this.pendampingRepository = pendampingRepository;
    	this.pendampingService = pendampingService;
    }
    
    @GetMapping("/pendamping/getall")
    public ResponseEntity<List<Pendamping>> getAll(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            Pageable pageable) {
        log.debug("REST request to get a page of Pendamping");
        
        Page<Pendamping> page = null;
        
        if ("snama".equals(skey) && svalue != null ) {
            page = pendampingRepository.findByNama(svalue, pageable);
        } else {
            page = pendampingRepository.findAll(pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @PostMapping("/pendamping/active")
    public ResponseEntity<Pendamping> active(@RequestBody PendampingDTO pendampingDTO) throws URISyntaxException {
        log.debug("REST request to Pendamping active {}", pendampingDTO);
        
        if (pendampingDTO.getId() == null) {
            throw new BadRequestAlertException("A new pendamping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        Pendamping pendamping = pendampingRepository.findById(pendampingDTO.getId()).get();
        	if (pendampingDTO.getStatus()) {
        		pendamping.setStatus(false);
        	} else {
        		pendamping.setStatus(true);
        	}
        	pendampingRepository.save(pendamping);
        
        return ResponseEntity.created(new URI("/api/pendamping/edit" + pendamping.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, pendamping.getId().toString()))
                .body(pendamping);
    }
    
    @PostMapping("/pendamping/edit")
    public ResponseEntity<Pendamping> edit(@RequestBody PendampingDTO pendampingDTO) throws URISyntaxException {
        log.debug("REST request to Pendamping Edit {}", pendampingDTO);
        
        if (pendampingDTO.getId() == null) {
            throw new BadRequestAlertException("A new pendamping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        Pendamping pendamping = pendampingRepository.findById(pendampingDTO.getId()).get();
        	pendamping.setNama(pendampingDTO.getNama());
        	pendamping.setFreeSaldo(pendampingDTO.getFreeSaldo());
        	pendamping.setSaldo(pendampingDTO.getSaldo());
        	
        	pendampingRepository.save(pendamping);
        
        return ResponseEntity.created(new URI("/api/pendamping/edit" + pendamping.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, pendamping.getId().toString()))
                .body(pendamping);
    }
    
    @GetMapping("/pendamping/getlogin")
    public Pendamping getlogin(Pageable pageable) {
    
    Pendamping pendamping = pendampingService.findByPendampingLogin();
   
    return pendamping;
    }

}
