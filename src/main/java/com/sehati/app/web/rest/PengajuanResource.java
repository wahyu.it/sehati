package com.sehati.app.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sehati.app.security.AuthoritiesConstants;
import com.sehati.app.security.SecurityUtils;
import com.sehati.app.config.ApplicationProperties;
import com.sehati.app.domain.DaftarProduk;
import com.sehati.app.domain.EntityUser;
import com.sehati.app.domain.Kabupaten;
import com.sehati.app.domain.Lembaga;
import com.sehati.app.domain.Pendamping;
import com.sehati.app.domain.Pengajuan;
import com.sehati.app.domain.Provinsi;
import com.sehati.app.domain.util.PengajuanStatus;
import com.sehati.app.domain.util.RecordStatus;
import com.sehati.app.repository.DaftarProdukRepository;
import com.sehati.app.repository.EntityUserRepository;
import com.sehati.app.repository.KabupatenRepository;
import com.sehati.app.repository.PendampingRepository;
import com.sehati.app.repository.PengajuanRepository;
import com.sehati.app.repository.ProvinsiRepository;
import com.sehati.app.repository.UserRepository;
import com.sehati.app.service.PendampingService;
import com.sehati.app.service.PengajuanService;
import com.sehati.app.service.ReferensiService;
import com.sehati.app.service.dto.PengajuanDTO;
import com.sehati.app.service.dto.SummaryPengajuanDTO;
import com.sehati.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;

import org.springframework.http.MediaType;

@RestController
@RequestMapping("/api/pengajuan")
public class PengajuanResource {

    private final Logger log = LoggerFactory.getLogger(PengajuanResource.class);

    private static final String ENTITY_NAME = "pengajuan";
    private static final String START = "START_NP";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
    private final PengajuanRepository pengajuanRepository;
    
    private final ProvinsiRepository provinsiRepository;
    
    private final KabupatenRepository kabupatenRepository;
    
    private final DaftarProdukRepository daftarProdukRepository;
    
    private final UserRepository userRepository;
    
    private final PendampingService pendampingService;
    
    private final ApplicationProperties applicationProperties;
    
    private final PengajuanService pengajuanService;
    
    private final ReferensiService referensiService;
    
    private final PendampingRepository pendampingRepository;

    private final EntityUserRepository entityUserRepository;

    public PengajuanResource(EntityUserRepository entityUserRepository, PendampingRepository pendampingRepository, ReferensiService referensiService, PengajuanService pengajuanService, ApplicationProperties applicationProperties, PendampingService pendampingService, UserRepository userRepository, DaftarProdukRepository daftarProdukRepository, PengajuanRepository pengajuanRepository, ProvinsiRepository provinsiRepository, KabupatenRepository kabupatenRepository) {
    	this.pengajuanRepository = pengajuanRepository;
    	this.provinsiRepository = provinsiRepository;
    	this.kabupatenRepository = kabupatenRepository;
    	this.daftarProdukRepository = daftarProdukRepository;
    	this.userRepository = userRepository;
    	this.pendampingService = pendampingService;
    	this.applicationProperties = applicationProperties;
    	this.pengajuanService = pengajuanService;
    	this.referensiService = referensiService;
    	this.pendampingRepository = pendampingRepository;
    	this.entityUserRepository = entityUserRepository;
    }
    
    @GetMapping("/getall")
    public ResponseEntity<List<Pengajuan>> getAllPengajuan(Pageable pageable) {
        log.debug("REST request to get a page of Kabupaten");
        
        Page<Pengajuan> page = pengajuanRepository.findAll(pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteTagihan(@PathVariable String id) {
        log.debug("REST request to delete Tagihan : {}", id);
        pengajuanRepository.deleteById(Long.valueOf(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    // search by pendamping
    @GetMapping("/search")
    public ResponseEntity<List<Pengajuan>> search(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            Pageable pageable) {
        log.debug("REST request to get a page of search {} {}", skey, svalue);
        
        Page<Pengajuan> page = null;
        Pendamping pendamping = pendampingService.findByPendampingLogin();
        log.debug("Pendamping" + pendamping.getNama());
        
        if("nomor".equals(skey) && svalue != null) {
        	page = pengajuanRepository.findByNoPengajuanAndPendamping(svalue, pendamping,pageable);
        } else if("namaPu".equals(skey) && svalue != null)  {
        	page = pengajuanRepository.findByNama(svalue, pageable);
        } else if("tglPengajuan".equals(skey) && svalue != null) {
            LocalDate tglPengajuan = LocalDate.parse(svalue);
        	page = pengajuanRepository.findByTglPengajuanAndPendamping(tglPengajuan, pendamping, pageable);
        } else if("blnPengajuan".equals(skey) && svalue != null) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.plusMonths(1);
        	page = pengajuanRepository.findByTglPengajuanBetweenAndPendamping(startDate, endDate, pendamping, pageable);
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.plusMonths(1);
            page = pengajuanRepository.findByTglPengajuanBetweenAndPendamping(startDate, endDate, pendamping, pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/summary/pendamping")
    public ResponseEntity<List<SummaryPengajuanDTO>> searchSummary(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            @RequestParam(value = "spendampingid", required = false) String spendampingid,
            Pageable pageable) {
        log.debug("REST request to get a page of searchSummary {} {} {}", skey, svalue, spendampingid);
        
        Page<SummaryPengajuanDTO> page = null;
        
       if("tglPengajuan".equals(skey) && svalue != null) {
            LocalDate tglPengajuan = LocalDate.parse(svalue);
            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryByDateBetweenAndPendamping(tglPengajuan, tglPengajuan, Long.parseLong(spendampingid), pageable);
            } else {
            	page = pengajuanRepository.findSummaryByDateBetween(tglPengajuan, tglPengajuan, pageable);
            }
        } else if("blnPengajuan".equals(skey) && svalue != null) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.plusMonths(1);
            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryByDateBetweenAndPendamping(startDate, endDate, Long.parseLong(spendampingid), pageable);
            } else {
            	page = pengajuanRepository.findSummaryByDateBetween(startDate, endDate, pageable); 
        	}
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.plusMonths(1);

            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryByDateBetweenAndPendamping(startDate, endDate, Long.parseLong(spendampingid), pageable);
            } else {
                page = pengajuanRepository.findSummaryByDateBetween(startDate, endDate, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/summary/operator")
    public ResponseEntity<List<SummaryPengajuanDTO>> searchSummaryOperator(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            @RequestParam(value = "spendampingid", required = false) String spendampingid,
            Pageable pageable) {
        log.debug("REST request to get a page of searchSummary {} {} {}", skey, svalue, spendampingid);
        
        Page<SummaryPengajuanDTO> page = null;
        
       if("tglPengajuan".equals(skey) && svalue != null) {
            LocalDate tglPengajuan = LocalDate.parse(svalue);
            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryOptByDateBetweenAndOpt(tglPengajuan, tglPengajuan, spendampingid, pageable);
            } else {
            	page = pengajuanRepository.findSummaryOptByDateBetween(tglPengajuan, tglPengajuan, pageable);
            }
        } else if("blnPengajuan".equals(skey) && svalue != null) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.plusMonths(1);
            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryOptByDateBetweenAndOpt(startDate, endDate, spendampingid, pageable);
            } else {
            	page = pengajuanRepository.findSummaryOptByDateBetween(startDate, endDate, pageable); 
        	}
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.plusMonths(1);

            if (!"undefined".equals(spendampingid) && spendampingid != null) {
            	page = pengajuanRepository.findSummaryOptByDateBetweenAndOpt(startDate, endDate, spendampingid, pageable);
            } else {
                page = pengajuanRepository.findSummaryOptByDateBetween(startDate, endDate, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/searchbycase")
    public ResponseEntity<List<Pengajuan>> searchbycase(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            Pageable pageable) {
        log.debug("REST request to get a page of search {} {}", skey, svalue);
        
        Page<Pengajuan> page = null;
        // entity user tipe 2 untuk lembaga yang pisah admin
        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(RecordStatus.ACTIVE.value(), SecurityUtils.getCurrentUserLogin().get());
//        Noted : entity user type 2 admin pisah
//        Noted : entity user type 1 admin gabung
        Boolean pisahAdmin = entityUser.getType() == 2 ? true : false;
        List<Lembaga> listLembaga = new ArrayList<Lembaga>();
        if (!pisahAdmin) {
        	listLembaga = pendampingService.findByLembagaEntityUser();
        }
        if("nomor".equals(skey) && svalue != null) {
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByNoPengajuanAndLembaga(svalue, entityUser.getLembaga(),pageable);
        	} else {
        		if (listLembaga.size() > 0) {
                	page = pengajuanRepository.findByNoPengajuanAndLembagaIn(svalue, listLembaga,pageable);
        		} else {
                	page = pengajuanRepository.findByNoPengajuan(svalue,pageable);
        		}
        	}
        } else if("namaPu".equals(skey) && svalue != null)  {
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByNamaAndLembaga(svalue, entityUser.getLembaga(), pageable);
        	} else {
            	page = pengajuanRepository.findByNamaAndLembagaIn(svalue, listLembaga, pageable);
        	}
        } else if("tglPengajuan".equals(skey) && svalue != null) {
            LocalDate tglPengajuan = LocalDate.parse(svalue);
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByTglPengajuanAndLembaga(tglPengajuan, entityUser.getLembaga(), pageable);
        	} else {
            	page = pengajuanRepository.findByTglPengajuanAndLembagaIn(tglPengajuan, listLembaga, pageable);
        	}
        } else if("blnPengajuan".equals(skey) && svalue != null) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.plusMonths(1).minusDays(1);
            log.debug("search startDate: {}", startDate);
            log.debug("search endDate: {}", endDate);
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByTglPengajuanBetweenAndLembaga(startDate, endDate, entityUser.getLembaga(), pageable);
        	} else {
            	page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaIn(startDate, endDate, listLembaga, pageable);
        	}
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.plusMonths(1).minusDays(1);
            log.debug("default startDate: {}", startDate);
            log.debug("default endDate: {}", endDate);
        	if (pisahAdmin) {
                page = pengajuanRepository.findByTglPengajuanBetweenAndLembaga(startDate, endDate, entityUser.getLembaga(), pageable);
        	} else {
                page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaIn(startDate, endDate, listLembaga, pageable);
        	}
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/laporan/searchbycase")
    public ResponseEntity<List<Pengajuan>> searchbycasereport(
            @RequestParam(value = "skey", required = false) String skey,
            @RequestParam(value = "svalue", required = false) String svalue,
            @RequestParam(value = "spendampingid", required = false) String spendampingid,
            Pageable pageable) {
        log.debug("REST request to get a page of searchbycase {} {} {}", skey, svalue, spendampingid);
        
        Page<Pengajuan> page = null;
        // entity user tipe 2 untuk lembaga yang pisah admin
        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(RecordStatus.ACTIVE.value(), SecurityUtils.getCurrentUserLogin().get());
//        Noted : entity user type 2 admin pisah
//        Noted : entity user type 1 admin gabung
        Boolean pisahAdmin = entityUser.getType() == 2 ? true : false;
        List<Lembaga> listLembaga = new ArrayList<Lembaga>();
        if (!pisahAdmin) {
        	listLembaga = pendampingService.findByLembagaEntityUser();
        }
        
        Integer[] statusArr = new Integer[]{PengajuanStatus.COMPLETE.value(), PengajuanStatus.INVOICE.value()};

        List<Integer> statusList = Arrays.asList(statusArr);
        
        if("nomor".equals(skey) && svalue != null) {
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByNoPengajuanAndLembagaAndStatusIn(svalue, entityUser.getLembaga(), statusList,pageable);
        	} else {
        		if (listLembaga.size() > 0) {
                	page = pengajuanRepository.findByNoPengajuanAndLembagaInAndStatusIn(svalue, listLembaga, statusList,pageable);
        		} else {
                	page = pengajuanRepository.findByNoPengajuanAndStatusIn(svalue, statusList,pageable);
        		}
        	}
        } else if("namaPu".equals(skey) && svalue != null)  {
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByNamaAndLembagaAndStatusIn(svalue, entityUser.getLembaga(), statusList, pageable);
        	} else {
            	page = pengajuanRepository.findByNamaAndLembagaInAndStatusIn(svalue, listLembaga, statusList, pageable);
        	}
        } else if("tglPengajuan".equals(skey) && svalue != null && "undefined".equals(spendampingid)) {
            LocalDate tglPengajuan = LocalDate.parse(svalue);
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByTglPengajuanAndLembagaAndStatusIn(tglPengajuan, entityUser.getLembaga(), statusList, pageable);
        	} else {
            	page = pengajuanRepository.findByTglPengajuanAndLembagaInAndStatusIn(tglPengajuan, listLembaga, statusList, pageable);
        	}
        } else if("blnPengajuan".equals(skey) && svalue != null && "undefined".equals(spendampingid)) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.plusMonths(1).minusDays(1);;
        	if (pisahAdmin) {
            	page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaAndStatusIn(startDate, endDate, entityUser.getLembaga(), statusList, pageable);
        	} else {
            	page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaInAndStatusIn(startDate, endDate, listLembaga, statusList, pageable);
        	}
        } else if (!"undefined".equals(spendampingid) && !spendampingid.isEmpty()) {
        	Pendamping pendamping = pendampingRepository.findById(Long.parseLong(spendampingid)).get();
        	if("tglPengajuan".equals(skey) && svalue != null) {
                LocalDate tglPengajuan = LocalDate.parse(svalue);
            	if (pisahAdmin) {
                	page = pengajuanRepository.findByTglPengajuanAndLembagaAndPendampingAndStatusIn(tglPengajuan, entityUser.getLembaga(), pendamping, statusList, pageable);
            	} else {
                	page = pengajuanRepository.findByTglPengajuanAndLembagaInAndPendampingAndStatusIn(tglPengajuan, listLembaga, pendamping, statusList, pageable);
            	}
            } else if("blnPengajuan".equals(skey) && svalue != null) {
                LocalDate startDate = LocalDate.parse(svalue + "-01");
                LocalDate endDate = startDate.plusMonths(1).minusDays(1);;
            	if (pisahAdmin) {
                	page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaAndPendampingAndStatusIn(startDate, endDate, entityUser.getLembaga(), pendamping, statusList, pageable);
            	} else {
                	page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaInAndPendampingAndStatusIn(startDate, endDate, listLembaga, pendamping, statusList, pageable);
            	}
            } else {
                LocalDate startDate = LocalDate.now().withDayOfMonth(1);
                LocalDate endDate = startDate.plusMonths(1).minusDays(1);;
            	if (pisahAdmin) {
                    page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaAndPendampingAndStatusIn(startDate, endDate, entityUser.getLembaga(), pendamping, statusList, pageable);
            	} else {
                    page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaInAndPendampingAndStatusIn(startDate, endDate, listLembaga, pendamping, statusList, pageable);
            	}
            }
        
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.plusMonths(1).minusDays(1);;
        	if (pisahAdmin) {
                page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaAndStatusIn(startDate, endDate, entityUser.getLembaga(), statusList, pageable);
        	} else {
                page = pengajuanRepository.findByTglPengajuanBetweenAndLembagaInAndStatusIn(startDate, endDate, listLembaga, statusList, pageable);
        	}
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @PostMapping(value = "/save", 
			consumes = MediaType.ALL_VALUE)
    public ResponseEntity<Pengajuan> createPengajuan(
    		@RequestBody PengajuanDTO pengajuanDto) throws URISyntaxException, IOException {
        log.debug("REST request to save pengajuanDto : {}", pengajuanDto);
        if (pengajuanDto.getId() != null) {
            throw new BadRequestAlertException("A new createPengajuan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        Provinsi provinsi = provinsiRepository.findById(pengajuanDto.getProvinsiid()).get();
        Kabupaten kabupaten = kabupatenRepository.findById(pengajuanDto.getKabupatenid()).get();
        DaftarProduk daftarProduk = daftarProdukRepository.findById(pengajuanDto.getDaftarpordukid()).get();
        
        Pendamping pendamping = pendampingService.findByPendampingLogin();
        Lembaga lembaga = pendampingService.findByLembagaLogin();

    	Map<Integer, String> bulan = new HashMap<>();
    	bulan.put(1, "I");
    	bulan.put(2, "II");
    	bulan.put(3, "III");
    	bulan.put(4, "IV");
    	bulan.put(5, "V");
    	bulan.put(6, "VI");
    	bulan.put(7, "VII");
    	bulan.put(8, "VIII");
    	bulan.put(9, "IX");
    	bulan.put(10, "X");
    	bulan.put(11, "XI");
    	bulan.put(12, "XII");
		
        Pengajuan result = new Pengajuan();
        	result.setNama(pengajuanDto.getNama());
        	result.setAlamat(pengajuanDto.getAlamat());
        	result.setEmail(pengajuanDto.getEmail());
        	result.setNik(pengajuanDto.getNik());
        	result.setNib(pengajuanDto.getNib());
        	result.setKodepos(pengajuanDto.getKodepos());
        	result.setAlamatUsaha(pengajuanDto.getAlamatUsaha());
        	result.setNoHp(pengajuanDto.getNoHp());
        	result.setMerkDagang(pengajuanDto.getMerkDagang());
        	result.setNamaUsaha(pengajuanDto.getNamaUsaha());
        	result.setTglPengajuan(LocalDate.now());
        	result.setDaftarProduk(pengajuanDto.getDaftarProduk());
        	result.setProvinsi(provinsi);
        	result.setKabupaten(kabupaten);
        	result.setDaftarProduk(daftarProduk);
        	result.setKab(kabupaten.getName());
        	result.setProv(provinsi.getName());
        	result.setProd(daftarProduk.getName());
        	result.setPendamping(pendamping);
        	result.setDamping(pendamping.getNama());
        	result.setLembaga(lembaga);
        	result.setStatus(2);
            pengajuanRepository.save(result);
            
            String nomorAwal = referensiService.applicationProp(START);
            Long idNomor = Long.parseLong(nomorAwal) + result.getId();
    		String nomor = idNomor + "/B/LDPM/"+ bulan.get(LocalDate.now().getMonthValue())+"/" +LocalDate.now().getYear();
            result.setNoPengajuan(nomor);
            pengajuanRepository.save(result);
            
            if (result != null) {
            	if (pendamping.getFreeSaldo() > 0) {
            		pendamping.setFreeSaldo(pendamping.getFreeSaldo()-1);
            	} else if (pendamping.getSaldo() > 0) {
            		pendamping.setSaldo(pendamping.getFreeSaldo()-1);
            	} else {
            		log.debug("Saldo tidak mencukupi...");
            	}
            	pendampingRepository.save(pendamping);
            	
            }
        return ResponseEntity.created(new URI("/api/pengajuan/save" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    

    
    @PostMapping(value = "/update", 
			consumes = MediaType.ALL_VALUE)
    public ResponseEntity<Pengajuan> updatePengajuan(
    		@RequestBody PengajuanDTO pengajuanDto) throws URISyntaxException, IOException {
        log.debug("REST request to update pengajuanDto : {}", pengajuanDto);
        
        Provinsi provinsi = provinsiRepository.findById(pengajuanDto.getProvinsiid()).get();
        Kabupaten kabupaten = kabupatenRepository.findById(pengajuanDto.getKabupatenid()).get();
        DaftarProduk daftarProduk = daftarProdukRepository.findById(pengajuanDto.getDaftarpordukid()).get();
        
        Pendamping pendamping = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PENDAMPING) ?
        		pendampingService.findByPendampingLogin(): null;

    	Map<Integer, String> bulan = new HashMap<>();
    	bulan.put(1, "I");
    	bulan.put(2, "II");
    	bulan.put(3, "III");
    	bulan.put(4, "IV");
    	bulan.put(5, "V");
    	bulan.put(6, "VI");
    	bulan.put(7, "VII");
    	bulan.put(8, "VIII");
    	bulan.put(9, "IX");
    	bulan.put(10, "X");
    	bulan.put(11, "XI");
    	bulan.put(12, "XII");
		
        Pengajuan result = pengajuanRepository.findById(pengajuanDto.getId()).get();
        	result.setNama(pengajuanDto.getNama());
        	result.setAlamat(pengajuanDto.getAlamat());
        	result.setEmail(pengajuanDto.getEmail());
        	result.setNik(pengajuanDto.getNik());
        	result.setNib(pengajuanDto.getNib());
        	result.setKodepos(pengajuanDto.getKodepos());
        	result.setAlamatUsaha(pengajuanDto.getAlamatUsaha());
        	result.setNoHp(pengajuanDto.getNoHp());
        	result.setMerkDagang(pengajuanDto.getMerkDagang());
        	result.setNamaUsaha(pengajuanDto.getNamaUsaha());
        	result.setTglPengajuan(LocalDate.now());
        	result.setDaftarProduk(pengajuanDto.getDaftarProduk());
        	result.setProvinsi(provinsi);
        	result.setKabupaten(kabupaten);
        	result.setDaftarProduk(daftarProduk);
        	result.setKab(kabupaten.getName());
        	result.setProv(provinsi.getName());
        	result.setProd(daftarProduk.getName());
        	if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PENDAMPING)) {
        		result.setPendamping(pendamping);
        		result.setDamping(pendamping.getNama());
            	result.setStatus(2);
        	}
            pengajuanRepository.save(result);
            
        return ResponseEntity.created(new URI("/api/pengajuan/update" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
	@RequestMapping(value = "/upload-doc/{id}", 
			method = RequestMethod.POST, 
			consumes = MediaType.ALL_VALUE)
	public void upload(
			@PathVariable Long id,
			@Valid @RequestParam(value = "foto", required = false) MultipartFile foto,
			@Valid @RequestParam(value = "kategori", required = false) String kategori) throws IOException {
		log.debug("upload-doc id: {} kategori:{} foto:{} ", id, kategori, foto);
		
		Pengajuan result = pengajuanRepository.findById(id).get();
		if (result != null) {
			if ("ktp".equals(kategori)) {
		     	pengajuanService.uploadfile(foto, result, "ktp");
			} else if ("formulir".equals(kategori)) {
		     	pengajuanService.uploadfile(foto, result, "formulir");
			} else if ("fotoProduk".equals(kategori)) {
		     	pengajuanService.uploadfile(foto, result, "fileproduk");
			} else {
		     	pengajuanService.uploadfile(foto, result, "filepu");
			}
        }
	}

	@RequestMapping(value = "/update-sertifikat/{id}", 
			method = RequestMethod.POST)
	public ResponseEntity<Pengajuan> updateSertifikat(
			@PathVariable Long id,
			@RequestParam(value = "nomor", required = true) String nomor,
			@RequestParam(value = "link", required = true) String link) throws IOException, URISyntaxException {
		log.debug("updateSertifikat id: {} nomor:{} link:{} ", id, nomor, link);
		
		Pengajuan result = pengajuanRepository.findById(id).get();
		if (result != null) {
        }    
		
		if (result != null) {
			result.setNoSertifikat(nomor);
			result.setLinkSertifikat(link);
			result.setStatus(3);
			result.setVerifyOn(ZonedDateTime.now());
			
			pengajuanRepository.save(result);
		}
        return ResponseEntity.created(new URI("/api/pengajuan/update-sertifikat" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
	}

	@RequestMapping(value = "/upload-doc-update/{id}", 
			method = RequestMethod.POST, 
			consumes = MediaType.ALL_VALUE)
	public void upload2(
			@PathVariable Long id,
			@Valid @RequestParam(value = "fileformulir", required = false) MultipartFile fileformulir,
			@Valid @RequestParam(value = "filektp", required = false) MultipartFile filektp,
			@Valid @RequestParam(value = "fileproduk", required = false) MultipartFile fileproduk,
			@Valid @RequestParam(value = "filepu", required = false) MultipartFile filepu) throws IOException {
		log.debug("upload-doc id: {} fileformulir:{} filektp:{} fileproduk: {} filepu: {}", id, fileformulir, filektp, fileproduk, filepu);
		
		Pengajuan result = pengajuanRepository.findById(id).get();
		if (result != null) {
			if (fileformulir != null)
				pengajuanService.uploadfile(fileformulir, result, "formulir");
			if (filektp != null)
				pengajuanService.uploadfile(filektp, result, "ktp");
			if (fileproduk != null)
				pengajuanService.uploadfile(fileproduk, result, "fileproduk");
			if (filepu != null)
				pengajuanService.uploadfile(filepu, result, "filepu");
        }
	}
	
    @GetMapping("/getAdmin/{id}")
    public Pengajuan getlogin(
			@PathVariable Long id) {
    
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
   
    return pengajuan;
   }
	
    @GetMapping("/setAdmin/{id}")
    public Pengajuan setAdmin(
			@PathVariable Long id) {
    	
    	String login = SecurityUtils.getCurrentUserLogin().get();
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	pengajuan.setVerifyBy(login);
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
    @GetMapping("/unSetAdmin/{id}")
    public Pengajuan unSetAdmin(
			@PathVariable Long id) {
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	pengajuan.setVerifyBy(null);
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
    @GetMapping("/reject/{id}")
    public Pengajuan reject(
			@PathVariable Long id) {
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	pengajuan.setStatus(1);
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
    @GetMapping("/complete/{id}")
    public Pengajuan complete(
			@PathVariable Long id) {
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	pengajuan.setStatus(3);
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
    @GetMapping("/submit/{id}")
    public Pengajuan submit(
			@PathVariable Long id) {
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	pengajuan.setStatus(4);
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
    @GetMapping("/invoice/{id}")
    public Pengajuan invoice(
			@PathVariable Long id) {
    	
    	Pengajuan pengajuan = pengajuanRepository.findById(id).get();
    	if (pengajuan.getStatus() == PengajuanStatus.COMPLETE.value()) {
        	pengajuan.setStatus(5);
    	} else {
        	pengajuan.setStatus(3);
    	}
    	
    	pengajuanRepository.save(pengajuan);
   
    return pengajuan;
   }
	
	@RequestMapping(value = "/download", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE, MediaType.ALL_VALUE })
	@ResponseBody
	public FileSystemResource downloadDoc (
			@RequestParam(value = "fileName", required = false) String fileName
			) throws Exception {
		log.debug("REST Download Doc by fileName:{}", fileName);
		
		try {
			
			String baseDirectory = applicationProperties.getDomain().getBaseDirectory()
				+ applicationProperties.getDirectory().getBerkas();
			
			String filePath = baseDirectory +"/"+ fileName;
			File file = new File(filePath);
			
			return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource.downloadDoc File failed caused by " + e.getMessage(), e);
			throw e;
		}
	}
	
//    @GetMapping("/home")
//    public ResponseEntity<List<HomeDTO>> homepangajuan(@ApiParam Pageable pageable) throws URISyntaxException {
//        log.debug("REST request to get a page of homepangajuan");
//        
//        Page<HomeDTO> page = null;//manager
//        Page<HomeDTO> page2 = null;//admin
//        Page<HomeDTO> page3 = null;//pendamping
//
//        boolean isRolePendamping = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PENDAMPING);
//        boolean isRoleOperator = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OPERATOR);
//        boolean isRoleManager = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
//        
//        if (isRolePendamping) {
//        	page3 = null;
//        } else if (isRoleOperator) {
//        	page2 = null;
//        } else {
//        	page = null;
//        }
//        
//        if("nomor".equals(skey) && svalue != null) {
//        	page = pengajuanRepository.findByNoPengajuanAndPendamping(svalue, pendamping,pageable);
//        } else if("namaPu".equals(skey) && svalue != null)  {
//        	page = pengajuanRepository.findByNama(svalue, pageable);
//        } else if("tglPengajuan".equals(skey) && svalue != null) {
//            LocalDate tglPengajuan = LocalDate.parse(svalue);
//        	page = pengajuanRepository.findByTglPengajuanAndPendamping(tglPengajuan, pendamping, pageable);
//        } else if("blnPengajuan".equals(skey) && svalue != null) {
//            LocalDate startDate = LocalDate.parse(svalue + "-01");
//            LocalDate endDate = startDate.plusMonths(1);
//        	page = pengajuanRepository.findByTglPengajuanBetweenAndPendamping(startDate, endDate, pendamping, pageable);
//        } else {
//            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
//            LocalDate endDate = startDate.plusMonths(1);
//            page = pengajuanRepository.findByTglPengajuanBetweenAndPendamping(startDate, endDate, pendamping, pageable);
//        }
//        
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//		return new ResponseEntity<>(
//				(page3 != null) ? page3.getContent(): ((page2 != null) ? page2.getContent(): page.getContent()), 
//				headers, HttpStatus.OK);
//    }
}
