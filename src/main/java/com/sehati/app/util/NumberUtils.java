package com.sehati.app.util;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberUtils {
	public final static NumberFormat FORMATTER_LONG_ID = NumberFormat.getInstance(new Locale("ID"));
	public final static NumberFormat FORMATTER_LONG_EN = NumberFormat.getInstance(new Locale("EN"));
	public final static String REGEX_NUMERIC = "^[-+]?\\d+(\\.\\d+)?$";
	
	public static String format(long number, NumberFormat formatter) {
		return formatter.format(number);
	}
	
	public static String formatNegative(long number, NumberFormat formatter) {
		return "(" + formatter.format(Math.abs(number)) + ")";
	}

	public static boolean isNumeric(String string) {
		return string.matches(REGEX_NUMERIC);
	}
	
//	public static void main(String[] args) {
//		System.out.println(NumberUtils.format(5600, NumberUtils.FORMATTER_LONG_ID));
//	}

	public static String getLatinMonth(int month) {
		switch (month) {
			case 1: return "I";
			case 2: return "II";
			case 3: return "III";
			case 4: return "IV";
			case 5: return "V";
			case 6: return "VI";
			case 7: return "VII";
			case 8: return "VIII";
			case 9: return "IX";
			case 10: return "X";
			case 11: return "XI";
			case 12: return "XII";
			default: return null;
		}
	}
}
