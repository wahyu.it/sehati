package com.sehati.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATE = DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ID"));
  
  public static final DateTimeFormatter FORMATTER_ID_COMPARATION = DateTimeFormatter.ofPattern("EEEE,dd-MM-yyyy", new Locale("ID"));
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_TIME = DateTimeFormatter.ISO_TIME;
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE = DateTimeFormatter.ISO_DATE;
  
  public static final DateTimeFormatter FORMATTER_ISO_OFFSET_DATE_TIME = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V2 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V3 = DateTimeFormatter.ofPattern("yyMMdd");
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V4 = DateTimeFormatter.ofPattern("dd MMMM yyyy");
  
  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATETIME = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm", new Locale("ID"));
  
  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATETIME_V2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", new Locale("ID"));
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATETIME = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_MONTH = DateTimeFormatter.ofPattern("yyyy-MM");
  
  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_MONTH_V2 = DateTimeFormatter.ofPattern("yyMM");
  
  public static final DateTimeFormatter FORMATTER_ISO_YYYYMM = DateTimeFormatter.ofPattern("yyyyMM");
  
  public static final SimpleDateFormat FORMATDATE_ISO_TRANSACTION_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  public static final SimpleDateFormat FORMATDATEISOTRANSACTIONDATETIME = new SimpleDateFormat("yyyyMMddHHmmss");
  
  public static final SimpleDateFormat FORMATDATE_ISO_DATETIME_V2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
  
  public static final DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("HH:mm");
  
  public static String parse(ZonedDateTime d, DateTimeFormatter formatter) {
      return d.format(formatter);
  }
  
  
  public static LocalDate nowDate() {
    return LocalDate.now();
  }
  
  public static String nowDateString() {
    return nowDateString(FORMATTER_ISO_LOCAL_DATE);
  }
  
  public static String nowDateString(DateTimeFormatter formatter) {
    return nowDate().format(formatter);
  }
  
  public static LocalDate parseDate(String localDate, DateTimeFormatter formatter) {
    return LocalDate.parse(localDate, formatter);
  }
  
  public static LocalDate parseDate(String localDate) {
    return parseDate(localDate, FORMATTER_ISO_LOCAL_DATE);
  }
  
  public static String formatDate(String localDate, DateTimeFormatter formatter) {
    LocalDate date = LocalDate.parse(localDate, FORMATTER_ISO_LOCAL_DATE);
    return date.format(formatter);
  }
  
  public static ZonedDateTime nowDateTime() {
    return ZonedDateTime.now();
  }
  
  public static ZonedDateTime startOfDay(ZonedDateTime zonedDateTime) {
    return zonedDateTime.toLocalDate().atStartOfDay(ZoneId.systemDefault());
  }
  
  public static ZonedDateTime endOfDay(ZonedDateTime zonedDateTime) {
    return zonedDateTime.toLocalDate().plusDays(1L).atStartOfDay(ZoneId.systemDefault()).minusSeconds(1L);
  }
  
  public static String nowDateTimeString() {
    return nowDateTimeString(FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
  }
  
  public static String nowDateTimeString(DateTimeFormatter formatter) {
    return nowDateTime().format(formatter);
  }
  
  public static ZonedDateTime parseZonedDateTime(String zonedDateTime, DateTimeFormatter formatter) {
    return ZonedDateTime.parse(zonedDateTime, formatter.withZone(ZoneId.systemDefault()));
  }
  
  public static ZonedDateTime parseTime(LocalDate localDate, String zonedDateTime) {
    return ZonedDateTime.parse(localDate.format(DateTimeFormatter.ISO_DATE) + "T" + zonedDateTime + ":00+07:00[Asia/Bangkok]", DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault()));
  }
  
  public static ZonedDateTime parseTime(String localDate, String zonedDateTime) {
    return ZonedDateTime.parse(localDate + "T" + zonedDateTime + ":00+07:00[Asia/Bangkok]", DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault()));
  }
  
  public static ZonedDateTime parseZonedDateTime(String zonedDateTime) {
    return parseZonedDateTime(zonedDateTime, FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
  }
  
  public static String formatDate(Date date, SimpleDateFormat formatter) {
    return formatter.format(date);
  }
  
  public static String formatZonedDateTime(String zonedDateTime, DateTimeFormatter formatter) {
    ZonedDateTime dateTime = ZonedDateTime.parse(zonedDateTime, FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
    return dateTime.format(formatter);
  }
  
  public static String formatZonedDateTime(ZonedDateTime zonedDateTime, DateTimeFormatter formatter) {
    return zonedDateTime.format(formatter);
  }
  
  public static Date parseDate(String dateString, SimpleDateFormat formatter) throws ParseException {
    return formatter.parse(dateString);
  }
  
  public static String[] getDayAndDate(LocalDate localDate) {
    String dateTimeStr = localDate.format(FORMATTER_ID_COMPARATION);
    return dateTimeStr.split(",");
  }
  
  public static Instant convertZonedDateTimeToInstant(ZonedDateTime zoneDatetime) {
    Instant instant = zoneDatetime.toInstant();
    return instant;
  }
  
  public static String format(Instant instant, String pattern) {
    return DateTimeFormatter.ofPattern(pattern, Locale.getDefault()).withZone(ZoneId.systemDefault()).format(instant);
  }
	
	public static long diffInMillis(ZonedDateTime start, ZonedDateTime end) {
		if(start != null && end != null)
			return ChronoUnit.MILLIS.between(start, end);
		return 0;
	}
}
