package com.sehati.app.util;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.sehati.app.service.dto.CellObject;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

public class ExcelUtil {

    public static void createCol(int colId, int rowStartId, List<Object> objects, CellStyle style, XSSFSheet sheet) {
        if(objects != null && objects.size() > 0) {
            for (int i=0; i<objects.size(); i++) {
            	Row row = sheet.getRow(i+rowStartId);
            	if(row == null) row = sheet.createRow(i+rowStartId);
            	
                Cell cell = row.createCell(colId);
                Object value = objects.get(i);

                if (value == null) {
                    cell.setCellType(CellType.BLANK);
                } else if(value instanceof String) {
                	if(((String) value).length() == 0) cell.setCellType(CellType.BLANK);
                	else cell.setCellValue((String) value);
                } else if(value instanceof ZonedDateTime) {
                    cell.setCellValue(Date.from(((ZonedDateTime) value).toInstant()));
                } else if(value instanceof LocalDate) {
                    cell.setCellValue(Date.from(((LocalDate) value).atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } else if(value instanceof Date) {
                    cell.setCellValue((Date) value);
                } else if(value instanceof Double) {
                    cell.setCellValue((Double) value);
                } else if(value instanceof Integer) {
                    cell.setCellValue(new Double((Integer) value));
                } else if(value instanceof Long) {
                    cell.setCellValue(new Double((Long) value));
                } else if(value instanceof Boolean) {
                    cell.setCellValue((Boolean) value);
                } else {
                    cell.setCellType(CellType.BLANK);
                }

                if(style != null) cell.setCellStyle(style);
            }
        }
    }
    
    public static void createRow(int index, CellObject[] cellObjects, XSSFSheet sheet) {
    	 createRow(index, cellObjects, sheet,(short) 300);
    }

    public static void createRow(int index, CellObject[] cellObjects, XSSFSheet sheet, short height) {
        Row row = sheet.createRow(index);
        row.setHeight(height);

        if(cellObjects != null && cellObjects.length > 0) {
            for (CellObject obj : cellObjects) {
                Cell cell = row.createCell(obj.getIndex());
                Object value = obj.getValue();

                if (value == null) {
                    cell.setCellType(CellType.BLANK);
                } else if(value instanceof String) {
                	if(((String) value).length() == 0) cell.setCellType(CellType.BLANK);
                	else cell.setCellValue((String) value);
                } else if(value instanceof ZonedDateTime) {
                    cell.setCellValue(Date.from(((ZonedDateTime) value).toInstant()));
                } else if(value instanceof LocalDate) {
                    cell.setCellValue(Date.from(((LocalDate) value).atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } else if(value instanceof Date) {
                    cell.setCellValue((Date) value);
                } else if(value instanceof Double) {
                    cell.setCellValue((Double) value);
                } else if(value instanceof Integer) {
                    cell.setCellValue(new Double((Integer) value));
                } else if(value instanceof Long) {
                    cell.setCellValue(new Double((Long) value));
                } else if(value instanceof Boolean) {
                    cell.setCellValue((Boolean) value);
                } else {
                    cell.setCellType(CellType.BLANK);
                }

                if(obj.getStyle() != null) cell.setCellStyle(obj.getStyle());
            }
        }
    }

    public static void createRow(int index, List<CellObject> cellObjects, XSSFSheet sheet) {
        Row row = sheet.createRow(index);

        if(cellObjects != null && cellObjects.size() > 0) {
            for (CellObject obj : cellObjects) {
                Cell cell = row.createCell(obj.getIndex());
                Object value = obj.getValue();

                if (value == null) {
                    cell.setCellType(CellType.BLANK);
                } else if(value instanceof String) {
                	if(((String) value).length() == 0) cell.setCellType(CellType.BLANK);
                	else cell.setCellValue((String) value);
                } else if(value instanceof ZonedDateTime) {
                    cell.setCellValue(Date.from(((ZonedDateTime) value).toInstant()));
                } else if(value instanceof LocalDate) {
                    cell.setCellValue(Date.from(((LocalDate) value).atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } else if(value instanceof Date) {
                    cell.setCellValue((Date) value);
                } else if(value instanceof Double) {
                    cell.setCellValue((Double) value);
                } else if(value instanceof Integer) {
                    cell.setCellValue(new Double((Integer) value));
                } else if(value instanceof Long) {
                    cell.setCellValue(new Double((Long) value));
                } else if(value instanceof Boolean) {
                    cell.setCellValue((Boolean) value);
                } else {
                    cell.setCellType(CellType.BLANK);
                }

                if(obj.getStyle() != null) cell.setCellStyle(obj.getStyle());
            }
        }
    }
    
    public static CellStyle createStyle(XSSFWorkbook workbook, int color, boolean border, boolean bold, boolean center, String format) {
		return createStyle(workbook, color, border, bold, center, false, format);
    }

    public static CellStyle createStyle(XSSFWorkbook workbook, int color, boolean border, boolean bold, boolean center, boolean right, String format) {
        CellStyle style = workbook.createCellStyle();

        if(color != 1) {
            style.setFillForegroundColor((short) color);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }

        if(border) {
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderBottom(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
        }

        if(bold) {
            Font font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
        }

        if(format!=null && !"".equals(format)) {
            CreationHelper createHelper = workbook.getCreationHelper();
            style.setDataFormat(createHelper.createDataFormat().getFormat(format));
        }

        if(center) {
            style.setAlignment(HorizontalAlignment.CENTER);
        }

        if(right) {
            style.setAlignment(HorizontalAlignment.RIGHT);
        }

        return style;
    }
}
