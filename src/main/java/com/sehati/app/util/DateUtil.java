package com.sehati.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DateUtil {
	

	  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATE = DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ID"));
	  
	  public static final DateTimeFormatter FORMATTER_ID_COMPARATION = DateTimeFormatter.ofPattern("EEEE,dd-MM-yyyy", new Locale("ID"));
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_TIME = DateTimeFormatter.ISO_TIME;
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE = DateTimeFormatter.ISO_DATE;
	  
	  public static final DateTimeFormatter FORMATTER_ISO_OFFSET_DATE_TIME = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V2 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V3 = DateTimeFormatter.ofPattern("yyMMdd");
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATE_V4 = DateTimeFormatter.ofPattern("dd MMMM yyyy");
	  
	  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATETIME = DateTimeFormatter.ofPattern("dd MMMM yyyy HH:mm", new Locale("ID"));
	  
	  public static final DateTimeFormatter FORMATTER_ID_LOCAL_DATETIME_V2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", new Locale("ID"));
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_DATETIME = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_MONTH = DateTimeFormatter.ofPattern("yyyy-MM");
	  
	  public static final DateTimeFormatter FORMATTER_ISO_LOCAL_MONTH_V2 = DateTimeFormatter.ofPattern("yyMM");
	  
	  public static final DateTimeFormatter FORMATTER_ISO_YYYYMM = DateTimeFormatter.ofPattern("yyyyMM");
	  
	  public static final SimpleDateFormat FORMATDATE_ISO_TRANSACTION_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	  
	  public static final DateTimeFormatter FORMATDATEISOTRANSACTIONDATETIME = DateTimeFormatter.ofPattern("ddMMMMyyyyHH:mm:ss", new Locale("ID"));
	  
	  public static final SimpleDateFormat FORMATDATE_ISO_DATETIME_V2 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	  
	  public static final DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("HH:mm");
	  
	  public static String parse(ZonedDateTime d, DateTimeFormatter formatter) {
	      return d.format(formatter);
	  }
	  
	  
	  public static LocalDate nowDate() {
	    return LocalDate.now();
	  }
	  
	  public static String nowDateString() {
	    return nowDateString(FORMATTER_ISO_LOCAL_DATE);
	  }
	  
	  public static String nowDateString(DateTimeFormatter formatter) {
	    return nowDate().format(formatter);
	  }
	  
	  public static LocalDate parseDate(String localDate, DateTimeFormatter formatter) {
	    return LocalDate.parse(localDate, formatter);
	  }
	  
	  public static LocalDate parseDate(String localDate) {
	    return parseDate(localDate, FORMATTER_ISO_LOCAL_DATE);
	  }
	  
	  public static String formatDate(String localDate, DateTimeFormatter formatter) {
	    LocalDate date = LocalDate.parse(localDate, FORMATTER_ISO_LOCAL_DATE);
	    return date.format(formatter);
	  }
	  
	  public static ZonedDateTime nowDateTime() {
	    return ZonedDateTime.now();
	  }
	  
	  public static ZonedDateTime startOfDay(ZonedDateTime zonedDateTime) {
	    return zonedDateTime.toLocalDate().atStartOfDay(ZoneId.systemDefault());
	  }
	  
	  public static ZonedDateTime endOfDay(ZonedDateTime zonedDateTime) {
	    return zonedDateTime.toLocalDate().plusDays(1L).atStartOfDay(ZoneId.systemDefault()).minusSeconds(1L);
	  }
	  
	  public static String nowDateTimeString() {
	    return nowDateTimeString(FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
	  }
	  
	  public static String nowDateTimeString(DateTimeFormatter formatter) {
	    return nowDateTime().format(formatter);
	  }
	  
	  public static ZonedDateTime parseZonedDateTime(String zonedDateTime, DateTimeFormatter formatter) {
	    return ZonedDateTime.parse(zonedDateTime, formatter.withZone(ZoneId.systemDefault()));
	  }
	  public static ZonedDateTime parseTime(String localDate, String zonedDateTime) {
	    return ZonedDateTime.parse(localDate + "T" + zonedDateTime + ":00+07:00[Asia/Bangkok]", DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault()));
	  }
	  
	  public static ZonedDateTime parseZonedDateTime(String zonedDateTime) {
	    return parseZonedDateTime(zonedDateTime, FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
	  }
	  
	  public static String formatDate(Date date, SimpleDateFormat formatter) {
	    return formatter.format(date);
	  }
	  
	  public static String formatZonedDateTime(String zonedDateTime, DateTimeFormatter formatter) {
	    ZonedDateTime dateTime = ZonedDateTime.parse(zonedDateTime, FORMATTER_ISO_LOCAL_DATETIME.withZone(ZoneId.systemDefault()));
	    return dateTime.format(formatter);
	  }
	  
	  public static Date parseDate(String dateString, SimpleDateFormat formatter) throws ParseException {
	    return formatter.parse(dateString);
	  }
	  
	  public static String[] getDayAndDate(LocalDate localDate) {
	    String dateTimeStr = localDate.format(FORMATTER_ID_COMPARATION);
	    return dateTimeStr.split(",");
	  }
	  
	  public static Instant convertZonedDateTimeToInstant(ZonedDateTime zoneDatetime) {
	    Instant instant = zoneDatetime.toInstant();
	    return instant;
	  }
	  
	  public static String format(Instant instant, String pattern) {
	    return DateTimeFormatter.ofPattern(pattern, Locale.getDefault()).withZone(ZoneId.systemDefault()).format(instant);
	  }
		
		public static long diffInMillis(ZonedDateTime start, ZonedDateTime end) {
			if(start != null && end != null)
				return ChronoUnit.MILLIS.between(start, end);
			return 0;
		}
	public final static DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public final static DateTimeFormatter FORMATTER_ID_DATE = DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ID"));
	public final static DateTimeFormatter FORMATTER_YYYYMMDD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public final static DateTimeFormatter FORMATTER_DDMM = DateTimeFormatter.ofPattern("ddMM");
    public final static DateTimeFormatter FORMATTER_HHMMSS = DateTimeFormatter.ofPattern("HH:mm:ss");
    public final static DateTimeFormatter FORMATTER_YYMM = DateTimeFormatter.ofPattern("yyMM");
    public final static DateTimeFormatter FORMATTER_DATE_REPORT = DateTimeFormatter.ofPattern("EEEEE, dd-MM-yyyy", new Locale("ID"));
	public final static DateTimeFormatter FORMATTER_ISO_DDMMYY = DateTimeFormatter.ofPattern("ddMMyy");
	private final static Map<String,String> mapDayOfWeekID = new HashMap<String,String>();
	static {
		mapDayOfWeekID.put("0", "Minggu");
		mapDayOfWeekID.put("1", "Senin");
		mapDayOfWeekID.put("2", "Selasa");
		mapDayOfWeekID.put("3", "Rabu");
		mapDayOfWeekID.put("4", "Kamis");
		mapDayOfWeekID.put("5", "Jumat");
		mapDayOfWeekID.put("6", "Sabtu");
	}
	private final static Map<String,String> mapMonthID = new HashMap<String,String>();
	static {
		mapMonthID.put("1", "Januari");
		mapMonthID.put("2", "Februari");
		mapMonthID.put("3", "Maret");
		mapMonthID.put("4", "April");
		mapMonthID.put("5", "Mei");
		mapMonthID.put("6", "Juni");
		mapMonthID.put("7", "Juli");
		mapMonthID.put("8", "Agustus");
		mapMonthID.put("9", "September");
		mapMonthID.put("10", "Oktober");
		mapMonthID.put("11", "November");
		mapMonthID.put("12", "Desember");
	}

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    static {

        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

	public static String formatDate(LocalDate localDate, DateTimeFormatter formatter) {
		return localDate.format(formatter);
	}

	public static String formatZonedDateTime(ZonedDateTime zonedDateTime, DateTimeFormatter formatter) {
		return zonedDateTime.format(formatter);
	}

	public static String getDayOfWeekID(LocalDate date) {
		return mapDayOfWeekID.get("" + date.getDayOfWeek().getValue());
	}

	public static String getMonthID(LocalDate date) {
		return mapMonthID.get("" + date.getMonthValue());
	}

    public final static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }

    public static int getCurrentMonth(){

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int currentMonth = cal.get(Calendar.MONTH);

        return currentMonth;
    }

    public static int getCurrentYear() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int currentYear = cal.get(Calendar.YEAR);

        return currentYear;
    }

    public static ZonedDateTime parseTime(LocalDate localDate, String zonedDateTime) {
        return ZonedDateTime.parse(localDate.format(DateTimeFormatter.ISO_DATE) + "T" + zonedDateTime + ":00+07:00[Asia/Bangkok]", DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault()));
    }
}
