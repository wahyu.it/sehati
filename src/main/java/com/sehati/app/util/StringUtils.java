package com.sehati.app.util;

import java.util.concurrent.TimeUnit;

public class StringUtils {

	public static String substringBeforeLast(String str, String separator) {
		if (str == null || str.length() == 0 || separator == null || separator.length() == 0) {
			return str;
		}
		
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return str;
		}
		return str.substring(0, pos);
	}

	public static String substringAfterLast(String str, String separator) {
		if (str == null || str.length() == 0 || separator == null || separator.length() == 0) {
			return str;
		}
		
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return str;
		}
		return str.substring(pos + 1, str.length());
	}
	
	public static String normalize(String str, String[] chars) {
		if (str == null || str.length() == 0 || chars == null || chars.length == 0) {
			return str;
		}
		
		for (String c : chars) {
			str = str.replaceAll("\\" + c, "");
		}
		
		return str.trim();
	}

	public static String padLeft(int in, int length) {
		return String.format("%0"+length+"d", in);
	}

	public static String padLeft(String text, int length) {
		return String.format("%1$"+ length + "s", text);
	}

	public static String padRight(String text, int length) {
		return String.format("%1$-"+ length + "s", text);
	}

	public static String padLeftText(String text, int length, String ch) {
		if(text == null) return null;
		if(text.length() >= length) return text;
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<(length-text.length()); i++)
			sb.append(ch);
		sb.append(text);
		return sb.toString();
	}
	
	public static String toCamelCase(String init) {
		if (init==null)
			return null;

		if(init.contains(".")) {
			init = init.replaceAll("\\.","\\. ");
			init = init.replaceAll("  "," ");
		}

		final StringBuilder ret = new StringBuilder(init.length());

		for (final String word : init.split(" ")) {
			if (!word.isEmpty()) {
				ret.append(word.substring(0, 1).toUpperCase());
				ret.append(word.substring(1).toLowerCase());
			}
			if (!(ret.length()==init.length()))
				ret.append(" ");
		}

		return ret.toString().trim().replaceAll("  "," ");
	}

    public static String center(String s, int size) {
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < (size - s.length()) / 2; i++) {
            sb.append(" ");
        }
        sb.append(s);
        while (sb.length() < size) {
            sb.append(" ");
        }
        return sb.toString();
    }
    
    private final static long oneDayMillis = 86400000l;
    
    public static String formatElapsedMillis(long millis) {
    	if(millis < 10000l) {
    		return String.format("%d ms", millis);
    	} else {
            long eTime = millis;
            long days = 0;
            if(millis > oneDayMillis) {
                days = TimeUnit.MILLISECONDS.toDays(eTime);
                eTime -= TimeUnit.DAYS.toMillis(days);
            }
            final long hr = TimeUnit.MILLISECONDS.toHours(eTime);
            eTime -= TimeUnit.HOURS.toMillis(hr);
            final long min = TimeUnit.MILLISECONDS.toMinutes(eTime);
            eTime -= TimeUnit.MINUTES.toMillis(min);
            final long sec = TimeUnit.MILLISECONDS.toSeconds(eTime);
            eTime -= TimeUnit.SECONDS.toMillis(sec);
            final long ms = eTime;
            
            if(millis > oneDayMillis)
            	return String.format("%d " + (days>1?"days":"day") + ", %02d:%02d:%02d.%d", days, hr, min, sec, ms);
            else return String.format("%02d:%02d:%02d.%d", hr, min, sec, ms);
    	}
    }
    
    public static String abbreviate( String string, int max) {
        if(string == null || string.length() <= max || max <= 3)  return string;
        
        return string.substring(0, max - 3) + "...";
    }
}
