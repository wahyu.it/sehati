package com.sehati.app.util;

public enum ResponseStatus {
	COMPLETED("COMPLETED"), FAILED("FAILED"), INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR");
	
	private String value;
	
	ResponseStatus (String value) {
		this.value = value;
	}

    public String value() {
        return value;
    }
}
