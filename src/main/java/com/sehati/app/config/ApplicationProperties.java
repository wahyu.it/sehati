package com.sehati.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Sistemsentralisasifidusia.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private final Domain domain = new Domain();
    private final Font font = new Font();
    private final Directory directory = new Directory();
    private final Template template = new Template();
    private final AccessLog accessLog = new AccessLog();
    private final HistoryLog historyLog = new HistoryLog();

    public static class Domain {
        private String baseDirectory;

        private String nomor;
        
        public String getBaseDirectory() {
            return baseDirectory;
        }

        public void setBaseDirectory(String baseDirectory) {
            this.baseDirectory = baseDirectory;
        }

		public String getNomor() {
			return nomor;
		}

		public void setNomor(String nomor) {
			this.nomor = nomor;
		}
        
    }

    public static class Font {
        private String baseFont;
        private String minutaFont;

        public String getBaseFont() {
            return baseFont;
        }

        public void setBaseFont(String baseFont) {
            this.baseFont = baseFont;
        }

        public String getMinutaFont() {
            return minutaFont;
        }

        public void setMinutaFont(String minutaFont) {
            this.minutaFont = minutaFont;
        }
    }

    public static class Directory {
        private String berkas;
        private String buktiLunas;
        private String cancel;
        private String cetakBerkas;
        private String font;
        private String input;
        private String minuta;
        private String orderKerja;
        private String orderKerjaUnsigned;
        private String pengantarBerkas;
        private String salinan;
        private String sertifikat;
        private String skSubstitusi;
        private String tagihan;
        private String tagihanSummary;
        private String template;
        private String voucher;
        private String permohonanMeterai;
        private String reportUpload;
        private String paperlog;
        private String perbaikan;
        private String registrasi;
        private String roya;
        private String sertifikatperubahan;
        private String lampiranobjek;
        private String safin;

        public String getBerkas() {
            return berkas;
        }

        public void setBerkas(String berkas) {
            this.berkas = berkas;
        }

        public String getBuktiLunas() {
            return buktiLunas;
        }

        public void setBuktiLunas(String buktiLunas) {
            this.buktiLunas = buktiLunas;
        }

        public String getCancel() {
            return cancel;
        }

        public void setCancel(String cancel) {
            this.cancel = cancel;
        }

        public String getCetakBerkas() {
            return cetakBerkas;
        }

        public void setCetakBerkas(String cetakBerkas) {
            this.cetakBerkas = cetakBerkas;
        }

        public String getFont() {
            return font;
        }

        public void setFont(String font) {
            this.font = font;
        }

        public String getInput() {
            return input;
        }

        public void setInput(String input) {
            this.input = input;
        }

        public String getMinuta() {
            return minuta;
        }

        public void setMinuta(String minuta) {
            this.minuta = minuta;
        }

        public String getOrderKerja() {
            return orderKerja;
        }

        public void setOrderKerja(String orderKerja) {
            this.orderKerja = orderKerja;
        }

        public String getOrderKerjaUnsigned() {
            return orderKerjaUnsigned;
        }

        public void setOrderKerjaUnsigned(String orderKerjaUnsigned) {
            this.orderKerjaUnsigned = orderKerjaUnsigned;
        }

        public String getPengantarBerkas() {
            return pengantarBerkas;
        }

        public void setPengantarBerkas(String pengantarBerkas) {
            this.pengantarBerkas = pengantarBerkas;
        }

        public String getSalinan() {
            return salinan;
        }

        public void setSalinan(String salinan) {
            this.salinan = salinan;
        }

        public String getSertifikat() {
            return sertifikat;
        }

        public void setSertifikat(String sertifikat) {
            this.sertifikat = sertifikat;
        }

        public String getSkSubstitusi() {
            return skSubstitusi;
        }

        public void setSkSubstitusi(String skSubstitusi) {
            this.skSubstitusi = skSubstitusi;
        }

        public String getTagihan() {
            return tagihan;
        }

        public void setTagihan(String tagihan) {
            this.tagihan = tagihan;
        }

        public String getTagihanSummary() {
            return tagihanSummary;
        }

        public void setTagihanSummary(String tagihanSummary) {
            this.tagihanSummary = tagihanSummary;
        }

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

        public String getVoucher() {
            return voucher;
        }

        public void setVoucher(String voucher) {
            this.voucher = voucher;
        }

        public String getPermohonanMeterai() {
            return permohonanMeterai;
        }

        public void setPermohonanMeterai(String permohonanMeterai) {
            this.permohonanMeterai = permohonanMeterai;
        }

		public String getReportUpload() {
			return reportUpload;
		}

		public void setReportUpload(String reportUpload) {
			this.reportUpload = reportUpload;
		}

		public String getPaperlog() {
			//String a = "reportpaper";
			//paperlog=a;
			return paperlog;
		}

		public void setPaperlog(String paperlog) {
			this.paperlog = paperlog;
		}

		public String getPerbaikan() {
			return perbaikan;
		}

		public void setPerbaikan(String perbaikan) {
			this.perbaikan = perbaikan;
		}

		public String getRegistrasi() {
			return registrasi;
		}

		public void setRegistrasi(String registrasi) {
			this.registrasi = registrasi;
		}

		public String getRoya() {
			return roya;
		}
		
		public void setRoya(String roya) {
			this.roya = roya;
		}

		public String getSertifikatperubahan() {
			return sertifikatperubahan;
		}

		public void setSertifikatperubahan(String sertifikatperubahan) {
			this.sertifikatperubahan = sertifikatperubahan;
		}

		public String getLampiranobjek() {
			return lampiranobjek;
		}

		public void setLampiranobjek(String lampiranobjek) {
			this.lampiranobjek = lampiranobjek;
		}

		public String getSafin() {
			return safin;
		}

		public void setSafin(String safin) {
			this.safin = safin;
		}

    }

    public static class Template {
        private String invoiceJasa;
        private String InvoicePnbp;
        private String orderKerja;
        private String pengantarBerkas;
        private String permohonanMeterai;
        private String serahTerimaBerkas;

        public String getInvoiceJasa() {
            return invoiceJasa;
        }

        public void setInvoiceJasa(String invoiceJasa) {
            this.invoiceJasa = invoiceJasa;
        }

        public String getInvoicePnbp() {
            return InvoicePnbp;
        }

        public void setInvoicePnbp(String invoicePnbp) {
            InvoicePnbp = invoicePnbp;
        }

        public String getOrderKerja() {
            return orderKerja;
        }

        public void setOrderKerja(String orderKerja) {
            this.orderKerja = orderKerja;
        }

        public String getPengantarBerkas() {
            return pengantarBerkas;
        }

        public void setPengantarBerkas(String pengantarBerkas) {
            this.pengantarBerkas = pengantarBerkas;
        }

        public String getPermohonanMeterai() {
            return permohonanMeterai;
        }

        public void setPermohonanMeterai(String permohonanMeterai) {
            this.permohonanMeterai = permohonanMeterai;
         }

		public String getSerahTerimaBerkas() {
			return serahTerimaBerkas;
		}

		public void setSerahTerimaBerkas(String serahTerimaBerkas) {
			this.serahTerimaBerkas = serahTerimaBerkas;
		}
    }

    public static class AccessLog {
        private Boolean enabled;
        private Boolean logParamIn;

		public Boolean getEnabled() {
			return enabled;
		}

		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}

		public Boolean getLogParamIn() {
			return logParamIn;
		}

		public void setLogParamIn(Boolean logParamIn) {
			this.logParamIn = logParamIn;
		}
    }

    public static class HistoryLog {
        private Boolean enabled;
        private Boolean logParamIn;

		public Boolean getEnabled() {
			return enabled;
		}

		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}

		public Boolean getLogParamIn() {
			return logParamIn;
		}

		public void setLogParamIn(Boolean logParamIn) {
			this.logParamIn = logParamIn;
		}
    }

    public Domain getDomain() {
        return domain;
    }

    public Font getFont() {
        return font;
    }

    public Directory getDirectory() {
        return directory;
    }

    public Template getTemplate() {
        return template;
    }

	public AccessLog getAccessLog() {
		return accessLog;
	}

	public HistoryLog getHistoryLog() {
		return historyLog;
	}
}
