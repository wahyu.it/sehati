package com.sehati.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
/**
 * A EntityUser.
 */
@Entity
@Table(name = "entity_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EntityUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @Column(name = "type")
    private Integer type;

    @Column(name = "role")
    private String role;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;
    
    @OneToOne
    private Pendamping pendamping;
    
    @ManyToOne
    private Lembaga lembaga;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public EntityUser id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return this.type;
    }

    public EntityUser type(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRole() {
        return this.role;
    }

    public EntityUser role(String role) {
        this.setRole(role);
        return this;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public EntityUser recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public EntityUser updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public EntityUser updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Pendamping getPendamping() {
		return pendamping;
	}

	public void setPendamping(Pendamping pendamping) {
		this.pendamping = pendamping;
	}

	public Lembaga getLembaga() {
		return lembaga;
	}

	public void setLembaga(Lembaga lembaga) {
		this.lembaga = lembaga;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EntityUser)) {
            return false;
        }
        return getId() != null && getId().equals(((EntityUser) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EntityUser{" +
            "id=" + getId() +
            ", type=" + getType() +
            ", role='" + getRole() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
