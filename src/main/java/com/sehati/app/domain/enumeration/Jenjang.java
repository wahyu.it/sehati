package com.sehati.app.domain.enumeration;

/**
 * The Jenjang enumeration.
 */
public enum Jenjang {
    MA, MTS, MI, TK
}
