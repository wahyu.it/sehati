package com.sehati.app.domain.enumeration;

/**
 * The StatusTransaksi enumeration.
 */
public enum StatusTransaksi {
    SUKSES, PROSES, GAGAL, EXPIRED, SUCCESS, REQUEST
}
