package com.sehati.app.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum CaraBayar {
    CASH, ANGSURAN
}
