package com.sehati.app.domain.enumeration;

/**
 * The Bank enumeration.
 */
public enum StatusBank {
    ACTIVE, INACTIVE
}
