package com.sehati.app.domain.enumeration;

public enum NotificationChannel {
	APP, WA, SMS, EMAIL, FIREBASE
}
