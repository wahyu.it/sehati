package com.sehati.app.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pendamping")
public class Pendamping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "nama", length = 100)
    private String nama;

    @Column(name = "saldo")
    private Integer saldo;

    @Size(max = 50)
    @Column(name = "username")
    private String username;

    @Size(max = 50)
    @Column(name = "password")
    private String password;

    @Column(name = "free_saldo")
    private Integer freeSaldo;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "nama_lembaga")
    private String namaLembaga;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getFreeSaldo() {
		return freeSaldo;
	}

	public void setFreeSaldo(Integer freeSaldo) {
		this.freeSaldo = freeSaldo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}


	public String getNamaLembaga() {
		return namaLembaga;
	}

	public void setNamaLembaga(String namaLembaga) {
		this.namaLembaga = namaLembaga;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pendamping other = (Pendamping) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Pendamping [id=" + id + ", nama=" + nama + ", saldo=" + saldo + ", username=" + username + ", password="
				+ password + ", freeSaldo=" + freeSaldo + ", status=" + status + ", namaLembaga=" + namaLembaga + "]";
	}

}
