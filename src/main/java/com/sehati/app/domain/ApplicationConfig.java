package com.sehati.app.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "application_config")
public class ApplicationConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "category", length = 50, nullable = false)
    private String category;

    @Size(max = 50)
    @Column(name = "name", length = 50)
    private String name;

    @Size(max = 200)
    @Column(name = "value", length = 200)
    private String value;

    @Size(max = 20)
    @Column(name = "used_for", length = 20)
    private String usedFor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public ApplicationConfig category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public ApplicationConfig name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public ApplicationConfig value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUsedFor() {
        return usedFor;
    }

    public ApplicationConfig usedFor(String usedFor) {
        this.usedFor = usedFor;
        return this;
    }

    public void setUsedFor(String usedFor) {
        this.usedFor = usedFor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApplicationConfig referensi = (ApplicationConfig) o;
        if (referensi.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, referensi.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Referensi{" +
            "id=" + id +
            ", category='" + category + "'" +
            ", name='" + name + "'" +
            ", value='" + value + "'" +
            ", usedFor='" + usedFor + "'" +
            '}';
    }
}
