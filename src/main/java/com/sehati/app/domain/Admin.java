package com.sehati.app.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "admin")
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "nama", length = 100)
    private String nama;

    @Size(max = 500)
    @Column(name = "alamat", length = 500)
    private String alamat;

    @Size(max = 11)
    @Column(name = "jenis_kelamin", length = 11)
    private Integer jenisKelamin;

    @Size(max = 11)
    @Column(name = "umur", length = 11)
    private Integer umur;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Integer getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(Integer jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public Integer getUmur() {
		return umur;
	}

	public void setUmur(Integer umur) {
		this.umur = umur;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Admin [id=" + id + ", nama=" + nama + ", alamat=" + alamat + ", jenisKelamin=" + jenisKelamin
				+ ", umur=" + umur + "]";
	}

}
