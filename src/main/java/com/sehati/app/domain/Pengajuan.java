package com.sehati.app.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name = "pengajuan")
public class Pengajuan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "no_pengajuan", length = 100)
    private String noPengajuan;

    @Column(name = "tgl_pengajuan")
    private LocalDate tglPengajuan;

    @Size(max = 100)
    @Column(name = "nama", length = 100)
    private String nama;

    @Size(max = 20)
    @Column(name = "nik", length = 20)
    private String nik;

    @Size(max = 500)
    @Column(name = "alamat", length = 500)
    private String alamat;

    @Size(max = 10)
    @Column(name = "kodepos", length = 10)
    private String kodepos;

    @Size(max = 100)
    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "no_hp")
    private String noHp;

    @Size(max = 100)
    @Column(name = "nama_usaha", length = 100)
    private String namaUsaha;

    @Size(max = 500)
    @Column(name = "alamat_usaha", length = 500)
    private String alamatUsaha;

    @Size(max = 100)
    @Column(name = "nib", length = 100)
    private String nib;
    
    @Size(max = 100)
    @Column(name = "merk_dagang", length = 100)
    private String merkDagang;
    
    @Size(max = 100)
    @Column(name = "file_form_pengajuan", length = 100)
    private String fileFormPengajuan;
    
    @Size(max = 100)
    @Column(name = "file_ktp", length = 100)
    private String fileKtp;
    
    @Size(max = 100)
    @Column(name = "file_foto_produk", length = 100)
    private String fileFotoProduk;
    
    @Size(max = 100)
    @Column(name = "foto_pu", length = 100)
    private String fotoPu;
    
    @Column(name = "status")
    private Integer status;
    
    @Size(max = 100)
    @Column(name = "verify_by", length = 100)
    private String verifyBy;
    
    @Column(name = "verify_on")
    private ZonedDateTime verifyOn;
    
    @Size(max = 100)
    @Column(name = "no_sertifikat", length = 100)
    private String noSertifikat;

    @Column(name = "tgl_sertifikat")
    private LocalDate tglSertifikat;

    @Column(name = "tgl_exp")
    private LocalDate tglExp;
    
    @ManyToOne
    private Provinsi provinsi;
    
    @ManyToOne
    private Kabupaten kabupaten;
    
    @ManyToOne
    private DaftarProduk daftarProduk;
    
    @ManyToOne
    private Pendamping pendamping;
    
    @ManyToOne
    private Lembaga lembaga;

    @Size(max = 100)
    @Column(name = "kab", length = 100)
    private String kab;

    @Size(max = 100)
    @Column(name = "prov", length = 100)
    private String prov;

    @Size(max = 100)
    @Column(name = "prod", length = 100)
    private String prod;

    @Size(max = 100)
    @Column(name = "pendamping", length = 100)
    private String damping;

    @Column(name = "link_sertifikat")
    private String linkSertifikat;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoPengajuan() {
		return noPengajuan;
	}

	public void setNoPengajuan(String noPengajuan) {
		this.noPengajuan = noPengajuan;
	}

	public LocalDate getTglPengajuan() {
		return tglPengajuan;
	}

	public void setTglPengajuan(LocalDate tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getKodepos() {
		return kodepos;
	}

	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getNamaUsaha() {
		return namaUsaha;
	}

	public void setNamaUsaha(String namaUsaha) {
		this.namaUsaha = namaUsaha;
	}

	public String getAlamatUsaha() {
		return alamatUsaha;
	}

	public void setAlamatUsaha(String alamatUsaha) {
		this.alamatUsaha = alamatUsaha;
	}

	public String getNib() {
		return nib;
	}

	public void setNib(String nib) {
		this.nib = nib;
	}

	public String getMerkDagang() {
		return merkDagang;
	}

	public void setMerkDagang(String merkDagang) {
		this.merkDagang = merkDagang;
	}

	public String getFileFormPengajuan() {
		return fileFormPengajuan;
	}

	public void setFileFormPengajuan(String fileFormPengajuan) {
		this.fileFormPengajuan = fileFormPengajuan;
	}

	public String getFileKtp() {
		return fileKtp;
	}

	public void setFileKtp(String fileKtp) {
		this.fileKtp = fileKtp;
	}

	public String getFileFotoProduk() {
		return fileFotoProduk;
	}

	public void setFileFotoProduk(String fileFotoProduk) {
		this.fileFotoProduk = fileFotoProduk;
	}

	public String getFotoPu() {
		return fotoPu;
	}

	public void setFotoPu(String fotoPu) {
		this.fotoPu = fotoPu;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getVerifyBy() {
		return verifyBy;
	}

	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}

	public ZonedDateTime getVerifyOn() {
		return verifyOn;
	}

	public void setVerifyOn(ZonedDateTime verifyOn) {
		this.verifyOn = verifyOn;
	}

	public String getNoSertifikat() {
		return noSertifikat;
	}

	public void setNoSertifikat(String noSertifikat) {
		this.noSertifikat = noSertifikat;
	}

	public LocalDate getTglSertifikat() {
		return tglSertifikat;
	}

	public void setTglSertifikat(LocalDate tglSertifikat) {
		this.tglSertifikat = tglSertifikat;
	}

	public LocalDate getTglExp() {
		return tglExp;
	}

	public void setTglExp(LocalDate tglExp) {
		this.tglExp = tglExp;
	}

	public Provinsi getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}

	public Kabupaten getKabupaten() {
		return kabupaten;
	}

	public void setKabupaten(Kabupaten kabupaten) {
		this.kabupaten = kabupaten;
	}

	public DaftarProduk getDaftarProduk() {
		return daftarProduk;
	}

	public void setDaftarProduk(DaftarProduk daftarProduk) {
		this.daftarProduk = daftarProduk;
	}

	public String getKab() {
		return kab;
	}

	public void setKab(String kab) {
		this.kab = kab;
	}

	public String getProv() {
		return prov;
	}

	public void setProv(String prov) {
		this.prov = prov;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public Pendamping getPendamping() {
		return pendamping;
	}

	public void setPendamping(Pendamping pendamping) {
		this.pendamping = pendamping;
	}

	public String getDamping() {
		return damping;
	}

	public void setDamping(String damping) {
		this.damping = damping;
	}

	public String getLinkSertifikat() {
		return linkSertifikat;
	}

	public void setLinkSertifikat(String linkSertifikat) {
		this.linkSertifikat = linkSertifikat;
	}

	public Lembaga getLembaga() {
		return lembaga;
	}

	public void setLembaga(Lembaga lembaga) {
		this.lembaga = lembaga;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pengajuan other = (Pengajuan) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Pengajuan [id=" + id + ", noPengajuan=" + noPengajuan + ", tglPengajuan=" + tglPengajuan + ", nama="
				+ nama + ", nik=" + nik + ", alamat=" + alamat + ", kodepos=" + kodepos + ", email=" + email + ", noHp="
				+ noHp + ", namaUsaha=" + namaUsaha + ", alamatUsaha=" + alamatUsaha + ", nib=" + nib + ", merkDagang="
				+ merkDagang + ", fileFormPengajuan=" + fileFormPengajuan + ", fileKtp=" + fileKtp + ", fileFotoProduk="
				+ fileFotoProduk + ", fotoPu=" + fotoPu + ", status=" + status + ", verifyBy=" + verifyBy
				+ ", verifyOn=" + verifyOn + ", noSertifikat=" + noSertifikat + ", tglSertifikat=" + tglSertifikat
				+ ", tglExp=" + tglExp + ", provinsi=" + provinsi + ", kabupaten=" + kabupaten + ", daftarProduk="
				+ daftarProduk + ", pendamping=" + pendamping + ", lembaga=" + lembaga + ", kab=" + kab + ", prov="
				+ prov + ", prod=" + prod + ", damping=" + damping + ", linkSertifikat=" + linkSertifikat + "]";
	}
}
