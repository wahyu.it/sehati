package com.sehati.app.domain.util;


public enum NasabahJenis {

    BADAN(1), PERORANGAN(2);

    private int value;

    NasabahJenis(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
