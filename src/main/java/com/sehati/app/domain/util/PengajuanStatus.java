package com.sehati.app.domain.util;


public enum PengajuanStatus {

    REVISI(1), PROSES(2), COMPLETE(3), SUBMIT(4), INVOICE(5);

    private int value;

    PengajuanStatus(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
