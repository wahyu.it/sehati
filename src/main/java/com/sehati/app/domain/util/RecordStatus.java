package com.sehati.app.domain.util;

public enum RecordStatus {
	ACTIVE(1), INACTIVE(0);
	
	private int value;
	
	RecordStatus (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }
}
