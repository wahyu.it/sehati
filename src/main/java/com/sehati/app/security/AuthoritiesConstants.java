package com.sehati.app.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {
	
	 public static final String ADMIN = "ROLE_ADMIN";

	    public static final String USER = "ROLE_USER";
	    
	    public static final String MANAGER = "ROLE_MANAGER";
	    
	    public static final String OPERATOR = "ROLE_OPERATOR";
	    
	    public static final String PENDAMPING = "ROLE_PENDAMPING";
	    
	    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
	    
    private AuthoritiesConstants() {
    }
}
