package com.sehati.app.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Referensi;

import java.util.List;

/**
 * Spring Data JPA repository for the Referensi entity.
 */
@SuppressWarnings("unused")
public interface ReferensiRepository extends JpaRepository<Referensi,Long> {

    List<Referensi> findByCategoryAndName(String category, String name);

    List<Referensi> findByCategory(String category);
    
    List<Referensi> findByCategoryAndNameAndUsedFor(String category, String name, String usedFor);

}
