package com.sehati.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Lembaga;

@Repository
public interface LembagaRepository extends JpaRepository<Lembaga, Long> {
}
