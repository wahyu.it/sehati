package com.sehati.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.EntityUser;
import com.sehati.app.domain.Lembaga;

/**
 * Spring Data JPA repository for the EntityUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EntityUserRepository extends JpaRepository<EntityUser, Long> {

    EntityUser findOneByRecordStatusAndUserLogin(Integer recordStatus, String userLogin);

	List<EntityUser> findByType(Integer type);

	List<EntityUser> findByRole(String string);

}
