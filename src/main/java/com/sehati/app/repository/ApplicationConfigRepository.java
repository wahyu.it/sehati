package com.sehati.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.ApplicationConfig;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationConfigRepository extends JpaRepository<ApplicationConfig, Long>  {

	List<ApplicationConfig> findByCategory(String string);

	ApplicationConfig findByCategoryAndName(String string, String string2);

}
