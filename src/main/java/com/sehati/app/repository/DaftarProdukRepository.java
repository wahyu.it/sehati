package com.sehati.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.DaftarProduk;

@Repository
public interface DaftarProdukRepository extends JpaRepository<DaftarProduk, Long>  {

}
