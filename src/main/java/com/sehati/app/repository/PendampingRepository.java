package com.sehati.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Pendamping;

@Repository
public interface PendampingRepository extends JpaRepository<Pendamping, Long> {

	Page<Pendamping> findByStatus(Boolean status, Pageable pageable);

	@Query("SELECT s"
			+ " FROM Pendamping s"
			+ " WHERE s.nama LIKE %:nama%")
	Page<Pendamping> findByNama(
			@Param("nama") String nama,
			Pageable pageable);

}
