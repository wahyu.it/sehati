package com.sehati.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sehati.app.domain.UserAuthority;

import java.util.List;

/**
 * Created by abdulqadir on 05/02/18.
 */
public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {

    List<UserAuthority> findByUserId(Long userId);
}
