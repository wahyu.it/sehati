package com.sehati.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sehati.app.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
