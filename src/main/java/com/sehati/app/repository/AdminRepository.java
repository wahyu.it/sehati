package com.sehati.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long>  {

}
