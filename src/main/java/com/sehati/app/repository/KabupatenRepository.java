package com.sehati.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Kabupaten;
import com.sehati.app.domain.Provinsi;

@Repository
public interface KabupatenRepository extends JpaRepository<Kabupaten, Long> {

	Page<Kabupaten> findByProvinsi(Provinsi provinsi, Pageable pageable);

}
