package com.sehati.app.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sehati.app.domain.Lembaga;
import com.sehati.app.domain.Pendamping;
import com.sehati.app.domain.Pengajuan;
import com.sehati.app.service.dto.SummaryPengajuanDTO;

@Repository
public interface  PengajuanRepository extends JpaRepository<Pengajuan, Long>  {
	
//	pengajuan.status === 1 REVISI
//	pengajuan.status === 2 PROSES
//	pengajuan.status === 3 COMPLETE
//	pengajuan.status === 4 SUBMIT
//	pengajuan.status === 5 INVOICE DONE

	Page<Pengajuan> findByNoPengajuan(String svalue, Pageable pageable);

	Page<Pengajuan> findByNama(String svalue, Pageable pageable);

	Page<Pengajuan> findByTglPengajuan(LocalDate tglPengajuan, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndDamping(String svalue, String nama, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndDamping(LocalDate tglPengajuan, String nama, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndDamping(LocalDate startDate, LocalDate endDate, String nama,
			Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndPendamping(LocalDate startDate, LocalDate endDate,
			Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndPendamping(LocalDate tglPengajuan, Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndPendamping(String svalue, Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndLembaga(String svalue, Lembaga lembaga, Pageable pageable);

	Page<Pengajuan> findByNamaAndLembaga(String svalue, Lembaga lembaga, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembaga(LocalDate tglPengajuan, Lembaga lembaga, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembaga(LocalDate startDate, LocalDate endDate, Lembaga lembaga,
			Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndLembagaIn(String svalue, List<Lembaga> listLembaga, Pageable pageable);

	Page<Pengajuan> findByNamaAndLembagaIn(String svalue, List<Lembaga> listLembaga, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaIn(LocalDate tglPengajuan, List<Lembaga> listLembaga,
			Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaIn(LocalDate startDate, LocalDate endDate,
			List<Lembaga> listLembaga, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaAndPendamping(LocalDate tglPengajuan, Lembaga lembaga,
			Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaInAndPendamping(LocalDate tglPengajuan, List<Lembaga> listLembaga,
			Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaAndPendamping(LocalDate startDate, LocalDate endDate,
			Lembaga lembaga, Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaInAndPendamping(LocalDate startDate, LocalDate endDate,
			List<Lembaga> listLembaga, Pendamping pendamping, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaInAndStatusIn(LocalDate startDate, LocalDate endDate,
			List<Lembaga> listLembaga, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaAndStatusIn(LocalDate startDate, LocalDate endDate,
			Lembaga lembaga, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaInAndPendampingAndStatusIn(LocalDate startDate,
			LocalDate endDate, List<Lembaga> listLembaga, Pendamping pendamping, List<Integer> statusList,
			Pageable pageable);

	Page<Pengajuan> findByTglPengajuanBetweenAndLembagaAndPendampingAndStatusIn(LocalDate startDate, LocalDate endDate,
			Lembaga lembaga, Pendamping pendamping, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaInAndPendampingAndStatusIn(LocalDate tglPengajuan,
			List<Lembaga> listLembaga, Pendamping pendamping, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaAndPendampingAndStatusIn(LocalDate tglPengajuan, Lembaga lembaga,
			Pendamping pendamping, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaInAndStatusIn(LocalDate tglPengajuan, List<Lembaga> listLembaga,
			List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByTglPengajuanAndLembagaAndStatusIn(LocalDate tglPengajuan, Lembaga lembaga,
			List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByNamaAndLembagaInAndStatusIn(String svalue, List<Lembaga> listLembaga,
			List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByNamaAndLembagaAndStatusIn(String svalue, Lembaga lembaga, List<Integer> statusList,
			Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndStatusIn(String svalue, List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndLembagaInAndStatusIn(String svalue, List<Lembaga> listLembaga,
			List<Integer> statusList, Pageable pageable);

	Page<Pengajuan> findByNoPengajuanAndLembagaAndStatusIn(String svalue, Lembaga lembaga, List<Integer> statusList,
			Pageable pageable);
	
	static final String SUMMARYPENGAJUAN_DTO  = "SELECT new com.sehati.app.service.dto.SummaryPengajuanDTO(b.nama, count(a.id), count(a1.id), count(a2.id), count(a3.id), count(a4.id), count(a5.id), a.tglPengajuan)"
			+ " FROM Pengajuan a"
			+ " LEFT JOIN Pengajuan a1 ON a1.id = a.id and a1.status=5"
			+ " LEFT JOIN Pengajuan a2 ON a2.id = a.id and a2.status=3"
			+ " LEFT JOIN Pengajuan a3 ON a3.id = a.id and a3.status=2"
			+ " LEFT JOIN Pengajuan a4 ON a4.id = a.id and a4.status=4"
			+ " LEFT JOIN Pengajuan a5 ON a5.id = a.id and a5.status=1"
			+ " INNER JOIN Pendamping b ON a.pendamping.id = b.id";
	
	static final String SUMMARYPENGAJUAN2_DTO  = "SELECT new com.sehati.app.service.dto.SummaryPengajuanDTO(a.verifyBy, count(a.id), count(a1.id), count(a2.id), count(a3.id), count(a4.id), count(a5.id), a.tglPengajuan)"
			+ " FROM Pengajuan a"
			+ " LEFT JOIN Pengajuan a1 ON a1.id = a.id and a1.status=5"
			+ " LEFT JOIN Pengajuan a2 ON a2.id = a.id and a2.status=3"
			+ " LEFT JOIN Pengajuan a3 ON a3.id = a.id and a3.status=2"
			+ " LEFT JOIN Pengajuan a4 ON a4.id = a.id and a4.status=4"
			+ " LEFT JOIN Pengajuan a5 ON a5.id = a.id and a5.status=1";
	
	@Query(SUMMARYPENGAJUAN_DTO + " WHERE a.tglPengajuan BETWEEN :start AND :end"
			+ " GROUP BY b.nama")
	Page<SummaryPengajuanDTO> findSummaryByDateBetween(
			@Param("start") LocalDate start, 
			@Param("end") LocalDate end, 
			Pageable pageable);
	
	@Query(SUMMARYPENGAJUAN_DTO + " WHERE a.tglPengajuan BETWEEN :start AND :end AND b.id=:id"
			+ " GROUP BY b.nama")
	Page<SummaryPengajuanDTO> findSummaryByDateBetweenAndPendamping(
			@Param("start") LocalDate start, 
			@Param("end") LocalDate end,
			@Param("id") Long id, Pageable pageable);
	
	@Query(SUMMARYPENGAJUAN2_DTO + " WHERE a.tglPengajuan BETWEEN :start AND :end"
			+ " GROUP BY a.verifyBy")
	Page<SummaryPengajuanDTO> findSummaryOptByDateBetween(
			@Param("start") LocalDate start, 
			@Param("end") LocalDate end, 
			Pageable pageable);
	
	@Query(SUMMARYPENGAJUAN2_DTO + " WHERE a.tglPengajuan BETWEEN :start AND :end AND a.verifyBy=:verifyBy"
			+ " GROUP BY a.verifyBy")
	Page<SummaryPengajuanDTO> findSummaryOptByDateBetweenAndOpt(
			@Param("start") LocalDate start, 
			@Param("end") LocalDate end,
			@Param("verifyBy") String verifyBy, Pageable pageable);
}
