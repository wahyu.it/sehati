import { Moment } from 'moment';
import { ProvinsiDTO } from './provinsiDTO.model';
import { KabupatenDTO } from './kabupatenDTO.model';
import { DaftarProdukDTO } from './daftarprodukDTO.model';

export interface IPengajuanDTO {
        id?: number;
		noPengajuan?: string;
        tglPengajuan?: Moment;
        nama?: string;
        nik?: string;
        alamat?: string;
        kodepos?: string;
        email?: string;
        noHp?: string;
        namaUsaha?: string;
        alamatUsaha?: string;
        nib?: string;
        merkDagang?: string;
        fileFormPengajuan?: string;
        fileKtp?: string;
        fileFotoProduk?: string;
        fotoPu?: string;
        status?: number;
        verifyBy?: string;
        verifyOn?: Moment;
        noSertifikat?: string;
        tglSertifikat?: Moment;
        tglExp?: Moment;
        provinsiid?: number;
        provinsinama?: string;
        kabupatenid?: number;
        kabupatennama?: string;
        daftarpordukid?: number;
        daftarporduknama?: string;
        kab?: KabupatenDTO;
        prov?: ProvinsiDTO;
        prod?: DaftarProdukDTO;
        pendamping?: string;
        provinsi?: ProvinsiDTO;
        kabupaten?: KabupatenDTO;
        daftarProduk?: DaftarProdukDTO;
        linkSertifikat?: string;
}

export class PengajuanDTO implements IPengajuanDTO {
  constructor(       
	   public  id?: number,
	   public  noPengajuan?: string,
       public  tglPengajuan?: Moment,
       public  nama?: string,
       public  nik?: string,
       public  alamat?: string,
       public  kodepos?: string,
       public  email?: string,
       public  noHp?: string,
       public  namaUsaha?: string,
       public  alamatUsaha?: string,
       public  nib?: string,
       public  merkDagang?: string,
       public  fileFormPengajuan?: string,
       public  fileKtp?: string,
       public  fileFotoProduk?: string,
       public  fotoPu?: string,
       public  status?: number,
       public  verifyBy?: string,
       public  verifyOn?: Moment,
       public  noSertifikat?: string,
       public  tglSertifikat?: Moment,
       public  tglExp?: Moment,
       public  provinsiid?: number,
       public  provinsinama?: string,
       public  kabupatenid?: number,
       public  kabupatennama?: string,
       public  daftarpordukid?: number,
       public  daftarporduknama?: string,
       public  kab?: KabupatenDTO,
       public  prov?: ProvinsiDTO,
       public  prod?: DaftarProdukDTO,
       public  damping?: string,
       public  provinsi?: ProvinsiDTO,
       public  kabupaten?: KabupatenDTO,
       public  daftarProduk?: DaftarProdukDTO,
       public  linkSertifikat?: string
  ) {}
}
