import { Moment } from 'moment';

export interface ISummaryPengajuanDTO {
        namaPendamping?: string;
        bulan?: Moment;
        jmlPembayaran?: number;
        jmlPengajuan?: number;
        jmlComplete?: number;
        jmlProses?: number;
        jmlSubmit?: number;
        jmlReject?: number;
}

export class SummaryPengajuanDTO implements ISummaryPengajuanDTO {
  constructor(       
       public  namaPendamping?: string,
       public  jmlPembayaran?: number,
       public  jmlPengajuan?: number,
       public  jmlComplete?: number,
       public  jmlProses?: number,
       public  jmlSubmit?: number,
       public  jmlReject?: number,
       public  bulan?: Moment,
  ) {}
}
