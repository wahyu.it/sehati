export interface IKabupatenDTO {
  id?: number;
  name?: string;
}

export class KabupatenDTO implements IKabupatenDTO {
  constructor(
    public id?: number,
    public name?: string,
  ) {}
}