export interface IProvinsiDTO {
  id?: number;
  name?: string;
}

export class ProvinsiDTO implements IProvinsiDTO {
  constructor(
    public id?: number,
    public name?: string,
  ) {}
}
