import { Moment } from 'moment';

export interface IBerkasDTO {
        id?: number;
        identitas?: string;
        kk?: string;
        ppk?: string;
        ppk2?: string;
        skNasabah?: string;
        updateBy?: string;
        updateOn?: Moment;
        uploadBy?: string;
        uploadOn?: Moment;
        ppkNomor?: string;
        nsbJenis?: number;
        cabangId?: number;
        berkasStatus?: number;
        namaNasabah?: string;
        tanggalPpk?: Moment;
        metodeKirim?: number;
        remarkId?: number;
        remarkName?: string;
        partitionId?: number;
        verifyBy?: string;
        verifyOn?: Moment;
        orderType?: number;
}

export class BerkasDTO implements IBerkasDTO {
  constructor(
       	public id?: number,
		public identitas?: string,
		public kk?: string,
		public ppk?: string,
		public ppk2?: string,
		public skNasabah?: string,
		public updateBy?: string,
		public updateOn?: Moment,
		public uploadBy?: string,
		public uploadOn?: Moment,
		public ppkNomor?: string,
		public nsbJenis?: number,
		public cabangId?: number,
		public berkasStatus?: number,
		public namaNasabah?: string,
		public tanggalPpk?: Moment,
		public metodeKirim?: number,
		public remarkId?: number,
		public remarkName?: string,
		public partitionId?: number,
		public verifyBy?: string,
		public verifyOn?: Moment,
		public orderType?: number,
  ) {}
}
