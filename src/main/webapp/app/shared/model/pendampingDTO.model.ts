export interface IPendampingDTO {
  id?: number;
  nama?: string;
  saldo?: number;
  username?: string;
  password?: string;
  freeSaldo?: number;
  status?:boolean;
  namaLembaga?: string;
}

export class PendampingDTO implements IPendampingDTO {
  constructor(
    public id?: number,
    public nama?: string,
	public saldo?: number,
	public username?: string,
	public password?: string,
  	public freeSaldo?: number,
    public status?: boolean,
    public namaLembaga?: string,
  ) {}
}
