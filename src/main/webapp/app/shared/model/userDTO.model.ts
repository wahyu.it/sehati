export interface IOperatorDTO {
        id?: number;
        login?: string;
}

export class OperatorDTO implements IOperatorDTO {
  constructor(       
       public  id?: number,
       public  login?: string,
  ) {}
}
