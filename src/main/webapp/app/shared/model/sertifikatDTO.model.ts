import { Moment } from 'moment';

export interface ISertifikatDTO {
        kodeVoucher?:  string;
        tglVoucher?:  Moment;
        noSertifikat?:  string; 
        tglSertifikat?:  Moment; 
        biayaPnbp?:  number;
        noReferensiBni?:  string;
        status?:  number;
        updateBy?:  string; 
        updateOn?:  Moment;
        registrationId?:  string;
        ppkNomor?:  string;
        tglOrder?:  Moment;   
        ppkTanggal?:  Moment;   
        biayaJasa?:  number;   
        lampiranObject?:  string; 
}

export class SertifikatDTO implements ISertifikatDTO {
  constructor(
       public kodeVoucher?:  string,
       public tglVoucher?:  Moment,
       public noSertifikat?:  string,
       public tglSertifikat?:  Moment,
       public biayaPnbp?:  number,
       public noReferensiBni?:  string,
       public status?:  number,
       public updateBy?:  string, 
       public updateOn?:  Moment,
       public registrationId?:  string,
       public  ppkNomor?:  string,
       public tglOrder?:  Moment,   
       public ppkTanggal?:  Moment,  
       public biayaJasa?:  number,  
       public lampiranObject?:  string, 
  ) {}
}
