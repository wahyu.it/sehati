export interface IDaftarProdukDTO {
  id?: number;
  name?: string;
}

export class DaftarProdukDTO implements IDaftarProdukDTO {
  constructor(
    public id?: number,
    public name?: string,
  ) {}
}