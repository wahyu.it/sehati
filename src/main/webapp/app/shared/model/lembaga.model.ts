export interface ILembaga {
  id?: number;
  name?: string;
}

export class Lembaga implements ILembaga {
  constructor(
    public id?: number,
    public name?: string,
  ) {}
}
