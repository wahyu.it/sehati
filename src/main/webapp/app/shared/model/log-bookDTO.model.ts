import { Moment } from 'moment';

export interface ILogBookDTO {
  id?: number;
  tanggal?: Moment;
  mulai?: Moment;
  selesai?: Moment;
  user?: string;
  keperluan?: string;
  catatan?: string;
}

export class LogBookDTO implements ILogBookDTO {
  constructor(
    public id?: number,
    public tanggal?: Moment,
    public jatuhTempo?: Moment,
    public tanggalBayar?: Moment,
    public user?: string,
    public keperluan?: string,
    public catatan?: string,
  ) {}
}
