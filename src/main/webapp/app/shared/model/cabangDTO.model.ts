import { Moment } from 'moment';

export interface ICabangDTO {
        id?:number,
		kode?: string,
		nama?: string,
		alamat?: string,
		provinsi?: string,
		kota?: string,
		pengadilanNegeri?: string,
		noKontak?: string,
		recordStatus?: number,
		updateBy?: string,
		updateOn?: Moment,
		leasingId?: number,
}

export class CabangDTO implements ICabangDTO {
  constructor(
	  	public id?:number,
		public kode?: string,
		public nama?: string,
		public alamat?: string,
		public provinsi?: string,
		public kota?: string,
		public pengadilanNegeri?: string,
		public noKontak?: string,
		public recordStatus?: number,
		public updateBy?: string,
		public updateOn?: Moment,
		public leasingId?: number,
  ) {}
}
