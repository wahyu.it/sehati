import { Injectable } from '@angular/core';

import { AccountService } from 'app/core/auth/account.service';
import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { Login } from 'app/core/login/login.model';

@Injectable({ providedIn: 'root' })
export class LoginService {
  constructor(private accountService: AccountService, private authServerProvider: AuthServerProvider) {}

  login(credentials: Login, callback?: any): any {
    const cb = callback || function(): any {};

    return new Promise((resolve: any, reject: any) => {
      this.authServerProvider.login(credentials).subscribe(
        data => {
          this.accountService.identity(true).subscribe(() => {
            resolve(data);
          });
          return cb();
        },
        err => {
          this.logout();
          reject(err);
          return cb(err);
        }
      );
    });
  }

  loginWithToken(jwt: any, rememberMe: any): void {
    this.authServerProvider.loginWithToken(jwt, rememberMe);
  }

  logout(): void {
    this.authServerProvider.logout().subscribe();
    this.accountService.authenticate(null);
  }
}
