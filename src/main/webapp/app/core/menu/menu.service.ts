import { Injectable } from '@angular/core';
import { AccountService } from 'app/core';

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  translate: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  ab: string;
  type?: string;
  translate: string;
  title: string;
}

@Injectable({ providedIn: 'root' })
export class MenuService {
  constructor(private accountService: AccountService) {}

  getMenuItems(): RouteInfo[] {
    if (this.accountService.hasAnyAuthority(['ROLE_ADMIN'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        {
          path: '/',
          title: 'Administration',
          type: 'sub',
          translate: 'entities.admin',
          icontype: 'list_alt',
          collapse: 'admin',
          children: [
            {
              path: 'admin/user-management',
              title: 'User Management',
              ab: 'UM',
              translate: 'admin.userManagement'
            },
            { path: 'admin/metrics', title: 'Metrics', ab: 'MT', translate: 'admin.metrics' },
            { path: 'admin/health', title: 'Health', ab: 'HE', translate: 'admin.health' },
            {
              path: 'admin/configuration',
              title: 'Configuration',
              ab: 'CO',
              translate: 'admin.configuration'
            },
            { path: 'admin/audits', title: 'Audits', ab: 'AU', translate: 'admin.audits' },
            { path: 'admin/logs', title: 'Logs', ab: 'LO', translate: 'admin.logs' },
            { path: 'admin/docs', title: 'API', ab: 'DO', translate: 'admin.apidocs' }
          ]
        }
      ]
	} else if (this.accountService.hasAnyAuthority(['ROLE_OPERATOR'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'pemeriksaan', title: 'Dashboard', type: 'link', translate: 'pemeriksaan', icontype: 'list_alt' },
			{ path: 'pendamping', title: 'Dashboard', type: 'link', translate: 'pendamping', icontype: 'list_alt' },
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_MANAGER'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'pemeriksaan', title: 'Dashboard', type: 'link', translate: 'pemeriksaan', icontype: 'list_alt' },
			{ path: 'pendamping', title: 'Dashboard', type: 'link', translate: 'pendamping', icontype: 'list_alt' },
			{ path: 'add-user', title: 'Dashboard', type: 'link', translate: 'adduser', icontype: 'list_alt' },
			{ path: 'laporan-pengajuan', title: 'Dashboard', type: 'link', translate: 'invoiceflag', icontype: 'list_alt' },
			{
	          path: '/',
	          title: 'Summary',
	          type: 'sub',
	          translate: 'entities.summary',
	          icontype: 'list_alt',
	          collapse: 'admin',
	          children: [
	            { path: 'summary-pendamping', title: 'Audits', ab: 'SP', translate: 'summary-damping' },
	            { path: 'summary-operator', title: 'Audits', ab: 'SA', translate: 'summary-admin' },
	          ]
	        }
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_PENDAMPING'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'pengajuan', title: 'Dashboard', type: 'link', translate: 'pengajuan', icontype: 'list_alt' },
		];
	} else {
      return [];
    }
  }
}
