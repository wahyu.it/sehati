import { Component, OnInit, ElementRef } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

import { VERSION } from 'app/app.constants';
import { LANGUAGES } from 'app/core/language/language.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { PengajuanService } from 'app/entities/pengajuan/pengajuan.service';
import { MenuService } from 'app/core';

const misc: any = {
  navbarMenuVisible: 0,
  activeCollapse: true,
  disabledCollapseInit: 0
};

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss']
})
export class NavbarComponent implements OnInit {
  listTitles: any[] | undefined;
  location: Location | undefined;
  mobileMenuVisible = 0;
  toggleButton: any;
  sidebarVisible: boolean | undefined;
  inProduction?: boolean;
  isNavbarCollapsed = true;
  languages = LANGUAGES;
  swaggerEnabled?: boolean;
  version: string;
  nativeElement: Node | undefined;

  constructor(
    private loginService: LoginService,
    private languageService: JhiLanguageService,
    private sessionStorage: SessionStorageService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private menuService: MenuService,
    private element: ElementRef,
    private router: Router,
    private pengajuanService: PengajuanService
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
    this.sidebarVisible = false;
  }

  ngOnInit(): void {
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    this.listTitles = this.menuService.getMenuItems().filter(listTitle => listTitle);

    const navbar: HTMLElement = this.element.nativeElement;
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    if (body.classList.contains('sidebar-mini')) {
      misc.sidebarMiniActive = true;
    }
    if (body.classList.contains('hide-sidebar')) {
      misc.hideSidebarActive = true;
    }
    //        this._router = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
    //            this.sidebarClose();
    //
    //            const $layer = document.getElementsByClassName('close-layer')[0];
    //            if ($layer) {
    //                $layer.remove();
    //            }
    //        });
  }

  minimizeSidebar(): void {
    const body = document.getElementsByTagName('body')[0];

    if (misc.sidebarMiniActive === true) {
      body.classList.remove('sidebar-mini');
      misc.sidebarMiniActive = false;
    } else {
      setTimeout(function(): void {
        body.classList.add('sidebar-mini');

        misc.sidebarMiniActive = true;
      }, 300);
    }

    const simulateWindowResize = setInterval(function(): void {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    setTimeout(function(): void {
      clearInterval(simulateWindowResize);
    }, 1000);
  }

  hideSidebar(): void {
    const body = document.getElementsByTagName('body')[0];
    const sidebar = document.getElementsByClassName('sidebar')[0];

    if (misc.hide_sidebar_active === true) {
      setTimeout(function(): void {
        body.classList.remove('hide-sidebar');
        misc.hideSidebarActive = false;
      }, 300);
      setTimeout(function(): void {
        sidebar.classList.remove('animation');
      }, 600);
      sidebar.classList.add('animation');
    } else {
      setTimeout(function(): void {
        body.classList.add('hide-sidebar');
        // $('.sidebar').addClass('animation');
        misc.hideSidebarActive = true;
      }, 300);
    }

    const simulateWindowResize = setInterval(function(): void {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    setTimeout(function(): void {
      clearInterval(simulateWindowResize);
    }, 1000);
  }

  sidebarOpen(): void {
    const $toggle = document.getElementsByClassName('navbar-toggler')[0];
    const toggleButton = this.toggleButton;
    const body = document.getElementsByTagName('body')[0];
    setTimeout(function(): void {
      toggleButton.classList.add('toggled');
    }, 500);
    body.classList.add('nav-open');
    setTimeout(function(): void {
      $toggle.classList.add('toggled');
    }, 430);

    const $layer = document.createElement('div');
    $layer.setAttribute('class', 'close-layer');

    if (body.querySelectorAll('.main-panel')) {
      document.getElementsByClassName('main-panel')[0].appendChild($layer);
    } else if (body.classList.contains('off-canvas-sidebar')) {
      document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
    }

    setTimeout(function(): void {
      $layer.classList.add('visible');
    }, 100);

    $layer.onclick = function(this: any): void {
      body.classList.remove('nav-open');

      $layer.classList.remove('visible');
      this.mobileMenuVisible = 0;
      this.sidebarVisible = false;
      setTimeout(function(): void {
        $layer.remove();
        $toggle.classList.remove('toggled');
      }, 400);
    }.bind(this);

    body.classList.add('nav-open');
    this.mobileMenuVisible = 1;
    this.sidebarVisible = true;
  }

  sidebarClose(): void {
    const $toggle = document.getElementsByClassName('navbar-toggler')[0];
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton.classList.remove('toggled');
    const $layer = document.createElement('div');
    $layer.setAttribute('class', 'close-layer');

    this.sidebarVisible = false;
    body.classList.remove('nav-open');
    // $('html').removeClass('nav-open');
    body.classList.remove('nav-open');
    if ($layer) {
      $layer.remove();
    }

    setTimeout(function(): void {
      $toggle.classList.remove('toggled');
    }, 400);

    this.mobileMenuVisible = 0;
  }

  changeLanguage(languageKey: string): void {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.sidebarClose();
    this.router.navigate(['/'])
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

  sidebarToggle(): void {
    console.log(this.sidebarVisible);
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }
}
