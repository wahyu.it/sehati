import { Component, OnInit, ElementRef, OnDestroy, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare let $: any;

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  authenticationError!: boolean;
  password!: string;
  rememberMe!: boolean;
  username!: string;
  credentials: any;
  nativeElement: Node;
  hide!: boolean;

  constructor(
    private element: ElementRef,
    private eventManager: JhiEventManager,
    private loginService: LoginService,
    private stateStorageService: StateStorageService,
    private elementRef: ElementRef,
    private renderer: Renderer,
    private router: Router,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal
  ) {
    this.nativeElement = element.nativeElement;
    this.credentials = {};
  }

  ngOnInit(): void {
    this.hide = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');

    const cards = document.getElementsByClassName('card');
    let card: any;
    for (let i = 0; i < cards.length; i++) {
      if (cards[i].id === 'card-login') {
        card = cards[i];
      }
    }
    setTimeout(function(): void {
      // after 1000 ms we add the class animated to the login/register card
      if (card) {
        card.classList.remove('card-hidden');
      }
    }, 700);
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

  cancel(): void {
    this.credentials = {
      username: null,
      password: null,
      rememberMe: true
    };
    this.authenticationError = false;
  }

  login(): void {
    this.loginService
      .login({
        username: this.username,
        password: this.password,
        rememberMe: this.rememberMe
      })
      .then(() => {
        this.authenticationError = false;
        if (
          this.router.url === '/account/register' ||
          this.router.url.startsWith('/account/activate') ||
          this.router.url.startsWith('/account/reset/')
        ) {
          this.router.navigate(['']);
        }
        this.router.navigate(['']);
        this.eventManager.broadcast({
          name: 'authenticationSuccess',
          content: 'Sending Authentication Success'
        });

        // previousState was set in the authExpiredInterceptor before being redirected to login modal.
        // since login is successful, go to stored previousState and clear previousState
        const redirect = this.stateStorageService.getUrl();
        if (redirect) {
          this.stateStorageService.storeUrl('');
          this.router.navigate([redirect]);
        }
        // console.log("login sukses...")
      })
      .catch(() => {
        this.authenticationError = true;
      });
  }

  pass(): void {
    this.hide = !this.hide;
  }

  register(): void {
    this.activeModal.dismiss('to state register');
    this.router.navigate(['/account/register']);
  }
}
