import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { sidebarRoute } from './layouts';

const LAYOUT_ROUTES = [navbarRoute, sidebarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: ['ROLE_ADMIN', 'ROLE_TU']
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule)
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
        },
        {
          path: 'pengajuan',
          loadChildren: () => import('./entities/pengajuan/pengajuan.module').then(m => m.PengajuanSehatiModule),
          data: {
	          pageTitle: 'global.menu.pengajuan'
	        }
        },
        {
          path: 'pemeriksaan',
          loadChildren: () => import('./entities/pemeriksaan/pemeriksaan.module').then(m => m.PemeriksaanSehatiModule),
          data: {
	          pageTitle: 'global.menu.pemeriksaan'
	        }
        },
        {
          path: 'pendamping',
          loadChildren: () => import('./entities/pendamping/pendamping.module').then(m => m.PendampingSehatiModule),
          data: {
	          pageTitle: 'global.menu.pendamping'
	        }
        },
        {
          path: 'add-user',
          loadChildren: () => import('./entities/add-user/add-user.module').then(m => m.AddUserModule),
          data: {
	          pageTitle: 'global.menu.pendamping'
	        }
        },
        {
          path: 'laporan-pengajuan',
          loadChildren: () => import('./entities/laporan/laporan-pengajuan.module').then(m => m.PengajuanSehatiModule),
          data: {
	          pageTitle: 'global.menu.pendamping'
	        }
        },
        {
          path: 'summary-pendamping',
          loadChildren: () => import('./entities/summary/pendamping/summary-pendamping.module').then(m => m.SummaryPengajuanPendampingSehatiModule),
          data: {
	          pageTitle: 'global.menu.pendamping'
	        }
        },
        {
          path: 'summary-operator',
          loadChildren: () => import('./entities/summary/operator/summary-operator.module').then(m => m.SummaryPengajuanOperatorSehatiModule),
          data: {
	          pageTitle: 'global.menu.summary-admin'
	        }
        },
        ...LAYOUT_ROUTES
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class SisteminformasimonitoringAppRoutingModule {}
