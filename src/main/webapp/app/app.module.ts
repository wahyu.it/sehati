import { NgModule } from '@angular/core';
import { NgbDatepickerConfig, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import * as moment from 'moment';

import './vendor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { SistemsentralisasifidusiaCoreModule } from 'app/core/core.module';
import { SisteminformasimonitoringAppRoutingModule } from './app-routing.module';
import { SistemsentralisasifidusiaHomeModule } from './home/home.module';
import { SisteminformasimonitoringEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { SidebarComponent, LoginComponent } from './layouts';
// import { Ng2Webstorage } from 'ngx-webstorage';
import { NgJhipsterModule } from 'ng-jhipster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatRippleModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; 

@NgModule({
  imports: [
    MatNativeDateModule,
    MatRippleModule,
    MatDatepickerModule,
    MatSelectModule,
    BrowserAnimationsModule,
    BrowserModule,
    SisteminformasimonitoringSharedModule,
    SistemsentralisasifidusiaCoreModule,
    SistemsentralisasifidusiaHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SisteminformasimonitoringEntityModule,
    SisteminformasimonitoringAppRoutingModule,
    NgbModule,
    //        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
    NgJhipsterModule.forRoot({
      // set below to true to make alerts look like toast
      alertAsToast: true,
      alertTimeout: 5000,
      i18nEnabled: true,
      defaultI18nLang: 'in'
    })
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    SidebarComponent,
    LoginComponent
  ],
  providers: [
    NgbActiveModal,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    }
  ],
  bootstrap: [MainComponent]
})
export class SistemsentralisasifidusiaAppModule {
  constructor(private dpConfig: NgbDatepickerConfig) {
    this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
  }
}
