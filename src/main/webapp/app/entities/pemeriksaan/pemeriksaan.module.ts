import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { PemeriksaanComponent } from './pemeriksaan.component';
import { EditPengajuanModalComponent } from './editpengajuan-modal.component';
import { CreatePengajuanModalComponent } from './createpengajuan-modal.component';
import { pemeriksaanState } from './pemeriksaan.route';
import { PendampingModalComponent } from './pendamping-modal.component';
import { UploadFotoModalComponent } from './uploadfoto-modal.component';
import { EditPemeriksaanModalComponent } from './editpemeriksaan-modal.component';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(pemeriksaanState)],
  declarations: [PemeriksaanComponent, EditPengajuanModalComponent, CreatePengajuanModalComponent, PendampingModalComponent, UploadFotoModalComponent, EditPemeriksaanModalComponent],
  entryComponents: [EditPengajuanModalComponent, CreatePengajuanModalComponent, PendampingModalComponent, UploadFotoModalComponent, EditPemeriksaanModalComponent]
})
export class PemeriksaanSehatiModule {}
