import { Routes } from '@angular/router';
import { PemeriksaanComponent } from './pemeriksaan.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const pemeriksaanState: Routes = [{
  path: '',
  component: PemeriksaanComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
