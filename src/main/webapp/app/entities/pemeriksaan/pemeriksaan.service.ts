import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { IProvinsiDTO } from 'app/shared/model/provinsiDTO.model';
import { IKabupatenDTO } from 'app/shared/model/kabupatenDTO.model';
import { IDaftarProdukDTO } from 'app/shared/model/daftarprodukDTO.model';
import { IPendampingDTO } from 'app/shared/model/pendampingDTO.model';

type EntityArrayResponseType = HttpResponse<IPengajuanDTO[]>;
type EntityArrayResponseTypeProvinsi = HttpResponse<IProvinsiDTO[]>;
type EntityArrayResponseTypeKabupaten = HttpResponse<IKabupatenDTO[]>;
type EntityArrayResponseTypeDaftarProduk = HttpResponse<IDaftarProdukDTO[]>;
type EntityResponseType = HttpResponse<IPengajuanDTO>;

@Injectable({ providedIn: 'root' })
export class PemeriksaanService {
  	  public resourceUrlPengajuan = SERVER_API_URL + 'api/pengajuan/';
  	  public resourceUrlPengajuanDel = SERVER_API_URL + 'api/pengajuan/delete/';
  	  public resourceUrlProvinsi = SERVER_API_URL + 'api/provinsi/getall';
  	  public resourceUrlKabupaten = SERVER_API_URL + 'api/kabupaten/provinsi/';
  	  public resourceUrlDaftarProduk = SERVER_API_URL + 'api/daftar-produk/getall';
  	  public resourceUrlPendamping = SERVER_API_URL + 'api/pendamping/getlogin';
	
	  constructor(protected http: HttpClient) {}

	  delete(id: number): Observable<HttpResponse<{}>> {
	    return this.http.delete(this.resourceUrlPengajuanDel + id, { observe: 'response' });
	  }
	  
	  create(pengajuan: IPengajuanDTO): Observable<EntityResponseType> {
	    return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'save', pengajuan, { observe: 'response' });
	  }

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IPengajuanDTO[]>(this.resourceUrlPengajuan + 'searchbycase', { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }

	  provinsi(req?: any): Observable<EntityArrayResponseTypeProvinsi> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IProvinsiDTO[]>(this.resourceUrlProvinsi, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeProvinsi) => this.convertDateArrayFromServer(res)));
	  }

	  kabupaten(id: any): Observable<EntityArrayResponseTypeKabupaten> {
	    return this.http
	      .get<IKabupatenDTO[]>(this.resourceUrlKabupaten + id, { observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeKabupaten) => this.convertDateArrayFromServer(res)));
	  }

	  daftarProduk(req?: any): Observable<EntityArrayResponseTypeDaftarProduk> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IDaftarProdukDTO[]>(this.resourceUrlDaftarProduk, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeDaftarProduk) => this.convertDateArrayFromServerDaftar(res)));
	  }
	  
	  upload(file: File): any {
	    const formData: FormData = new FormData();
	    formData.append('file', file);
	    return this.http.post(`${this.resourceUrlDaftarProduk}/simple-form-upload-mvc`, formData);
	  }

	  pendampingProfil(): Observable<IPendampingDTO> {
	    return this.http.get<IPendampingDTO>(this.resourceUrlPendamping);
	  }

	  checkAdmin(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'getAdmin/' + id, { observe: 'response' });
	  }

	  setAdmin(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'setAdmin/' + id, { observe: 'response' });
	  }

	  unSetAdmin(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'unSetAdmin/' + id, { observe: 'response' });
	  }

	  reject(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'reject/' + id, { observe: 'response' });
	  }

	  complete(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'complete/' + id, { observe: 'response' });
	  }

	  submit(id: any): Observable<HttpResponse<any>>{
	    return this.http.get<EntityArrayResponseType>(this.resourceUrlPengajuan + 'submit/' + id, { observe: 'response' });
	  }
	  
	  updateSertifikat(id: any, req?: any): Observable<EntityResponseType> {
	    const options = createRequestOption(req);
	    	return this.http.post<EntityResponseType>(this.resourceUrlPengajuan + 'update-sertifikat/' + id, options, {observe: 'response' });
	  }

	  getDocImage(fileName?: string): Observable<Blob> {
	    const options = { responseType: 'blob' as 'json' };
	    return this.http.get<Blob>(
	      this.resourceUrlPengajuan+ "download" + (fileName ? `?fileName=${fileName}` : ''),
	      options
	    );
	  }

/*

  checkOut(): Observable<HttpResponse<any>> {
    return this.http.put(this.resourceUrlCheckOut, null, { observe: 'response' });
  }*/
	  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
		    if (res.body) {
		      res.body.forEach((r: IPengajuanDTO) => {
		        r.tglPengajuan = r.tglPengajuan ? moment(r.tglPengajuan) : undefined;
		        r.tglSertifikat = r.tglSertifikat ? moment(r.tglSertifikat) : undefined;
		        r.tglExp = r.tglExp ? moment(r.tglExp) : undefined;
		      });
		    }
	    return res;
	  }
	
	  protected convertDateArrayFromServerProv(res: EntityArrayResponseTypeProvinsi): EntityArrayResponseTypeProvinsi {
	    return res;
	  }

  protected convertDateArrayFromServerKab(res: EntityArrayResponseTypeKabupaten): EntityArrayResponseTypeKabupaten {
    return res;
  }

  protected convertDateArrayFromServerDaftar(res: EntityArrayResponseTypeDaftarProduk): EntityArrayResponseTypeKabupaten {
    return res;
  }
	
}