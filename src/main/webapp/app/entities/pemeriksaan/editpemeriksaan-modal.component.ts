import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPengajuanDTO, PengajuanDTO } from 'app/shared/model/pengajuanDTO.model'; 
import { IProvinsiDTO } from 'app/shared/model/provinsiDTO.model';
import { IKabupatenDTO } from 'app/shared/model/kabupatenDTO.model';
import { IDaftarProdukDTO } from 'app/shared/model/daftarprodukDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiEventManager, JhiDataUtils  } from 'ng-jhipster';
import { PengajuanService } from '../pengajuan/pengajuan.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-editpemeriksaan-modal',
  templateUrl: './editpemeriksaan-modal.component.html',
  styleUrls: ['pemeriksaan.scss']
})
export class EditPemeriksaanModalComponent implements OnInit  {
  pengajuan?: IPengajuanDTO |any;
  provinsis?: IProvinsiDTO[] |any;
  kabupatens?: IKabupatenDTO[] |any;
  produks?: IDaftarProdukDTO[] |any;
  isSaving = false;
  idProv ?: "" ;
  idKab ?: number;
  idProd?:"";
  formulir !: File;
  ktp !: File;
  fotoProduk !: File;
  fotoPemilihUsaha !: File;
  sizeFoto!: number;
  selectedFiles!: FileList;
  currentFile!: File;
  
   editForm = this.fb.group({
    nama:['', [Validators.required]],
    nik:['', [Validators.required]],
    email:['', [Validators.required]],
    noHp:['', [Validators.required]],
    alamat:['', [Validators.required]],
    namaUsaha:['', [Validators.required]],
    merkDagang:['', [Validators.required]],
    alamatUsaha:['', [Validators.required]],
    kodepos:['', [Validators.required]],
    formPengajuan:[''],
    fotoProduk:[''],
    fotoPemilihUsaha:[''],
    ktp:[''],
    id:[''],
    nib:['', [Validators.required]],
    prov:['', [Validators.required]],
    kab:['', [Validators.required]],
    prod:['', [Validators.required]]
  });

  constructor(
	  	public activeModal: NgbActiveModal,
    	private fb: FormBuilder,
    	protected pengajuanService: PengajuanService,
    	protected eventManager: JhiEventManager,
    	protected dataUtils: JhiDataUtils,
    ) {
	    this.pengajuanService
	      .provinsi({
		        size: 1000
		      })
	      .subscribe((res: HttpResponse<IProvinsiDTO[]>) => this.provinsis = res.body || []);
	    this.pengajuanService
	      .daftarProduk({
		        size: 2000
		      })
	      .subscribe((res: HttpResponse<IDaftarProdukDTO[]>) => this.produks = res.body || []);
	}
  
  
  ngOnInit(): void {
	  this.updateForm(this.pengajuan);
  }

  dismiss(): void {
    this.activeModal.dismiss();
    this.eventManager.broadcast('pemeriksaanListModification');
  }
  

  update(): void {
    this.isSaving = true;
    const pengajuan = this.createFromForm();
    
    if (pengajuan) {
	    this.pengajuanService.update(pengajuan)
	      .subscribe((res: HttpResponse<IProvinsiDTO>) => {
	      	this.pengajuan = res.body
    		this.subscribeToSaveResponse(this.pengajuanService.create2( this.pengajuan.id,this.formulir, this.ktp, this.fotoProduk, this.fotoPemilihUsaha));
		  });
	}
  }
  
  selectFile(kategori: string,event: any): void {
	  if (kategori === 'formulir') {
    	this.formulir = event.target.files.item(0);
    	console.log(this.formulir);
	  } else if (kategori === 'ktp') {
    	this.ktp = event.target.files.item(0);
    	console.log(this.ktp);
	  } else if (kategori === 'fotoProduk') {
    	this.fotoProduk = event.target.files.item(0);
    	console.log(this.fotoProduk);
	  } else {
    	this.fotoPemilihUsaha = event.target.files.item(0);
    	console.log(this.fotoPemilihUsaha);
	  }
  }
  
  setFileData(event: any, field: string) : void {
	  if (event.target) {
 		 const formData = new FormData();
          formData.append('file', event.target.files.item(0));
          formData.append('tipe', field);
          this.subscribeToSaveResponse(this.pengajuanService.upload(formData));
	  }
    }
    
    onChangeProd(event: any): void {
	  this.idProd = event;
	}
	
	onChangeKab(event: any): void {
	  this.idKab = event;
	}
	
  onChange(id: any): void {
	    this.pengajuanService
	      .kabupaten(id)
	      .subscribe((res: HttpResponse<IKabupatenDTO[]>) => this.kabupatens = res.body || []);

		this.editForm.patchValue({
		  kab: '',
		});
		this.idProv = id;
	  
  }
    
  updateForm(pengajuan: IPengajuanDTO): void {
	this.onChange(pengajuan.provinsi!.id);
	
    this.editForm.patchValue({
     	id: pengajuan.id,
	    nama: pengajuan.nama,
	    nik: pengajuan.nik,
	    email: pengajuan.email,
	    noHp: pengajuan.noHp,
	    alamat: pengajuan.alamat,
	    namaUsaha: pengajuan.namaUsaha,
	    merkDagang: pengajuan.merkDagang,
	    alamatUsaha: pengajuan.alamatUsaha,
	    kodepos: pengajuan.kodepos,
	    nib: pengajuan.nib,
	    prov: pengajuan.provinsi,
	    kab: pengajuan.kabupaten,
	    prod: pengajuan.daftarProduk,
    });
  }
    
  
  private createFromForm(): IPengajuanDTO {
    return {
      ...new PengajuanDTO(),
		id: this.editForm.get(['id'])!.value,
	    nama: this.editForm.get(['nama'])!.value,
	    nik: this.editForm.get(['nik'])!.value,
	    alamat: this.editForm.get(['alamat'])!.value,
	    kodepos: this.editForm.get(['kodepos'])!.value,
	    email: this.editForm.get(['email'])!.value,
	    noHp: this.editForm.get(['noHp'])!.value,
	    namaUsaha: this.editForm.get(['namaUsaha'])!.value,
	    alamatUsaha: this.editForm.get(['alamatUsaha'])!.value,
	    nib: this.editForm.get(['nib'])!.value,
	    merkDagang: this.editForm.get(['merkDagang'])!.value,
	    provinsiid: this.editForm.get(['prov'])!.value.id === undefined ? this.idProv : this.editForm.get(['prov'])!.value.id,
	    kabupatenid: this.editForm.get(['kab'])!.value.id === undefined ? this.idKab : this.editForm.get(['kab'])!.value.id,
	    daftarpordukid: this.editForm.get(['prod'])!.value.id === undefined ? this.idProd : this.editForm.get(['prod'])!.value.id,
    };
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPengajuanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('pemeriksaanListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
