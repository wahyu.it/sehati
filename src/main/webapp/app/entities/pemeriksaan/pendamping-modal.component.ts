import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PemeriksaanService } from './pemeriksaan.service';
import { JhiEventManager  } from 'ng-jhipster';
import { IPendampingDTO } from 'app/shared/model/pendampingDTO.model';

@Component({
  selector: 'jhi-pendamping-modal',
  templateUrl: './pendamping-modal.component.html',
  styleUrls: ['pemeriksaan.scss']
})
export class PendampingModalComponent implements OnInit  {
  pendamping?: IPendampingDTO |any;
  
  constructor(
	  	public activeModal: NgbActiveModal,
    	protected eventManager: JhiEventManager,
   		protected pemeriksaanService: PemeriksaanService,) {}
  
  
  ngOnInit(): void {
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
}
