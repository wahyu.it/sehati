import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { PemeriksaanService } from './pemeriksaan.service';
import { EditPengajuanModalComponent } from './editpengajuan-modal.component';
import { CreatePengajuanModalComponent } from './createpengajuan-modal.component';
import { IPengajuanDTO, PengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { PendampingDTO } from 'app/shared/model/pendampingDTO.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PendampingModalComponent } from './pendamping-modal.component';
import { EditPemeriksaanModalComponent } from './editpemeriksaan-modal.component';
import { UploadFotoModalComponent } from './uploadfoto-modal.component';

@Component({
  selector: 'jhi-pemeriksaan',
  templateUrl: './pemeriksaan.component.html',
  styleUrls: ['pemeriksaan.scss']
})
export class PemeriksaanComponent implements OnInit {
  pengajuans?: IPengajuanDTO[] |any;
  pendamping?: PendampingDTO |any;
  pengajuan?: PengajuanDTO |any;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;
  saldo!: number | undefined;
  admin!: string;

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    ppknomor: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    ppknomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected pemeriksaanService: PemeriksaanService,
    private datePipe: DatePipe,
    protected router: Router,
    private modalService: NgbModal,
	) {
		this.sPpd = true;	
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	      this.loadPage();
	    });
    this.registerChangeInPengajuan();
    this.loadData();
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'tglPengajuan';
	  } else {
		 this.sPpd = false;
		 this.key = 'blnPengajuan';
	  }
  }
  
  searchppk(nomor: string): void {
	  let ppkNo = this.ppkGroup.get(['ppknomor'])!.value;;
	  if (this.key === 'ppkNomor') {
		  ppkNo = this.ppkGroup.get(['ppknomor'])!.value;
	  }
      this.searchByParam(nomor, ppkNo);
  }

  searchPpd(key: string): void {
	  console.log(this.key + " " + key)
	  if (key === 'ppdDate') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  	  console.log("this.month : " +this.month)
	  } 
	  
      this.searchByParam(this.key, this.value);
  }

  clear(): void {
    this.editForm.patchValue({
      nama: ''
    });
    this.key ='';
    this.value='';
    this.loadData();
  }
  
  uploadPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(UploadFotoModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/pemeriksaan'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  transition(): void {
      this.router.navigate(['/pemeriksaan'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  private loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.pemeriksaanService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.pemeriksaanService
        .search({
          skey: this.key,
          svalue: this.value,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }
  
// open new tab 
  openDoc(fileName: string): void {
      window.open(fileName, '_blank');
  }

  sort(): string[] {
    const result = [this.predicate + ',' +  'desc'];
    if (this.predicate !== 'tglPengajuan') {
      result.push('tglPengajuan');
    }
    return result;
  }

  registerChangeInPengajuan(): void {
    this.eventSubscriber = this.eventManager.subscribe('pemeriksaanListModification', () => this.loadPage());
  }
  
  editPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(EditPengajuanModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  check(id: any): void {
	  this.pemeriksaanService.checkAdmin(id).subscribe((res: HttpResponse<any>)  => {
			this.admin = res.body.verifyBy;
			if (this.admin === null) {
	  			this.pemeriksaanService.setAdmin(id).subscribe((res2: HttpResponse<any>)  => {
					this.pengajuan = res2.body;
					this.loadPage();
				  })
			} else {
				Swal.fire({
				      icon: 'info',
				      text: 'Pengajuan ini sudah dikerjakan Admin Lain, Silahkan pilih pengajuan lain !!!',
				      showCancelButton: false,
				    })
				this.loadPage();
			}
		}); 
  }
  
  unSetAdmin(id: any): void {
	this.pemeriksaanService.unSetAdmin(id).subscribe((res2: HttpResponse<any>)  => {
		this.pengajuan = res2.body;
		this.loadPage();
	  })
  }
  
  confirmDelete(id: number): void {
    Swal.fire({
      type: 'warning',
      title: 'DELETE PENGAJUAN',
      showCancelButton: true,
      confirmButtonText: 'yes',
      cancelButtonText: 'no',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.pemeriksaanService.delete(id).subscribe(
          (res: HttpResponse<IPengajuanDTO>) => this.onDeleteSuccess(res),
          (res: HttpErrorResponse) => this.onDeleteError(res)
        );
      }
    });
  }
  

  protected onDeleteSuccess(response: any): void {
    console.log('result:', response);
    this.loadPage();
  }

  protected onDeleteError(response?: any): void {
    console.log('result:', response);
  }
  
  reject(id: any): void {
	this.pemeriksaanService.reject(id).subscribe((res2: HttpResponse<any>)  => {
		this.pengajuan = res2.body;
		this.loadPage();
	  })
  }
  
  submit(id: any): void {
	this.pemeriksaanService.submit(id).subscribe((res2: HttpResponse<any>)  => {
		this.pengajuan = res2.body;
		this.loadPage();
	  })
  }
  
  editPemeriksaan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(EditPemeriksaanModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  openPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(CreatePengajuanModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  openPendamping(pendamping: PendampingDTO): void {
    const modalRef = this.modalService.open(PendampingModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pendamping = pendamping;
  }
  
  protected onSuccess(data: IPengajuanDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/pemeriksaan'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.pengajuans = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    return result
  }
}
