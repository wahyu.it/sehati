import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import { PemeriksaanService } from './pemeriksaan.service';
import { Observable } from 'rxjs';
import { JhiEventManager  } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-editpengajuan-modal',
  templateUrl: './editpengajuan-modal.component.html',
  styleUrls: ['pemeriksaan.scss']
})
export class EditPengajuanModalComponent implements OnInit  {
  pengajuan?: IPengajuanDTO |any;
  isSaving = false;
  
   editForm = this.fb.group({
    nomor:['', [Validators.required]],
    link:['', [Validators.required]],
  });

  constructor(
	  	public activeModal: NgbActiveModal,
    	private fb: FormBuilder,
    	protected eventManager: JhiEventManager,
   		protected pemeriksaanService: PemeriksaanService,) {}
  
  
  ngOnInit(): void {
	  console.log("pengajuan : " + this.pengajuan.nama);
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
  
  save(id?: any): void {
    this.isSaving = true;
    this.subscribeToSaveResponse(
	    this.pemeriksaanService.updateSertifikat(id, {
			nomor: this.editForm.get(['nomor'])!.value,
			link : this.editForm.get(['link'])!.value
		})
	)     
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPengajuanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('pemeriksaanListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
  
  
}
