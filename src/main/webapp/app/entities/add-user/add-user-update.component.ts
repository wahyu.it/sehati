import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';

import { LANGUAGES } from 'app/core/language/language.constants';
import { User, IUserDTO, UserDTO } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ILembaga } from 'app/shared/model/lembaga.model';
import { LembagaUpdateComponent } from './lembaga-update.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'jhi-add-user-update',
  templateUrl: './add-user-update.component.html'
})
export class AddUserUpdateComponent implements OnInit {
  user!: User;
  userDTO !: IUserDTO;
  languages = LANGUAGES;
  authorities: string[] = [];
  isSaving = false;
  lembaga?: ILembaga[] |any;
  eventSubscriber?: Subscription;
  hide = true;
  hide2 = true;
  hide3 = true;
  lembagaId ?: any;

  editForm = this.fb.group({
    id: [],
    login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('^[_.@A-Za-z0-9-]*')]],
    firstName: ['', [Validators.maxLength(50)]],
    password: ['', [Validators.maxLength(50)]],
    lastName: ['', [Validators.maxLength(50)]],
    email: ['', [Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    activated: [],
    langKey: [],
    authorities: [],
    lembaga:[],
    auth:[]
  });

  constructor(
      protected eventManager: JhiEventManager,
      private modalService: NgbModal,
	  private userService: UserService, 
	  private route: ActivatedRoute, 
	  private fb: FormBuilder) {
		  
	    this.userService
	      .queryLembaga()
	      .subscribe((res: HttpResponse<ILembaga[]>) => this.lembaga = res.body || []);
	  }

  ngOnInit(): void {
  /*  this.route.data.subscribe(({ user }) => {
      if (user) {
        this.user = user;
        if (this.user.id === undefined) {
          this.user.activated = true;
        }
        this.updateForm(user);
      }
    });*/
    this.userService.authorities().subscribe(authorities => {
      this.authorities = authorities;
    });
  }
  
  onChange(event: Event): void {
	  console.log(event);
	  this.lembagaId = event;
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
   this.isSaving = true;
    const user = this.createFromForm();
    
    if (user) 
    	this.subscribeToSaveResponse(this.userService.create2(user));
  }
  
  onChangePisah(event: any) : void {
	  if (event === 'pisah'){
		  this.hide3 = false;
	  } else {
	  	  console.log(event)
		  this.hide3 = true;
	  }
  }
  
  onChangeAut(event: any): void {
	  if (event === '2'){
		  this.hide = false;
//		  this.hide2 = false;
	  } else {
		  this.hide = true;
	  }
	  
	  
	  if (event === '2') {
		  this.hide2 = false;
	  } else if (event === '3'){
		  this.hide2 = false;
	  } else {
		  this.hide2 = true;
	  }
  }
  
  loadLembaga(): void {
    this.userService
      .queryLembaga()
      .subscribe((res: HttpResponse<ILembaga[]>) => this.lembaga = res.body || []);
  }
  
  registerChangeInPengajuan(): void {
    this.eventSubscriber = this.eventManager.subscribe('lembagaListModification', () => this.loadLembaga());
  }
  
  createLambaga(): void {
    const modalRef = this.modalService.open(LembagaUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.lembaga = '';
  }
  
 private createFromForm(): UserDTO {
    return {
      ...new UserDTO(),
      id: this.editForm.get(['id'])!.value,
      login: this.editForm.get(['login'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      password: this.editForm.get(['password'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      email: this.editForm.get(['email'])!.value,
      lembagaId:this.lembagaId,
      auth:this.editForm.get(['auth'])!.value,
    };
  }
  
  protected subscribeToSaveResponse(result: Observable<IUserDTO>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  private onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  private onSaveError(): void {
    this.isSaving = false;
  }
}
