import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager  } from 'ng-jhipster';

import { LANGUAGES } from 'app/core/language/language.constants';
import { User } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ILembaga, Lembaga } from 'app/shared/model/lembaga.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-lembaga-update',
  templateUrl: './lembaga-update.component.html'
})
export class LembagaUpdateComponent implements OnInit {
  user!: User;
  languages = LANGUAGES;
  authorities: string[] = [];
  isSaving = false;
  lembaga?: ILembaga[] |any;

  editForm = this.fb.group({
    id: [],
    name: ['', [Validators.maxLength(100)]],
  });

  constructor(
      protected eventManager: JhiEventManager,
	  public activeModal: NgbActiveModal,
	  private userService: UserService, 
	  private fb: FormBuilder) {
	  }

  ngOnInit(): void {
  }

 private createFromForm(): ILembaga {
    return {
      ...new Lembaga(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
  

  dismiss(): void {
    this.activeModal.dismiss();
  }
  
  save1(): void {
    this.isSaving = true;
    const lembaga = this.createFromForm();
    
    if (lembaga) {
      this.subscribeToSaveResponse(this.userService.createLembaga(lembaga));
	}
  }

  success(): void {
	  Swal.fire({
		      icon: 'info',
		      text: 'Lembaga Baru Berhasil di Simpan !!!',
		      showCancelButton: false,
		    } as any);
  }
  
  protected subscribeToSaveResponse(result: Observable<ILembaga>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.success();
    this.eventManager.broadcast('pengajuantListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
