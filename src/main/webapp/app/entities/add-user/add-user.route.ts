import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { User, IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { AddUserComponent } from './add-user.component';
import { AddUserDetailComponent } from './add-user-detail.component';
import { AddUserUpdateComponent } from './add-user-update.component';

@Injectable({ providedIn: 'root' })
export class AddUserResolve implements Resolve<IUser> {
  constructor(private service: UserService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUser> {
    const id = route.params['login'] ? route.params['login'] : null;
    if (id) {
      return this.service.find(id);
    }
    return of(new User());
  }
}

export const addUserRoute: Routes = [
  {
    path: '',
    component: AddUserComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      defaultSort: 'id,asc'
    }
  },
  {
    path: ':login/view',
    component: AddUserDetailComponent,
    resolve: {
      user: AddUserResolve
    }
  },
  {
    path: 'new',
    component: AddUserUpdateComponent,
    resolve: {
      user: AddUserResolve
    }
  },
  {
    path: ':login/edit',
    component: AddUserUpdateComponent,
    resolve: {
      user: AddUserResolve
    }
  }
];
