import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { AddUserComponent } from './add-user.component';
import { AddUserDetailComponent } from './add-user-detail.component';
import { AddUserUpdateComponent } from './add-user-update.component';
import { AddUserDeleteDialogComponent } from './add-user-delete-dialog.component';
import { LembagaUpdateComponent } from './lembaga-update.component';
import { addUserRoute } from './add-user.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(addUserRoute)],
  declarations: [
    AddUserComponent,
    AddUserDetailComponent,
    AddUserUpdateComponent,
    AddUserDeleteDialogComponent,
    LembagaUpdateComponent
  ],
  entryComponents: [AddUserDeleteDialogComponent, LembagaUpdateComponent]
})
export class AddUserModule {}
