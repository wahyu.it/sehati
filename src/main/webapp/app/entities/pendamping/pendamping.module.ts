import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { PendampingComponent } from './pendamping.component';
import { PendampingModalComponent } from './pendamping-modal.component';
import { pendampingState } from './pendamping.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(pendampingState)],
  declarations: [PendampingComponent, PendampingModalComponent],
  entryComponents: [PendampingModalComponent]
})
export class PendampingSehatiModule {}
