import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IPendampingDTO } from 'app/shared/model/pendampingDTO.model';

type EntityArrayResponseType = HttpResponse<IPendampingDTO[]>;
type EntityResponseType = HttpResponse<IPendampingDTO>;

@Injectable({ providedIn: 'root' })
export class PendampingService {
  	  public resourceUrl = SERVER_API_URL + 'api/pendamping/';
	
	  constructor(protected http: HttpClient) {}
	  
	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IPendampingDTO[]>(this.resourceUrl+ 'getall', { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }
	  
	  create(pengajuan: IPendampingDTO): Observable<EntityResponseType> {
	    return this.http.post<IPendampingDTO>(this.resourceUrl + 'edit', pengajuan, { observe: 'response' });
	  }
	  
	  active(pengajuan: IPendampingDTO): Observable<EntityResponseType> {
	    return this.http.post<IPendampingDTO>(this.resourceUrl + 'active', pengajuan, { observe: 'response' });
	  }

	  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
	    return res;
	  }
	
	
}