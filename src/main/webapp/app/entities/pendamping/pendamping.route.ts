import { Routes } from '@angular/router';
import { PendampingComponent } from './pendamping.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const pendampingState: Routes = [{
  path: '',
  component: PendampingComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
