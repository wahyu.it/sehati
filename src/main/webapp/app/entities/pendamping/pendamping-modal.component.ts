import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPendampingDTO, PendampingDTO } from 'app/shared/model/pendampingDTO.model';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PendampingService } from './pendamping.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-pendamping-modal',
  templateUrl: './pendamping-modal.component.html'
})
export class PendampingModalComponent implements OnInit  {
  pendamping?: IPendampingDTO |any;
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nama: [],
    saldo: [],
    freesaldo: [],
    username: [],
    passwrod: []
  });

  constructor(
		public activeModal: NgbActiveModal,
    	protected activatedRoute: ActivatedRoute,
    	protected pendampingService: PendampingService,
    	protected eventManager: JhiEventManager,
    	private fb: FormBuilder
    ) {}
  
  
  ngOnInit(): void {
	  this.updateForm(this.pendamping);
  }
  
  updateForm(pendamping: IPendampingDTO): void {
    this.editForm.patchValue({
      id: pendamping.id,
      nama: pendamping.nama,
      username: pendamping.username,
      password: pendamping.password,
      saldo: pendamping.saldo,
      freesaldo: pendamping.freeSaldo,
      status: pendamping.status
    });
  }
  
  simpan():void {
    this.isSaving = true;
    const pendamping = this.createFromForm();
    this.subscribeToSaveResponse(this.pendampingService.create(pendamping));
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

 private createFromForm(): IPendampingDTO {
    return {
      ...new PendampingDTO(),
      id: this.editForm.get(['id'])!.value,
      nama: this.editForm.get(['nama'])!.value,
      saldo: this.editForm.get(['saldo'])!.value,
      freeSaldo: this.editForm.get(['freesaldo'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPendampingDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('pendampingListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
