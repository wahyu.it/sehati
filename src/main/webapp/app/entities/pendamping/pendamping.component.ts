import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { PendampingService } from './pendamping.service';
import { PendampingModalComponent } from './pendamping-modal.component';
import { IPendampingDTO, PendampingDTO } from 'app/shared/model/pendampingDTO.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'jhi-pendamping',
  templateUrl: './pendamping.component.html',
  styleUrls: ['pendamping.scss']
})
export class PendampingComponent implements OnInit {
  pendampings?: PendampingDTO |any;
  pendamping?: IPendampingDTO |any;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  key!: string;
  value!: string;
  saldo!: number | undefined;

  editForm = new FormGroup({
    nama: new FormControl(''),
    id:  new FormControl(''),
    saldo:  new FormControl(''),
    freesaldo:  new FormControl(''),
    username:  new FormControl(''),
    passwrod:  new FormControl('')
  });
  
  
  
  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected pendampingService: PendampingService,
    protected router: Router,
    private modalService: NgbModal,
	) {
	}

  ngOnInit(): void {
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	      this.loadPage();
	    });
    this.registerChangeInPengajuan();
    this.loadData();
  }
  
  
  searchnama(): void {
	  const nama = this.editForm.get(['nama'])!.value;;
      this.searchByParam('snama', nama);
  }

  active(pendamping: IPendampingDTO, status: string): void {
	  console.log(status)
	const st = 'true' === status ? false : true;
	pendamping.status = st;
	  console.log(st)
    this.subscribeToSaveResponse(this.pendampingService.active(pendamping));
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/pendamping'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  transition(): void {
      this.router.navigate(['/pendamping'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  private loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.pendampingService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<PendampingDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.pendampingService
        .search({
          skey: this.key,
          svalue: this.value,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<PendampingDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInPengajuan(): void {
    this.eventSubscriber = this.eventManager.subscribe('pendampingListModification', () => this.loadPage());
  }
  
  editPengajuan(pendamping: PendampingDTO): void {
    const modalRef = this.modalService.open(PendampingModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pendamping = pendamping;
  }

  clear(): void {
    this.editForm.patchValue({
      nama: ''
    });
    this.key ='';
    this.value='';
    this.loadData();
  }
  
  warning(): void {
	  Swal.fire({
		      icon: 'info',
		      text: ' Saldo anda tidak mencukupi, segera lakukan isi ulang !!!',
		      showCancelButton: false,
		    } as any);
  }
  
/*  openPengajuan(): void {
    const modalRef = this.modalService.open(PendampingModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = "";
  }*/
  
  protected onSuccess(data: PendampingDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/pendamping'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.pendampings = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
  
  

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPendampingDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.eventManager.broadcast('pendampingListModification');
  }

  protected onSaveError(): void {
  }
}
