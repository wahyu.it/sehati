import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { PengajuanComponent } from './pengajuan.component';
import { EditPengajuanModalComponent } from './editpengajuan-modal.component';
import { CreatePengajuanModalComponent } from './createpengajuan-modal.component';
import { DetailPengajuanModalComponent } from './detailpengajuan-modal.component';
import { UploadFotoModalComponent } from './uploadfoto-modal.component';
import { pengajuanState } from './pengajuan.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(pengajuanState)],
  declarations: [PengajuanComponent, EditPengajuanModalComponent, CreatePengajuanModalComponent, UploadFotoModalComponent,DetailPengajuanModalComponent],
  entryComponents: [EditPengajuanModalComponent, CreatePengajuanModalComponent, UploadFotoModalComponent, DetailPengajuanModalComponent]
})
export class PengajuanSehatiModule {}
