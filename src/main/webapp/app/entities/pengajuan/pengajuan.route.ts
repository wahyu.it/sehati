import { Routes } from '@angular/router';
import { PengajuanComponent } from './pengajuan.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const pengajuanState: Routes = [{
  path: '',
  component: PengajuanComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
