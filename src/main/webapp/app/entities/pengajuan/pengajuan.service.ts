import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { IProvinsiDTO } from 'app/shared/model/provinsiDTO.model';
import { IKabupatenDTO } from 'app/shared/model/kabupatenDTO.model';
import { IDaftarProdukDTO } from 'app/shared/model/daftarprodukDTO.model';
import { IPendampingDTO } from 'app/shared/model/pendampingDTO.model';

type EntityArrayResponseType = HttpResponse<IPengajuanDTO[]>;
type EntityArrayResponseTypeProvinsi = HttpResponse<IProvinsiDTO[]>;
type EntityArrayResponseTypeKabupaten = HttpResponse<IKabupatenDTO[]>;
type EntityArrayResponseTypeDaftarProduk = HttpResponse<IDaftarProdukDTO[]>;
type EntityResponseType = HttpResponse<IPengajuanDTO>;

@Injectable({ providedIn: 'root' })
export class PengajuanService {
  	  public resourceUrlPengajuan = SERVER_API_URL + 'api/pengajuan/';
  	  public resourceUrlPengajuanDel = SERVER_API_URL + 'api/pengajuan/delete';
  	  public resourceUrlProvinsi = SERVER_API_URL + 'api/provinsi/getall';
  	  public resourceUrlKabupaten = SERVER_API_URL + 'api/kabupaten/provinsi/';
  	  public resourceUrlDaftarProduk = SERVER_API_URL + 'api/daftar-produk/getall';
  	  public resourceUrlPendamping = SERVER_API_URL + 'api/pendamping/getlogin';
	
	  constructor(protected http: HttpClient) {}

	  delete(id: number): Observable<HttpResponse<{}>> {
	    return this.http.delete(this.resourceUrlPengajuanDel + id, { observe: 'response' });
	  }
	  
	  createAll(id: any, formulir?: any, ktp?: any, fotoProduk?: any, fotoPemilihUsaha?: any): Observable<EntityResponseType> {
		const formData = new FormData();
		      formData.append('fileformulir', formulir);
		      formData.append('filektp', ktp);
		      formData.append('fileproduk', fotoProduk);
		      formData.append('filepu', fotoPemilihUsaha);
		const header = new HttpHeaders();
		  	header.append('Content-Type', 'multipart/form-data');
	    	return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'upload-doc-update/' + id, formData, { observe: 'response', headers: header });
	  }
	  
	  create(id: any, kategori: any, foto?: any): Observable<EntityResponseType> {
		const formData = new FormData();
		      formData.append('foto', foto);
		      formData.append('kategori', kategori);
		const header = new HttpHeaders();
		  	header.append('Content-Type', 'multipart/form-data');
	    	return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'upload-doc/' + id, formData, { observe: 'response', headers: header });
	  } 
	  
	  create2(id: any, formulir?: any, ktp?: any, fotoProduk?: any, fotoPemilihUsaha?: any): Observable<EntityResponseType> {
		const formData = new FormData();
		      formData.append('fileformulir', formulir);
		      formData.append('filektp', ktp);
		      formData.append('fileproduk', fotoProduk);
		      formData.append('filepu', fotoPemilihUsaha);
		const header = new HttpHeaders();
		  	header.append('Content-Type', 'multipart/form-data');
	    	return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'upload-doc-update/' + id, formData, { observe: 'response', headers: header });
	  }

	  save(pengajuan: IPengajuanDTO): Observable<EntityResponseType> {
	    	return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'save', pengajuan , { observe: 'response'});
	  }

	  update(pengajuan: IPengajuanDTO): Observable<EntityResponseType> {
	    	return this.http.post<IPengajuanDTO>(this.resourceUrlPengajuan + 'update', pengajuan , { observe: 'response'});
	  }

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IPengajuanDTO[]>(this.resourceUrlPengajuan + 'search', { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }

	  provinsi(req?: any): Observable<EntityArrayResponseTypeProvinsi> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IProvinsiDTO[]>(this.resourceUrlProvinsi, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeProvinsi) => this.convertDateArrayFromServer(res)));
	  }

	  kabupaten(id: any): Observable<EntityArrayResponseTypeKabupaten> {
	    return this.http
	      .get<IKabupatenDTO[]>(this.resourceUrlKabupaten + id, { observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeKabupaten) => this.convertDateArrayFromServer(res)));
	  }

	  daftarProduk(req?: any): Observable<EntityArrayResponseTypeDaftarProduk> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IDaftarProdukDTO[]>(this.resourceUrlDaftarProduk, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseTypeDaftarProduk) => this.convertDateArrayFromServerDaftar(res)));
	  }
	  
/*	  upload(file: File, id: string, tipe: string): any {
	    const formData: FormData = new FormData();
	    formData.append('file', file);
	    formData.append('id', id);
	    formData.append('tipe', tipe);
	    return this.http.post(this.resourceUrlPengajuan + 'upload-doc', formData);
	  }*/

	  upload(req?: any): Observable<any> {
		const header = new HttpHeaders();
		  header.append('Content-Type', 'multipart/form-data');
	    return this.http.post(this.resourceUrlPengajuan + 'upload-doc', req, {
			headers: header
		})
	  }
	  
	/*  public sendGetRequest() {
		  const myFormData = { email: 'abc@abc.com', password: '123' };
		  const headers = new HttpHeaders();
		  headers.append('Content-Type', 'application/json');
		
		  this.httpClient.post(this.REST_API_SERVER, myFormData, {
		    headers: headers,
		  })
		  .subscribe((data) => {
		    console.log(data, myFormData, headers);
		    return data;
		  });
		}*/

	  pendampingProfil(): Observable<IPendampingDTO> {
	    return this.http.get<IPendampingDTO>(this.resourceUrlPendamping);
	  }

	  getDocImage(fileName?: string): Observable<Blob> {
	    const options = { responseType: 'blob' as 'json' };
	    return this.http.get<Blob>(
	      this.resourceUrlPengajuan+ "download" + (fileName ? `?fileName=${fileName}` : ''),
	      options
	    );
	  }

	  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
		    if (res.body) {
		      res.body.forEach((r: IPengajuanDTO) => {
		        r.tglPengajuan = r.tglPengajuan ? moment(r.tglPengajuan) : undefined;
		        r.tglSertifikat = r.tglSertifikat ? moment(r.tglSertifikat) : undefined;
		        r.tglExp = r.tglExp ? moment(r.tglExp) : undefined;
		      });
		    }
	    return res;
	  }
	
	  protected convertDateArrayFromServerProv(res: EntityArrayResponseTypeProvinsi): EntityArrayResponseTypeProvinsi {
	    return res;
	  }

  protected convertDateArrayFromServerKab(res: EntityArrayResponseTypeKabupaten): EntityArrayResponseTypeKabupaten {
    return res;
  }

  protected convertDateArrayFromServerDaftar(res: EntityArrayResponseTypeDaftarProduk): EntityArrayResponseTypeKabupaten {
    return res;
  }
	
}