import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPengajuanDTO, PengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import { PengajuanService } from './pengajuan.service';
import { IProvinsiDTO } from 'app/shared/model/provinsiDTO.model';
import { IKabupatenDTO } from 'app/shared/model/kabupatenDTO.model';
import { IDaftarProdukDTO } from 'app/shared/model/daftarprodukDTO.model';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiEventManager, JhiDataUtils  } from 'ng-jhipster';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-createpengajuan-modal',
  templateUrl: './createpengajuan-modal.component.html',
  styleUrls: ['pengajuan.scss']
})
export class CreatePengajuanModalComponent implements OnInit  {
  pengajuan?: IPengajuanDTO |any;
  provinsis?: IProvinsiDTO[] |any;
  kabupatens?: IKabupatenDTO[] |any;
  produks?: IDaftarProdukDTO[] |any;
  isSaving = false;
  idProv?: "";
  idKab?: "";
  idProd?:"";
  formulir !: File;
  ktp !: File;
  fotoProduk !: File;
  fotoPemilihUsaha !: File;
  sizeFoto!: number;
  selectedFiles!: FileList;
  currentFile!: File;
  
   editForm = this.fb.group({
    nama:['', [Validators.required]],
    nik:['', [Validators.required]],
    email:['', [Validators.required]],
    noHp:['', [Validators.required]],
    alamat:['', [Validators.required]],
    namaUsaha:['', [Validators.required]],
    merkDagang:['', [Validators.required]],
    alamatUsaha:['', [Validators.required]],
    kodepos:['', [Validators.required]],
    /* formPengajuan:['', [Validators.required]],
    fotoProduk:['', [Validators.required]],
    fotoPemilihUsaha:['', [Validators.required]],
    ktp:['', [Validators.required]],*/
    nib:['', [Validators.required]],
    prov:['', [Validators.required]],
    kab:['', [Validators.required]],
    prod:['', [Validators.required]]
  });

  constructor(
	  	public activeModal: NgbActiveModal,
    	private fb: FormBuilder,
    	protected pengajuanService: PengajuanService,
    	protected eventManager: JhiEventManager,
    	protected dataUtils: JhiDataUtils,
    ) {
	    this.pengajuanService
	      .provinsi({
		        size: 1000
		      })
	      .subscribe((res: HttpResponse<IProvinsiDTO[]>) => this.provinsis = res.body || []);
	    this.pengajuanService
	      .daftarProduk({
		        size: 2000
		      })
	      .subscribe((res: HttpResponse<IDaftarProdukDTO[]>) => this.produks = res.body || []);
	}
  
  
  ngOnInit(): void {
  }
  
  onChange(id: number): void {
	    this.pengajuanService
	      .kabupaten(id)
	      .subscribe((res: HttpResponse<IKabupatenDTO[]>) => this.kabupatens = res.body || []);

		this.editForm.patchValue({
		  kab: this.kabupatens,
		});
	  
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
  

  save(): void {
    this.isSaving = true;
    const pengajuan = this.createFromForm();
    
    if (pengajuan) {
	   /* this.pengajuanService.save(pengajuan).subscribe((res: HttpResponse<IProvinsiDTO>) => {
	      	this.pengajuan = res.body
	      	this.subscribeToSaveResponse(this.pengajuan);
		  });*/
		  
		  
      this.subscribeToSaveResponse(this.pengajuanService.save(pengajuan));
	}
  }
  
  selectFile(kategori: string,event: any): void {
	  if (kategori === 'formulir') {
    	this.formulir = event.target.files.item(0);
    	console.log(this.formulir);
	  } else if (kategori === 'ktp') {
    	this.ktp = event.target.files.item(0);
    	console.log(this.ktp);
	  } else if (kategori === 'fotoProduk') {
    	this.fotoProduk = event.target.files.item(0);
    	console.log(this.fotoProduk);
	  } else {
    	this.fotoPemilihUsaha = event.target.files.item(0);
    	console.log(this.fotoPemilihUsaha);
	  }
  }

  upload(file: File, tipe: string): void {
	  console.log(file, tipe)
      const formData = new FormData();
          formData.append('file', file);
          formData.append('tipe', tipe);
          this.subscribeToSaveResponse(this.pengajuanService.upload(formData));
  }
  
  setFileData(event: any, field: string) : void {
	  if (event.target) {
 		 const formData = new FormData();
          formData.append('file', event.target.files.item(0));
          formData.append('tipe', field);
          this.subscribeToSaveResponse(this.pengajuanService.upload(formData));
	  }
    }
    

/*  setFileData(event: Event, field: string, isImage: boolean): void {
    if (event.target) {
      this.sizeFoto = 0;
      const files = (event.target as HTMLInputElement).files;
      if (files && files.length > 0) {
        const sizeFile = files[0].size;
        console.log('size' + files[0].size / 1024 + 'kb');
        if (sizeFile > 2097152) {
          this.sizeFoto = sizeFile;
        } else {
          this.upload(files, field);
          this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
            this.eventManager.broadcast(
              new JhiEventWithContent<AlertError>('pitoenetApp.error', { ...err, key: 'error.file.' + err.key })
            );
          });
        }
      }
    }
  }*/
  
  private createFromForm(): IPengajuanDTO {
    return {
      ...new PengajuanDTO(),
//	    id: this.editForm.get(['id'])!.value,
//		noPengajuan: this.editForm.get(['id'])!.value,
//	    tglPengajuan: this.editForm.get(['id'])!.value,
	    nama: this.editForm.get(['nama'])!.value,
	    nik: this.editForm.get(['nik'])!.value,
	    alamat: this.editForm.get(['alamat'])!.value,
	    kodepos: this.editForm.get(['kodepos'])!.value,
	    email: this.editForm.get(['email'])!.value,
	    noHp: this.editForm.get(['noHp'])!.value,
	    namaUsaha: this.editForm.get(['namaUsaha'])!.value,
	    alamatUsaha: this.editForm.get(['alamatUsaha'])!.value,
	    nib: this.editForm.get(['nib'])!.value,
	    merkDagang: this.editForm.get(['merkDagang'])!.value,
//	    fileFormPengajuan: this.editForm.get(['id'])!.value,
//	    fileKtp: this.editForm.get(['id'])!.value,
//	    fileFotoProduk: this.editForm.get(['id'])!.value,
//	    fotoPu: this.editForm.get(['id'])!.value,
//	    status: this.editForm.get(['id'])!.value,
//	    verifyBy: this.editForm.get(['id'])!.value,
//	    verifyOn: this.editForm.get(['id'])!.value,
//	    noSertifikat: this.editForm.get(['id'])!.value,
//	    tglSertifikat: this.editForm.get(['id'])!.value,
//	    tglExp: this.editForm.get(['id'])!.value,
	    provinsiid: this.editForm.get(['prov'])!.value,
//	    provinsinama: this.editForm.get(['id'])!.value,
	    kabupatenid: this.editForm.get(['kab'])!.value,
//	    kabupatennama: this.editForm.get(['id'])!.value,
	    daftarpordukid: this.editForm.get(['prod'])!.value,
//	    daftarporduknama: this.editForm.get(['id'])!.value,
//	    kab: this.editForm.get(['id'])!.value,
//	    prov: this.editForm.get(['id'])!.value,
//	    prod: this.editForm.get(['id'])!.value,
//	    pendamping: this.editForm.get(['id'])!.value,
    };
  }
  
  success(): void {
	  Swal.fire({
		      icon: 'info',
		      text: 'Pengajuan Berhasil di Simpan, Segera Upload Kelengkapan Dokumen !!!',
		      showCancelButton: false,
		    } as any);
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPengajuanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.success();
    this.eventManager.broadcast('pengajuantListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
