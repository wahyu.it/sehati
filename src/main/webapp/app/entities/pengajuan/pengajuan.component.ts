import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { PengajuanService } from './pengajuan.service';
import { EditPengajuanModalComponent } from './editpengajuan-modal.component';
import { CreatePengajuanModalComponent } from './createpengajuan-modal.component';
import { UploadFotoModalComponent } from './uploadfoto-modal.component';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { PendampingDTO } from 'app/shared/model/pendampingDTO.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { DetailPengajuanModalComponent } from './detailpengajuan-modal.component';
@Component({
  selector: 'jhi-pengajuan',
  templateUrl: './pengajuan.component.html',
  styleUrls: ['pengajuan.scss']
})
export class PengajuanComponent implements OnInit {
  pengajuans?: IPengajuanDTO[] |any;
  pendamping?: PendampingDTO |any;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;
  saldo!: number | undefined;

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    nomor: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    nomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected pengajuanService: PengajuanService,
    private datePipe: DatePipe,
    protected router: Router,
    private modalService: NgbModal,
	) {
		this.sPpd = true;	
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	      this.loadPage();
	    });
    this.registerChangeInPengajuan();
    this.loadData();
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'tglPengajuan';
	  } else {
		 this.sPpd = false;
		 this.key = 'blnPengajuan';
	  }
  }
  
  searchppk(noPengajuan: string): void {
	  let nomor = this.ppkGroup.get(['nomor'])!.value;;
	  if (this.key === 'noPengajuan') {
		  nomor = this.ppkGroup.get(['nomor'])!.value;
	  }
      this.searchByParam(noPengajuan, nomor);
  }

  searchPpd(key: string): void {
	  console.log(this.key + " " + key)
	  if (key === 'ppdDate') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  	  console.log("this.month : " +this.month)
	  } 
	  
      this.searchByParam(this.key, this.value);
  }
  
// open new tab 
  openDoc(fileName: string): void {
      window.open(fileName, '_blank');
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/pengajuan'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  transition(): void {
      this.router.navigate(['/pengajuan'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  private loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.pengajuanService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
      
		this.pengajuanService.pendampingProfil().subscribe(pendamping => {
			this.pendamping = pendamping;
			this.saldo = pendamping.saldo;
		});
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.pengajuanService
        .search({
          skey: this.key,
          svalue: this.value,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' +  'desc'];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInPengajuan(): void {
    this.eventSubscriber = this.eventManager.subscribe('pengajuantListModification', () => this.loadPage());
  }
  
  editPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(EditPengajuanModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  detailPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(DetailPengajuanModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  uploadPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(UploadFotoModalComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  createPengajuan(): void {
	   this.pengajuanService.pendampingProfil().subscribe(pendamping => {
			this.pendamping = pendamping;
		  if (pendamping.saldo === 0  && pendamping.freeSaldo === 0) {
			  this.warning();
		  } else {
			  this.openPengajuan();
		  }
		});
	  
  }
  
  warning(): void {
	  Swal.fire({
		      icon: 'info',
		      text: ' Saldo anda tidak mencukupi, segera lakukan isi ulang !!!',
		      showCancelButton: false,
		    } as any);
  }
  
  openPengajuan(): void {
    const modalRef = this.modalService.open(CreatePengajuanModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = "";
  }
  
  protected onSuccess(data: IPengajuanDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/pengajuan'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.pengajuans = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    console.log("result :" +result)
    return result
  }
}
