import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import { PengajuanService } from './pengajuan.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiEventManager, JhiDataUtils  } from 'ng-jhipster';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-uploadfoto-modal',
  templateUrl: './uploadfoto-modal.component.html',
  styleUrls: ['pengajuan.scss']
})
export class UploadFotoModalComponent implements OnInit  {
  pengajuan?: IPengajuanDTO |any;
  isSaving = false;
  idProv?: "";
  idKab?: "";
  idProd?:"";
  formulir !: File;
  formulirName !: string;
  ktp !: File;
  ktpName!: string;
  fotoProduk !: File;
  fotoProdukName !: string;
  fotoPemilihUsaha !: File;
  fotoPemilihUsahaName !: string;
  sizeFoto!: number;
  selectedFiles!: FileList;
  currentFile!: File;
  
   editForm = this.fb.group({
    formPengajuan:['', [Validators.required]],
    fotoProduk:['', [Validators.required]],
    fotoPemilihUsaha:['', [Validators.required]],
    ktp:['', [Validators.required]],
  });

  constructor(
	  	public activeModal: NgbActiveModal,
    	private fb: FormBuilder,
    	protected pengajuanService: PengajuanService,
    	protected eventManager: JhiEventManager,
    	protected dataUtils: JhiDataUtils,
    ) {}
  
  
  ngOnInit(): void {
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
  

  save(id: any, kategori: string): void {
	 if ('ktp' === kategori) {
  		this.subscribeToSaveResponse(this.pengajuanService.create(id,kategori, this.ktp));
	 } else if ('formulir' === kategori){
  		this.subscribeToSaveResponse(this.pengajuanService.create(id,kategori, this.formulir));
	 } else if ('fotoProduk' === kategori){
  		this.subscribeToSaveResponse(this.pengajuanService.create(id,kategori, this.fotoProduk));
	 } else {
  		this.subscribeToSaveResponse(this.pengajuanService.create(id,kategori, this.fotoPemilihUsaha));
	 }
    
  }
  
  saveAll(id: any): void{
  		this.subscribeToSaveResponse(this.pengajuanService.createAll(id, this.formulir, this.ktp, this.fotoProduk, this.fotoPemilihUsaha));
  }
  
  selectFile(kategori: string,event: any): void {
	  if (kategori === 'formulir') {
    	this.formulir = event.target.files.item(0);
    	this.formulirName = event.target.files.item(0).name.substr(event.target.files.item(0).name.length-20, event.target.files.item(0).name.length);
	  } else if (kategori === 'ktp') {
    	this.ktp = event.target.files.item(0);
    	this.ktpName = event.target.files.item(0).name.substr(event.target.files.item(0).name.length-20, event.target.files.item(0).name.length);
	  } else if (kategori === 'fotoProduk') {
    	this.fotoProduk = event.target.files.item(0);
    	this.fotoProdukName = event.target.files.item(0).name.substr(event.target.files.item(0).name.length-20, event.target.files.item(0).name.length);
	  } else {
    	this.fotoPemilihUsaha = event.target.files.item(0);
    	this.fotoPemilihUsahaName = event.target.files.item(0).name.substr(event.target.files.item(0).name.length-20, event.target.files.item(0).name.length);
	  }
  }

  upload(file: File, tipe: string): void {
	  console.log(file, tipe)
      const formData = new FormData();
          formData.append('file', file);
          formData.append('tipe', tipe);
          this.subscribeToSaveResponse(this.pengajuanService.upload(formData));
  }
  
  setFileData(event: any, field: string) : void {
	  if (event.target) {
 		 const formData = new FormData();
          formData.append('file', event.target.files.item(0));
          formData.append('tipe', field);
          this.subscribeToSaveResponse(this.pengajuanService.upload(formData));
	  }
    }
  
  success(): void {
	  Swal.fire({
		      icon: 'info',
		      text: 'Dokumen Pengajuan Berhasil di Simpan !!!',
		      showCancelButton: false,
		    } as any);
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPengajuanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.success();
    this.eventManager.broadcast('pengajuantListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
