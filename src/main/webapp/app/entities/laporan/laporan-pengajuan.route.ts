import { Routes } from '@angular/router';
import { PengajuanComponent } from './laporan-pengajuan.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const pengajuanState: Routes = [{
  path: '',
  component: PengajuanComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
