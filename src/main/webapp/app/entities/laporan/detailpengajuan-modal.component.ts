import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IPengajuanDTO, PengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import { LaporanPengajuanService } from './laporan-pengajuan.service';
import { IProvinsiDTO } from 'app/shared/model/provinsiDTO.model';
import { IKabupatenDTO } from 'app/shared/model/kabupatenDTO.model';
import { IDaftarProdukDTO } from 'app/shared/model/daftarprodukDTO.model';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import * as fileSaver from 'file-saver';

@Component({
  selector: 'jhi-detailpengajuan-modal',
  templateUrl: './detailpengajuan-modal.component.html',
  styleUrls: ['laporan-pengajuan.scss']
})
export class DetailPengajuanModalComponent implements OnInit  {
  pengajuan?: IPengajuanDTO |any;
  provinsis?: IProvinsiDTO[] |any;
  kabupatens?: IKabupatenDTO[] |any;
  produks?: IDaftarProdukDTO[] |any;
  isSaving = false;
  idProv?: "";
  idKab?: "";
  idProd?:"";
  formulir !: File;
  ktp !: File;
  fotoProduk !: File;
  fotoPemilihUsaha !: File;
  
   editForm = this.fb.group({
    nama:['', [Validators.required]],
    nik:['', [Validators.required]],
    email:['', [Validators.required]],
    noHp:['', [Validators.required]],
    alamat:['', [Validators.required]],
    namaUsaha:['', [Validators.required]],
    merkDagang:['', [Validators.required]],
    alamatUsaha:['', [Validators.required]],
    kodepos:['', [Validators.required]],
    formPengajuan:['', [Validators.required]],
    fotoProduk:['', [Validators.required]],
    fotoPemilihUsaha:['', [Validators.required]],
    ktp:['', [Validators.required]],
    nib:['', [Validators.required]],
    prov:['', [Validators.required]],
    kab:['', [Validators.required]],
    prod:['', [Validators.required]]
  });

  constructor(
	  	public activeModal: NgbActiveModal,
    	private fb: FormBuilder,
    	protected pemeriksaanService: LaporanPengajuanService,
    	protected eventManager: JhiEventManager,
    ) {
	    this.pemeriksaanService
	      .provinsi({
		        size: 1000
		      })
	      .subscribe((res: HttpResponse<IProvinsiDTO[]>) => this.provinsis = res.body || []);
	    this.pemeriksaanService
	      .daftarProduk({
		        size: 2000
		      })
	      .subscribe((res: HttpResponse<IDaftarProdukDTO[]>) => this.produks = res.body || []);
	}
  
  
  ngOnInit(): void {
  }
  
  onChange(id: number): void {
	    this.pemeriksaanService
	      .kabupaten(id)
	      .subscribe((res: HttpResponse<IKabupatenDTO[]>) => this.kabupatens = res.body || []);

		this.editForm.patchValue({
		  kab: this.kabupatens,
		});
	  
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
  

/*  save(): void {
    this.isSaving = true;
    const pengajuan = this.createFromForm();
    if (pengajuan) {
    	this.subscribeToSaveResponse(this.pemeriksaanService.create(pengajuan));
	}
  }*/
  
  selectFile(kategori: string,event: any): void {
	  if (kategori === 'formulir') {
    	this.formulir = event.target.files.item(0);
	  } else if (kategori === 'ktp') {
    	this.ktp = event.target.files.item(0);
	  } else if (kategori === 'fotoProduk') {
    	this.fotoProduk = event.target.files.item(0);
	  } else {
    	this.fotoPemilihUsaha = event.target.files.item(0);
	  }
  }

  downloadDoc(fileName: string): void {
    this.pemeriksaanService.getDocImage(fileName).subscribe(
      res => {
        // Success
        console.log('start download:', fileName);
        const blob = new Blob([res], { type: 'application/pdf' });
        fileSaver.saveAs(blob, fileName);
      },
      error => {
        // Error
        console.log(error);
      }
    );
  }

  private createFromForm(): IPengajuanDTO {
    return {
      ...new PengajuanDTO(),
	    nama: this.editForm.get(['nama'])!.value,
	    nik: this.editForm.get(['nik'])!.value,
	    alamat: this.editForm.get(['alamat'])!.value,
	    kodepos: this.editForm.get(['kodepos'])!.value,
	    email: this.editForm.get(['email'])!.value,
	    noHp: this.editForm.get(['noHp'])!.value,
	    namaUsaha: this.editForm.get(['namaUsaha'])!.value,
	    alamatUsaha: this.editForm.get(['alamatUsaha'])!.value,
	    nib: this.editForm.get(['nib'])!.value,
	    merkDagang: this.editForm.get(['merkDagang'])!.value,
	    provinsiid: this.editForm.get(['prov'])!.value,
	    kabupatenid: this.editForm.get(['kab'])!.value,
	    daftarpordukid: this.editForm.get(['prod'])!.value,
    };
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPengajuanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('pemeriksaanListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
