import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { PengajuanComponent } from './laporan-pengajuan.component';
import { DetailPengajuanModalComponent } from './detailpengajuan-modal.component';
import { pengajuanState } from './laporan-pengajuan.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(pengajuanState)],
  declarations: [PengajuanComponent,DetailPengajuanModalComponent],
  entryComponents: [DetailPengajuanModalComponent]
})
export class PengajuanSehatiModule {}
