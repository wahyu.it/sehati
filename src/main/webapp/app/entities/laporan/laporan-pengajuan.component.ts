import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { LaporanPengajuanService } from './laporan-pengajuan.service';
import { IPengajuanDTO } from 'app/shared/model/pengajuanDTO.model';
import { IPendampingDTO } from 'app/shared/model/pendampingDTO.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { DetailPengajuanModalComponent } from './detailpengajuan-modal.component';
import { PendampingService } from '../pendamping/pendamping.service';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

@Component({
  selector: 'jhi-laporan-pengajuan',
  templateUrl: './laporan-pengajuan.component.html',
  styleUrls: ['laporan-pengajuan.scss']
})
export class PengajuanComponent implements OnInit {
  pengajuans?: IPengajuanDTO[] |any;
  pengajuan?: IPengajuanDTO |any;
  pendampings?: IPendampingDTO[] |any;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;
  saldo!: number | undefined;
  pendampingid!: string; 

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    nomor: new FormControl(''),
    pendamping: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    nomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected pengajuanService: LaporanPengajuanService,
    protected matSelectModule: MatSelectModule,
    protected matFormFieldModule: MatFormFieldModule,
    private datePipe: DatePipe,
    protected router: Router,
    private modalService: NgbModal,
    protected pendampingService: PendampingService,
	) {
		this.sPpd = true;	
	    this.pendampingService
	      .search({
		        size: 10000
		      })
	      .subscribe((res: HttpResponse<IPendampingDTO[]>) => this.pendampings = res.body || []);
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	      this.loadPage();
	    });
    this.registerChangeInPengajuan();
    this.loadData();
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'tglPengajuan';
	  } else {
		 this.sPpd = false;
		 this.key = 'blnPengajuan';
	  }
  }
  
  onChangePendamping(e:any): void {
	  console.log("pendamping:" + e);
	  this.pendampingid = e;
  }
  
  searchppk(noPengajuan: string): void {
	  this.key = noPengajuan;
	  let nomor = this.ppkGroup.get(['nomor'])!.value;;
	  if (this.key === 'noPengajuan') {
		  nomor = this.ppkGroup.get(['nomor'])!.value;
	  }
	  this.value = nomor;
      this.searchByParam();
  }
  
  searchpendamping(): void {
      this.searchByParam();
  }

  searchPpd(keys: string): void {
	  if (keys === 'ppdDate') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  } 
	  
      this.searchByParam();
  }
  
// open new tab 
  openDoc(fileName: string): void {
      window.open(fileName, '_blank');
  }
  
  searchByParam(): void  {
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/laporan-pengajuan'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          spendampingid : this.pendampingid,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadPage();
  }

  transition(): void {
      this.router.navigate(['/laporan-pengajuan'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  private loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.pengajuanService
      .search({
        skey: this.key,
        svalue: this.value,
        spendampingid : this.pendampingid,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.pengajuanService
        .search({
          skey: this.key,
          svalue: this.value,
          spendampingid : this.pendampingid,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IPengajuanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  invoice(id: any): void {
	this.pengajuanService.invoice(id).subscribe((res2: HttpResponse<any>)  => {
		this.pengajuan = res2.body;
		this.loadPage();
	  })
  }
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' +  'desc'];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInPengajuan(): void {
    this.eventSubscriber = this.eventManager.subscribe('LaporanPengajuantListModification', () => this.loadPage());
  }
  
  detailPengajuan(pengajuan: IPengajuanDTO): void {
    const modalRef = this.modalService.open(DetailPengajuanModalComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.pengajuan = pengajuan;
  }
  
  warning(): void {
	  Swal.fire({
		      icon: 'info',
		      text: ' Saldo anda tidak mencukupi, segera lakukan isi ulang !!!',
		      showCancelButton: false,
		    } as any);
  }
  
  protected onSuccess(data: IPengajuanDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/laporan-pengajuan'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.pengajuans = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    console.log("result :" +result)
    return result
  }
}
