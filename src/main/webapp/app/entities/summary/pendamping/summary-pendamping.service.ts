import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { ISummaryPengajuanDTO } from 'app/shared/model/summaryPengajuanDTO.model';

type EntityArrayResponseType = HttpResponse<ISummaryPengajuanDTO[]>;

@Injectable({ providedIn: 'root' })
export class SummaryPengajuanPendampingService {
  	  public resourceUrlPengajuan = SERVER_API_URL + 'api/pengajuan/';
	
	  constructor(protected http: HttpClient) {}

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<ISummaryPengajuanDTO[]>(this.resourceUrlPengajuan + 'summary/pendamping', { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }


	  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType { 
		  if (res.body) {
		    res.body.forEach((r: ISummaryPengajuanDTO) => {
		        r.bulan = r.bulan ? moment(r.bulan) : undefined;
		      });
		    }
	    return res;
	  }
	
}