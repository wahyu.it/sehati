import { Routes } from '@angular/router';
import { SummaryPengajuanPendampingComponent } from './summary-pendamping.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const summaryPengajuanPendampingService: Routes = [{
  path: '',
  component: SummaryPengajuanPendampingComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
