import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { SummaryPengajuanPendampingComponent } from './summary-pendamping.component';
import { summaryPengajuanPendampingService } from './summary-pendamping.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(summaryPengajuanPendampingService)],
  declarations: [SummaryPengajuanPendampingComponent],
})
export class SummaryPengajuanPendampingSehatiModule {}
