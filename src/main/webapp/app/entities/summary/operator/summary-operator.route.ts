import { Routes } from '@angular/router';
import { SummaryPengajuanOperatorComponent } from './summary-operator.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const summaryPengajuanOperatorService: Routes = [{
  path: '',
  component: SummaryPengajuanOperatorComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
