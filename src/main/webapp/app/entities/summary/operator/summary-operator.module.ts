import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { SummaryPengajuanOperatorComponent } from './summary-operator.component';
import { summaryPengajuanOperatorService } from './summary-operator.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(summaryPengajuanOperatorService)],
  declarations: [SummaryPengajuanOperatorComponent],
})
export class SummaryPengajuanOperatorSehatiModule {}
